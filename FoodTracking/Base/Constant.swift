
import Foundation
//import Firebase
import UIKit
import SwiftyUserDefaults
import RealmSwift

//*****************************************************************//
                        //Global Variable//
//*****************************************************************//

var loginNav : UINavigationController!
var mainNav : UINavigationController!

var dropViewVC:DropViewVC!
var darkVC:DarkVC!
var contactCoachVC:ContactCoachVC!
let durationOfAnimationInSecond = 1.0
var sampleMealName = 0
var alertController : UIAlertController? = nil
var collectionViewDataSource = [CollectionViewModel]() //Total Food data
var cal_ListDataSource = [[Cal_listModel]]() // lowest datastructure
var expandableHeaderId = [String]() // expandable datasource headerId
var dropDownOptionsDataSource = [String]() // dropdown name list
var recommendedTxtVal = [String]() // recommendedText string
var mealCaloriesArray = [0,0,0,0,0,0] // sixmeal calories array
var sampleArrayList = [[String]]() // Total sampleMeal double Array List
var sampleArray = [String]() // sampleMeal name Array
var longPressIndex : Int?
var foodHistoyModel : FoodHistoryModel! // meal HistoyModel
var totalfoodHistoyModel : TotalFoodHistoryModel! // meal HistoyModel
var categoriesModelList = List<CategoriesModel>() // six meal model list
var totalcategoriesModelDataSource = [TotalCategoriesModel]()
var tempModelList = [tempFoodModel]()
// breakfast meal model list
var historyDataSouce = [HistoryModel]()
var historyIndex = 0
var SplashScreen = false
//var timeNow = Int(NSDate().timeIntervalSince1970)
var dialogIndex = -1
var workoutVideoId = -1
var nutrionistProductId = -1
var coatchingServiceProductId = -1
var default_time = 0

var checkoutcountry : String?
var checkoutfirstName : String?
var checkoutlastName : String?
var checkoutaddress : String?
var checkouttown : String?
var checkoutstate : String?
var checkoutzipcode : String?
var checkoutemail : String?
var checkoutphone : String?
var checkoutamount : String?
var subscriptionId : String?
var programvideoID : String?
var downloadvideoID : String?

var shippingcountry : String?
var shippingfirstName : String?
var shippinglastName : String?
var shippingaddress : String?
var shippingtown : String?
var shippingstate : String?
var shippingzipcode : String?
var shippingVC : Bool = false

var ds_checkoutModel = [WcnModel]()
var downloadModel: WcnModel?
var cancelsubsriptionModel : WcnModel?
var nutritionModel : WcnModel? // 

var calltag = -1

var sub_price: String = ""
var sub_title: String = ""
var sub_content : String = ""

var videoUri: String?
var loginEmail: String?
//var productType: String? //
var subtabType: String? // for setting subtab's state for all type
var fragmentType: String? // setting for the product type for view cart screen
var foodctrType: String? // for food tracking side controller
var messagePhoneNumber: String? // for message body setting

var remove_dialogindex: Int = 0
var removeType : String? // for setting collection and table view remove..

struct Constants {
    static let SAVE_ROOT_PATH = "psj"
    static let TOTAL_CALORIE = "total_cal"
    static let EMAIL = "email"
    static let REMAINING_CALORIE = "remaining_cal"
    static let TOTAL_BREAKFAST_CALORIE = "break_cal"
    static let TOTAL_FIRST_SNACK_CALORIE = "first_cal"
    static let TOTAL_LUNCH_CALORIE = "lunch_cal"
    static let TOTAL_SECOND_SNACK_CALORIE = "second_cal"
    static let TOTAL_DINNER_CALORIE = "dinner_cal"
    static let TOTAL_THIRD_SNACK_CALORIE = "third_cal"
    static let DAILY_SCORE = "daily_score"
    static let DAILY_GRADE = "daily_grade"
    static let OLD_VALUE = "old_value"
    static let BREAKFAST = "1"
    static let FIRTSNACK = "2"
    static let LUNCH = "3"
    static let SECONDSNACK = "4"
    static let DINNER = "5"
    static let THIRDSNACK = "6"
    static let LASTTIMEDATE = "lasttimedate"
    static let LOGOUT = "logout"
    static let BACKGROUND = "background"
    static let one_1 = "one_1"
    static let one_2 = "one_2"
    static let one_3 = "one_3"
    static let one_4 = "one_4"
    static let two_1 = "two_1"
    static let two_2 = "two_2"
    static let two_3 = "two_3"
    static let two_4 = "two_4"
    static let three_1 = "three_1"
    static let three_2 = "three_2"
    static let three_3 = "three_3"
    static let three_4 = "three_4"
    static let four_1 = "four_1"
    static let four_2 = "four_2"
    static let four_3 = "four_3"
    static let four_4 = "four_4"
    static let five_1 = "five_1"
    static let five_2 = "five_2"
    static let five_3 = "five_3"
    static let five_4 = "five_4"
    static let six_1 = "six_1"
    static let six_2 = "six_2"
    static let six_3 = "six_3"
    static let six_4 = "six_4"
    static let deviceToken = UIDevice.current.identifierForVendor!.uuidString
    static let deviceType = "iphone"
    
    // card informations
    static let CARD_FIRSTNAME = "card_firstname"
    static let CARD_LASTNAME = "card_lastname"
    static let CARD_EMAIL = "card_email"
    static let CARD_PHONE = "card_phone"
    static let CARD_COUNTRY = "card_country"
    static let CARD_CARDCITY = "card_city"
    static let CARD_STATE = "card_state"
    static let CARD_ZIPCODE = "card_zipcode"
    static let CARD_ADDRESS = "card_address"
    static let CARD_NUMBER = "card_number"
    static let CARD_CODE = "card_code"
    static let CARD_EXPIERATIONDATE = "card_expirationdate"
}

