//
//  BaseVC1.swift
//  Loyaltiyapp
//
//  Created by PSJ on 10/21/19.
//  Copyright © 2019 PSJ. All rights reserved.
//

import UIKit
import SwiftyUserDefaults
import SwiftyJSON
import KRProgressHUD
import KRActivityIndicatorView
import RealmSwift

class BaseVC1 : UIViewController {
    
    var gradientLayer: CAGradientLayer!
    override func viewDidLoad() {
        super.viewDidLoad()
        (UIApplication.shared.delegate as! AppDelegate).restrictRotation = .portrait
    }
    
    func auto_paymentProcess(_ vncModel: WcnModel!){
        //showProgress()
        
        if let fragmenttype = fragmentType{
            let cardNumber = UserDefault.getString(key: Constants.CARD_NUMBER, defaultValue: "")
                let expirationDate = UserDefault.getString(key: Constants.CARD_EXPIERATIONDATE, defaultValue: "")
                let cardCode = UserDefault.getString(key: Constants.CARD_CODE, defaultValue: "")
                let firstName = UserDefault.getString(key: Constants.CARD_FIRSTNAME, defaultValue: "")
                let lastName = UserDefault.getString(key: Constants.CARD_LASTNAME, defaultValue: "")
                let country = UserDefault.getString(key: Constants.CARD_COUNTRY, defaultValue: "")
                let address = UserDefault.getString(key: Constants.CARD_ADDRESS, defaultValue: "")
                let city = UserDefault.getString(key: Constants.CARD_CARDCITY, defaultValue: "")
                let state = UserDefault.getString(key: Constants.CARD_STATE, defaultValue: "")
                let zipCode = UserDefault.getString(key: Constants.CARD_ZIPCODE, defaultValue: "")
                let email = UserDefault.getString(key: Constants.CARD_EMAIL, defaultValue: "")
                let phone = UserDefault.getString(key: Constants.CARD_PHONE, defaultValue: "")
                
                let descriptionn = "Title : " + vncModel.title + ", Type : " + vncModel.type + ", Price : $" + vncModel.price + ", Quantity : " + vncModel.quantity
            
                ApiManager.paymentProcess(pay_type:"auto" , productID: vncModel.id, type: fragmentType!, user_id: user!.id, first_name: firstName!, last_name:lastName! , user_email: email!, user_phone: phone!, country: country!, address: address!, city: city!, state: state!, zipcode: zipCode!, cardNumber: cardNumber!, expirationDate: expirationDate! , cardCode:cardCode!, amount: vncModel.price, description: descriptionn){
                    
                (isSuccess, data) in
                //self.hideProgress()
                if isSuccess{
                    let id = vncModel.id
                    let timeNow = Int(NSDate().timeIntervalSince1970) * 1000
                    var enddayDouble: Int = 0
                    if let period = vncModel.period.toInt(){
                        enddayDouble =  Int(timeNow + period  * 86400000)
                    }
                    let endday = "\(enddayDouble)"
                    
                    ApiManager.setPurchaseProduct(ids: id, endDay: endday) { (isSuccess1, data1) in
                        if isSuccess1{
                            if fragmenttype == FragmentType.workout.rawValue{
                                self.gotoStoryBoardVC(VC_WorkoutVideoVC, fullscreen: true)
                            }
                            else if fragmenttype == FragmentType.nutrition.rawValue{
                                self.gotoStoryBoardVC(VC_NutritionistVC, fullscreen: true)
                            }
                            else {
                                self.gotoStoryBoardVC(VC_contact, fullscreen: true)
                            }
                        }
                        else{
                            ApiManager.removeSubscription(product_id: vncModel.id) { (isSuccess, data) in
                                if isSuccess{
                                    //self.progShowInfo(true, msg: "Subscription payment process fail.Please check your cardinfo!")
                                }
                            }
                        }
                    }
                }
                else{
                    //self.alertDisplay(alertController: self.alertMake("Something went wrong"))
                }
            }
        }
    }
    
    func gotoStoryBoardVC(_ name: String, fullscreen: Bool) {
        let storyboad = UIStoryboard(name: name, bundle: nil)
        let targetVC = storyboad.instantiateViewController(withIdentifier: name)
        if fullscreen{
            targetVC.modalPresentationStyle = .fullScreen
        }
        self.present(targetVC, animated: false, completion: nil)
    }
    
    func gotoStoryBoardVCModal(_ name: String) {
        let storyboad = UIStoryboard(name: name, bundle: nil)
        let targetVC = storyboad.instantiateViewController(withIdentifier: name)
        targetVC.modalPresentationStyle = .pageSheet
        //targetVC.modalTransitionStyle = .crossDissolve
        self.present(targetVC, animated: false, completion: nil)
    }
    
    func gotoVCModal(_ name: String) {
        let storyboad = UIStoryboard(name: "Main", bundle: nil)
        let targetVC = storyboad.instantiateViewController(withIdentifier: name)
        targetVC.modalPresentationStyle = .fullScreen
        //targetVC.modalTransitionStyle = .crossDissolve
        self.present(targetVC, animated: false, completion: nil)
    }
    
    func goto4dialog(_ name: String, index: Int) {
        let storyboad = UIStoryboard(name: "Main", bundle: nil)
        let targetVC = storyboad.instantiateViewController(withIdentifier: name)
        targetVC.modalPresentationStyle = .fullScreen
        //targetVC.modalTransitionStyle = .crossDissolve
        remove_dialogindex = index
        self.present(targetVC, animated: false, completion: nil)
    }
    
    func gotoVC(_ nameVC: String){
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let toVC = storyboard.instantiateViewController( withIdentifier: nameVC)
        toVC.modalPresentationStyle = .fullScreen
        self.present(toVC, animated: false, completion: nil)
        //print("herereached")
    }
    
    func initDropDownDataSource(){
        
        dropDownOptionsDataSource.removeAll()
        for one in collectionViewDataSource{
            dropDownOptionsDataSource.append(one.title)
        }
    }
    
    func  initRecommendedShow(_ index : Int) ->  String {
        recommendedTxtVal.removeAll()
        for one in collectionViewDataSource{
            recommendedTxtVal.append(one.recomended)
        }
        return recommendedTxtVal[index]
    }
    
    func  initCalList(_ index : Int) ->  [Cal_listModel] {
        cal_ListDataSource.removeAll()
       for one in collectionViewDataSource{
        cal_ListDataSource.append(one.calorie_list)
       }
        return cal_ListDataSource[index]
    }
    
    
    func  initExpandableHeaderForId(_ index : Int) ->  String {
        expandableHeaderId.removeAll()
       for one in collectionViewDataSource{
        expandableHeaderId.append(one.id)
       }
        //print("this is initID ===",expandableHeaderId[index])
        return expandableHeaderId[index]
    }
    
    func createGradientView(_ targetView : UIView) {
        gradientLayer = CAGradientLayer()
        gradientLayer.frame = targetView.bounds
        gradientLayer.colors = [UIColor.init(named: "startColor")!.cgColor, UIColor.init(named: "endColor")!.cgColor]
        targetView.layer.addSublayer(gradientLayer)
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
        
    }
    
    func createGradientLabel(_ targetView : UIView, letter : String,fontsize : Int ,position : Int) {
        
        gradientLayer = CAGradientLayer()
        gradientLayer.frame = targetView.bounds
        gradientLayer.colors = [UIColor.init(named: "startColor")!.cgColor, UIColor.init(named: "endColor")!.cgColor]
        targetView.layer.addSublayer(gradientLayer)
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
        
        // Create a label and add it as a subview
        let label = UILabel(frame: targetView.bounds)
        label.text = letter
        label.font = UIFont.boldSystemFont(ofSize: CGFloat(fontsize))
        
        if position == 0 {
            label.textAlignment = .center
        }
        else if position == 1{
            label.textAlignment = .left
        }
        
        else if position == 2{
            label.textAlignment = .right
        }
        
        targetView.addSubview(label)
        
        // Tha magic! Set the label as the views mask
        targetView.mask = label
    }
    
    func transitionAnimation(view: UIView, animationOptions: UIView.AnimationOptions, isReset: Bool) {
      UIView.transition(with: view, duration: durationOfAnimationInSecond, options: animationOptions, animations: {
        view.backgroundColor = UIColor.init(named: isReset ? "darkGreen" :  "darkRed")
      }, completion: nil)
    }
    
//    func showMessage1(_ message : String) {
//        self.view.makeToast(message)
//    }
    
    func setProgressHUDStyle(_ style: Int,backcolor: UIColor,textcolor : UIColor, imagecolor : UIColor ) {
        
            if style != 2{
            let styles: [KRProgressHUDStyle] = [.white, .black, .custom(background: #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1), text: #colorLiteral(red: 0.1294117719, green: 0.2156862766, blue: 0.06666667014, alpha: 1), icon: #colorLiteral(red: 0.9372549057, green: 0.3490196168, blue: 0.1921568662, alpha: 1))]
                KRProgressHUD.set(style: styles[style]) }
            
            else{
                let styles : KRProgressHUDStyle = .custom (background:backcolor,text : textcolor, icon: imagecolor )
                 KRProgressHUD.set(style: styles)
                
            }
    }
   func getHistory(){
    
    historyDataSouce.removeAll()
    let email = UserDefault.getString(key: PARAMS.EMAIL,defaultValue: "")
       //print("this is email",email)
    ApiManager.getHistory(email: email ?? "") { (isSuccess, data) in
           //self.hideProgress()
           
           if isSuccess{
               let dict = JSON(data as Any)
               //print(dict)
               for one in dict{
                    let miniJSON = JSON(one.1)
                    historyDataSouce.append( HistoryModel(miniJSON))
               }
               //self.gotoVC("HistoryVC")
            self.gotoHistoryVC()
            
           }
           else{
            //self.showAlert_with_OK_Cancel(message: "No History!", title: "")
            //self.gotoHistoryVC()
            self.alertDisplay(alertController: self.alertMake("No History"))
           }
       }
   }
   
    func gotoHistoryVC(){
        let storyboard = UIStoryboard(name: "HistoryVC", bundle: nil)
        let historyVC = storyboard.instantiateViewController(withIdentifier: "HistoryVC") as! HistoryVC
        UIApplication.shared.keyWindow?.rootViewController = historyVC
    }
    
    func getTotalValArray()  {
           mealCaloriesArray[0] = UserDefault.getInt(key: Constants.TOTAL_BREAKFAST_CALORIE)
           mealCaloriesArray[1] = UserDefault.getInt(key: Constants.TOTAL_FIRST_SNACK_CALORIE)
           mealCaloriesArray[2] = UserDefault.getInt(key: Constants.TOTAL_LUNCH_CALORIE)
           mealCaloriesArray[3] = UserDefault.getInt(key: Constants.TOTAL_SECOND_SNACK_CALORIE)
           mealCaloriesArray[4] = UserDefault.getInt(key: Constants.TOTAL_DINNER_CALORIE)
           mealCaloriesArray[5] = UserDefault.getInt(key: Constants.TOTAL_THIRD_SNACK_CALORIE)
    }

    func clearLocalDatabase(_ item : Bool){
        
        if item {
            UserDefault.setString(key: Constants.OLD_VALUE,value: "0")
            UserDefault.setInt(key: Constants.TOTAL_BREAKFAST_CALORIE,value: 0)
            UserDefault.setInt(key: Constants.TOTAL_FIRST_SNACK_CALORIE,value: 0)
            UserDefault.setInt(key: Constants.TOTAL_LUNCH_CALORIE,value: 0)
            UserDefault.setInt(key: Constants.TOTAL_SECOND_SNACK_CALORIE,value: 0)
            UserDefault.setInt(key: Constants.TOTAL_DINNER_CALORIE,value: 0)
            UserDefault.setInt(key: Constants.TOTAL_THIRD_SNACK_CALORIE,value: 0)
            UserDefault.setFloat(key: Constants.DAILY_SCORE, value: 0)
            UserDefault.setString(key: Constants.DAILY_GRADE, value: "0")
            totalfoodHistoyModel = TotalFoodHistoryModel()
            DBHelper.shared.removeall()
        }
        else{
            UserDefault.setInt(key: Constants.TOTAL_BREAKFAST_CALORIE,value: 0)
            UserDefault.setInt(key: Constants.TOTAL_FIRST_SNACK_CALORIE,value: 0)
            UserDefault.setInt(key: Constants.TOTAL_LUNCH_CALORIE,value: 0)
            UserDefault.setInt(key: Constants.TOTAL_SECOND_SNACK_CALORIE,value: 0)
            UserDefault.setInt(key: Constants.TOTAL_DINNER_CALORIE,value: 0)
            UserDefault.setInt(key: Constants.TOTAL_THIRD_SNACK_CALORIE,value: 0)
            UserDefault.setFloat(key: Constants.DAILY_SCORE, value: 0)
            UserDefault.setString(key: Constants.DAILY_GRADE, value: "0")
            totalfoodHistoyModel = TotalFoodHistoryModel()
            DBHelper.shared.removeall()
        }
        
        
    }
    
   func showProgressSet_withMessage(_ msg : String, msgOn : Bool,styleVal: Int ,backColor: UIColor,textColor : UIColor,imgColor : UIColor , headerColor : UIColor, trailColor : UIColor)  {
    
    setProgressHUDStyle(styleVal,backcolor: backColor,textcolor : textColor, imagecolor: imgColor)
        KRProgressHUD.set(activityIndicatorViewColors: [headerColor, trailColor])
        KRProgressHUD.show(withMessage: msgOn == false ? nil : msg)
    }
    
    func progressSet(styleVal: Int ,backColor: UIColor,textColor : UIColor , imgcolor: UIColor, headerColor : UIColor, trailColor : UIColor)  {
    
        setProgressHUDStyle(styleVal,backcolor: backColor,textcolor : textColor, imagecolor: imgcolor)
        KRProgressHUD.set(activityIndicatorViewColors: [headerColor, trailColor])
        
    }

    func progShowSuccess(_ msgOn:Bool, msg:String){
        self.progressSet( styleVal: 2, backColor: UIColor.init(named: "ColorBlur")!, textColor: .white, imgcolor: .red, headerColor: .red, trailColor: .yellow)
        KRProgressHUD.showSuccess(withMessage: msgOn == false ? nil : msg)
    }

    func progShowError(_ msgOn:Bool, msg:String) {
        self.progressSet( styleVal: 2, backColor: UIColor.init(named: "ColorBlur")!, textColor: .white, imgcolor: .red, headerColor: .red, trailColor: .yellow)
        KRProgressHUD.showError(withMessage: msgOn == false ? nil : msg)
    }

    func progShowWithImage(_ msgOn:Bool, msg:String ,image : String) {
        self.progressSet( styleVal: 2, backColor: UIColor.init(named: "ColorBlur")!, textColor: .white, imgcolor: .red, headerColor: .red, trailColor: .yellow)
        let image = UIImage(named:image)!
        KRProgressHUD.showImage(image, message: msgOn == false ? nil : msg)
    }
    
    func progShowInfo(_ msgOn:Bool, msg:String) {
        self.progressSet( styleVal: 2, backColor: UIColor.init(named: "ColorBlur")!, textColor: .white, imgcolor: .red, headerColor: .red, trailColor: .yellow)
        KRProgressHUD.showInfo(withMessage: msgOn == false ? nil : msg)
    }
    
    func showProgress() {
        self.progressSet( styleVal: 2, backColor: UIColor.init(named: "ColorBlur")!, textColor: .white, imgcolor: .red, headerColor: .red, trailColor: .yellow)
        KRProgressHUD.show()
    }
    func hideProgress() {
        KRProgressHUD.dismiss()
    }
    
    func resetProgress() {
        KRProgressHUD.resetStyles()
    }
    
    func alertMake(_ msg : String) -> UIAlertController {
        alertController = UIAlertController(title: nil, message: msg, preferredStyle: .alert)
        alertController!.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        return alertController!
    }
    
    func alertDisplay(alertController: UIAlertController) {
        self.present(alertController, animated: true, completion: nil)
    }
    
    func gotoNavPresent1(_ storyname : String) {
              
          let toVC = self.storyboard?.instantiateViewController(withIdentifier: storyname)
          toVC?.modalPresentationStyle = .fullScreen
          self.navigationController?.pushViewController(toVC!, animated: false)
            
    }
    
    func goBack() {
        self.navigationController?.popViewController(animated: false)
    }
    func goBackHome() {
        self.navigationController?.popToRootViewController(animated: false)
    }
    func goSetting() {
       let toVC = self.storyboard?.instantiateViewController(withIdentifier: "SettingsVC")
       toVC?.modalPresentationStyle = .fullScreen
       self.navigationController?.pushViewController(toVC!, animated: false)
    }
    
    func navBarHidden() {
       self.navigationController?.isNavigationBarHidden = true
   }
    func navBarTransparent1() {
      self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
      self.navigationController?.navigationBar.shadowImage = UIImage()
      self.navigationController?.navigationBar.isTranslucent = true
      self.navigationController?.view.backgroundColor = .clear
    }
    
    func navBarwithColor() {
         self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
         self.navigationController?.navigationBar.shadowImage = UIImage()
         self.navigationController?.navigationBar.isTranslucent = true
         self.navigationController?.view.backgroundColor = .blue
   }
    
    func dayDifference(from interval : TimeInterval) -> Bool
    {
        let calendar = Calendar.current
        let date = Date(timeIntervalSince1970: interval)
        if calendar.isDateInYesterday(date) {
            print("Yesterday")
            return true }
        else if calendar.isDateInToday(date) {
            print("Today")
            return false }
        else if calendar.isDateInTomorrow(date) {
            print("Tomorrow")
            return false }
        else {
            let startOfNow = calendar.startOfDay(for: Date())
            let startOfTimeStamp = calendar.startOfDay(for: date)
            let components = calendar.dateComponents([.day], from: startOfNow, to: startOfTimeStamp)
            let day = components.day!
            if day < 1 {
                print("\(-day) days ago")
                return true }
            else {
                print("In \(day) days")
                return false }
        }
    }
    
// slider navigation storyboard creation
    func creatNav() {
        
        dropViewVC = (UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DropViewVC") as! DropViewVC)
        dropViewVC.view.frame = CGRect(x:0 , y: -UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        
        darkVC = (UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DarkVC") as! DarkVC)
        darkVC.view.frame = CGRect(x: 0, y: -UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
    }
    
    func creatNav1() {
        
        contactCoachVC = (UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ContactCoachVC") as! ContactCoachVC)
        contactCoachVC.view.frame = CGRect(x:0 , y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        
        darkVC = (UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DarkVC") as! DarkVC)
        darkVC.view.frame = CGRect(x: 0, y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
    }

    // slider navigation open and close
    func openMenu(){
        darkVC.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        self.view.addSubview(darkVC.view)
        self.addChild(darkVC)
        UIView.animate(withDuration: 0.3, animations: {
            dropViewVC.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            self.view.addSubview(dropViewVC.view)
            self.addChild(dropViewVC)
        })
    }
    
    func openMenu1(){
        
        darkVC.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        self.view.addSubview(darkVC.view)
        self.addChild(darkVC)
        UIView.animate(withDuration: 0.3, animations: {
            contactCoachVC.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            self.view.addSubview(contactCoachVC.view)
            self.addChild(contactCoachVC)
        })
    }
    
    func closeMenu(){
        
        
            darkVC.view.frame = CGRect(x:0 , y: -UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            darkVC.view.removeFromSuperview()
            UIView.animate(withDuration: 0.3, animations: {
                dropViewVC.view.frame = CGRect(x:0 , y: -UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
                dropViewVC.view.willRemoveSubview(dropViewVC.view)
            })
        
        
    }
    
    func closeMenu1(){
        if !SplashScreen{
        darkVC.view.frame = CGRect(x:0 , y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        darkVC.view.removeFromSuperview()
        UIView.animate(withDuration: 0.3, animations: {
            contactCoachVC.view.frame = CGRect(x:0 , y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            contactCoachVC.view.willRemoveSubview(contactCoachVC.view)
        })
        }
        else{
            self.gotoStoryBoardVC("LoginVC", fullscreen: true)
        }
    }
    
    func setEdtPlaceholderColor(_ edittextfield : UITextField , placeholderText : String, placeColor : UIColor)  {
        edittextfield.attributedPlaceholder = NSAttributedString(string: placeholderText,
        attributes: [NSAttributedString.Key.foregroundColor: placeColor])
    }
    
    func makeMealHistory(_ mealtype : String) -> FoodHistoryModel{
        foodHistoyModel = FoodHistoryModel(id: mealtype , categories: categoriesModelList)
        return foodHistoyModel
    }
    
    func uploadHistory2Server() {
        ApiManager.uploadHistory(foodHistory: totalfoodHistoyModel){
            (issuccess,data) in
            if issuccess{
                
            }
            else{
                
            }
            
        }
    }
    func pureRelam() {
        do {
            try FileManager.default.removeItem(at: Realm.Configuration.defaultConfiguration.fileURL!)
        } catch  is NSError {
            print("error")
        }
        
    }
}


extension UITextField {
    
    enum PaddingSide1 {
        case left(CGFloat)
        case right(CGFloat)
        case both(CGFloat)
    }
    
    func addPadding1(_ padding: PaddingSide1) {
        
        self.leftViewMode = .always
        self.layer.masksToBounds = true
        
        switch padding {
            
        case .left(let spacing):
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: spacing, height: self.frame.height))
            self.leftView = paddingView
            self.rightViewMode = .always
            
        case .right(let spacing):
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: spacing, height: self.frame.height))
            self.rightView = paddingView
            self.rightViewMode = .always
            
        case .both(let spacing):
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: spacing, height: self.frame.height))
            self.leftView = paddingView
            self.leftViewMode = .always
            self.rightView = paddingView
            self.rightViewMode = .always
        }
    }
}

extension UIView {
    func dropShadowleft1(scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = UIColor.white.cgColor
        layer.shadowOpacity = 0.8
        layer.shadowOffset = CGSize(width: -1.0, height: 1.0)
        layer.shadowRadius = 3.0
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    
    func ShadowCenter1(scale: Bool = true) {
           layer.masksToBounds = false
           layer.shadowColor = UIColor.white.cgColor
           layer.shadowOpacity = 0.8
           layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
           layer.shadowRadius = 3.0
           layer.shouldRasterize = true
           layer.rasterizationScale = scale ? UIScreen.main.scale : 1
       }
    
    func dropShadowbottom1(scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = UIColor.white.cgColor
        layer.shadowOpacity = 0.7
        layer.shadowOffset = CGSize(width: 0.0, height:4.0)
        layer.shadowRadius = 2.0
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    
}

extension UIViewController {

    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }

    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}

extension UIColor {
    convenience init(hexString: String, alpha: CGFloat = 1.0) {
        let hexString: String = hexString.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let scanner = Scanner(string: hexString)

        if (hexString.hasPrefix("#")) {
            scanner.scanLocation = 1
        }

        var color: UInt32 = 0
        scanner.scanHexInt32(&color)

        let mask = 0x000000FF
        let r = Int(color >> 16) & mask
        let g = Int(color >> 8) & mask
        let b = Int(color) & mask

        let red   = CGFloat(r) / 255.0
        let green = CGFloat(g) / 255.0
        let blue  = CGFloat(b) / 255.0

        self.init(red:red, green:green, blue:blue, alpha:alpha)
    }

    func toHexString() -> String {
        var r:CGFloat = 0
        var g:CGFloat = 0
        var b:CGFloat = 0
        var a:CGFloat = 0

        getRed(&r, green: &g, blue: &b, alpha: &a)

        let rgb:Int = (Int)(r*255)<<16 | (Int)(g*255)<<8 | (Int)(b*255)<<0

        return String(format:"#%06x", rgb)
    }
}

extension UIButton {
    func configure(color: UIColor = .blue, font: UIFont = UIFont.boldSystemFont(ofSize: 12)) {
        self.setTitleColor(color, for: .normal)
        self.titleLabel?.font = font
        self.layer.borderColor = color.cgColor
        self.layer.borderWidth = 1.0
        self.layer.cornerRadius = 5.0
    }

    func configure(icon: UIImage, color: UIColor? = nil) {
        self.setImage(icon, for: .normal)
        if let color = color {
            tintColor = color
        }
    }

    func configure(color: UIColor = .blue,
                   font: UIFont = UIFont.boldSystemFont(ofSize: 12),
                   cornerRadius: CGFloat,
                   borderColor: UIColor? = nil,
                   backgroundColor: UIColor,
                   borderWidth: CGFloat? = nil) {
        self.setTitleColor(color, for: .normal)
        self.titleLabel?.font = font
        self.backgroundColor = backgroundColor
        if let borderColor = borderColor {
            self.layer.borderColor = borderColor.cgColor
        }
        if let borderWidth = borderWidth {
            self.layer.borderWidth = borderWidth
        }
        self.layer.cornerRadius = cornerRadius
    }
}

extension UITextField {
    func configure(color: UIColor = .blue,
                   font: UIFont = UIFont.boldSystemFont(ofSize: 12),
                   cornerRadius: CGFloat,
                   borderColor: UIColor? = nil,
                   backgroundColor: UIColor,
                   borderWidth: CGFloat? = nil) {
        if let borderWidth = borderWidth {
            self.layer.borderWidth = borderWidth
        }
        if let borderColor = borderColor {
            self.layer.borderColor = borderColor.cgColor
        }
        self.layer.cornerRadius = cornerRadius
        self.font = font
        self.textColor = color
        self.backgroundColor = backgroundColor
    }
}



extension DefaultsKeys {
    
    static let user_id = DefaultsKey<String?>("user_id")
    static let username = DefaultsKey<String?>("username")
    static let email = DefaultsKey<String?>("email")
    static let profile = DefaultsKey<String?>("profile")
    static let pwd = DefaultsKey<String?>("pwd")
    //static let calorieSetState = DefaultsKey<Int?>("calorieSetState")
    static let oldValue = DefaultsKey<String?>("oldValue")
    
}

extension UITextField {
    @IBInspectable var placeholderColor: UIColor {
        get {
            return attributedPlaceholder?.attribute(.foregroundColor, at: 0, effectiveRange: nil) as? UIColor ?? .clear
        }
        set {
            guard let attributedPlaceholder = attributedPlaceholder else { return }
            let attributes: [NSAttributedString.Key: UIColor] = [.foregroundColor: newValue]
            self.attributedPlaceholder = NSAttributedString(string: attributedPlaceholder.string, attributes: attributes)
        }
    }

}



