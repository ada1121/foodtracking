//
//  DBManager.swift
//  CouponExpress
//
//  Created by Elite on 04/12/16.
//  Copyright © 2016 EliteDev. All rights reserved.
//

import Foundation
import Realm
import RealmSwift
let realm = try! Realm()
/// DB Manager
class DBHelper {
    // Get the default Realm
    static let shared = DBHelper()
    
    private init() {}
    
    func updateHistoy(_ id : String, updateModel:FoodHistoryModel) {
        try! realm.write {
            let meal = realm.object(ofType: FoodHistoryModel.self, forPrimaryKey: id)
            
            if meal != nil{
                    meal?.categories.removeAll()
                    for one in updateModel.categories{
                        meal?.categories.append(one)
                    realm.add(meal!, update: .modified)
                    realm.refresh()
                }
            }
            else{
               realm.create(FoodHistoryModel.self, value: updateModel, update: .all)
               realm.refresh()
            }
        }
    }
    
    func getMealHistory( _ id: String) -> FoodHistoryModel {
        var mealhistory = FoodHistoryModel()
        if realm.object(ofType: FoodHistoryModel.self, forPrimaryKey: id) != nil{
            mealhistory = realm.object(ofType: FoodHistoryModel.self, forPrimaryKey: id)!
        }
        else{
            print("No object yet")
        }
        return mealhistory
    }
    
    func getTotalHistoy() -> Results<FoodHistoryModel> {
        let totalHistory = realm.objects(FoodHistoryModel.self)
        return totalHistory
    }
    
    func removewithIndex(_ id: String) {
       do {
           //let objects = realm.objects(FoodHistoryModel.self)
           try! realm.write {
            realm.delete(realm.object(ofType: FoodHistoryModel.self, forPrimaryKey: id)!)
           }
       } catch let error as NSError {
           // handle error
           print("error - \(error.localizedDescription)")
       }
    }

    func removeall(){
         try! realm.write {
            realm.deleteAll()
            realm.refresh()
         }
    }
    
    func getTotalUploadHistoryFromRealm(){
        
        let breakHistory : FoodHistoryModel? = DBHelper.shared.getMealHistory(Constants.BREAKFAST)
        let firstHistory : FoodHistoryModel? = DBHelper.shared.getMealHistory(Constants.FIRTSNACK)
        let lunchHistory : FoodHistoryModel? = DBHelper.shared.getMealHistory(Constants.LUNCH)
        let secondHistory : FoodHistoryModel? = DBHelper.shared.getMealHistory(Constants.SECONDSNACK)
        let dinnerHistory : FoodHistoryModel? = DBHelper.shared.getMealHistory(Constants.DINNER)
        let thirdHistory : FoodHistoryModel? = DBHelper.shared.getMealHistory(Constants.THIRDSNACK)
        
        let totalFoodHistoryModelList = List<FoodHistoryModel>()
        
        if breakHistory?.id != nil && breakHistory?.id != ""{
           totalFoodHistoryModelList.append(breakHistory!)
        }
        if firstHistory?.id != nil && firstHistory?.id != ""{
           totalFoodHistoryModelList.append(firstHistory!)
        }
        if lunchHistory?.id != nil && lunchHistory?.id != ""{
           totalFoodHistoryModelList.append(lunchHistory!)
        }
        if secondHistory?.id != nil && secondHistory?.id != ""{
           totalFoodHistoryModelList.append(secondHistory!)
        }
        if dinnerHistory?.id != nil && dinnerHistory?.id != ""{
           totalFoodHistoryModelList.append(dinnerHistory!)
        }
        if thirdHistory?.id != nil && thirdHistory?.id != ""{
           totalFoodHistoryModelList.append(thirdHistory!)
        }
        
        //print("\(totalFoodHistoryModelList.count)")
        if totalFoodHistoryModelList.count != 0{
            
            var superone = 0
            for one in totalFoodHistoryModelList{
                
                var oneloop = 0
                for two in one.categories{
                    var totalSubCategoriesModelDataSource = [TotalSubcategoriesModel]()
                    var twoloop = 0
                    for three in two.subcategories{
                        totalSubCategoriesModelDataSource.append(TotalSubcategoriesModel(id: three.id, title: three.title, cal: three.cal, carbs: three.carbs, fat: three.fat, protein: three.protein, fiber: three.fiber))
                        twoloop += 1
                        if twoloop == two.subcategories.count{
                            totalcategoriesModelDataSource.append(TotalCategoriesModel(id: two.id, meal_type: two.meal_type, food_type: two.food_type, subcategories: totalSubCategoriesModelDataSource))
                            totalSubCategoriesModelDataSource.removeAll()
                            oneloop += 1
                            
                            if oneloop == one.categories.count{
                                superone += 1
                                if superone == totalFoodHistoryModelList.count{
                                    self.makeTotalUploadHistoryModel()
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    func makeTotalUploadHistoryModel() {
        let timeNow = Int(NSDate().timeIntervalSince1970) * 1000
        totalfoodHistoyModel = TotalFoodHistoryModel(id:"\(timeNow)",email: UserDefault.getString(key: Constants.EMAIL, defaultValue: "")!, score: "\(UserDefault.getFloat(key:Constants.DAILY_SCORE, defaultValue: 0))", grade: UserDefault.getString(key: Constants.DAILY_GRADE, defaultValue: "0")!, date: UserDefault.getString(key: Constants.LASTTIMEDATE, defaultValue: "\(timeNow)")!, total: "\(UserDefault.getString(key: Constants.OLD_VALUE)?.toInt() ?? 0)", remain: "\(UserDefault.getInt(key: Constants.REMAINING_CALORIE, defaultValue: 0))", breakfast: "\(UserDefault.getInt(key: Constants.TOTAL_BREAKFAST_CALORIE, defaultValue: 0))", lunch: "\(UserDefault.getInt(key: Constants.TOTAL_LUNCH_CALORIE, defaultValue: 0))", dinner: "\(UserDefault.getInt(key: Constants.TOTAL_DINNER_CALORIE, defaultValue: 0))", firstsnack: "\(UserDefault.getInt(key: Constants.TOTAL_FIRST_SNACK_CALORIE, defaultValue: 0))", secondsnack: "\(UserDefault.getInt(key: Constants.TOTAL_SECOND_SNACK_CALORIE, defaultValue: 0))", thirdsnack: "\(UserDefault.getInt(key: Constants.TOTAL_THIRD_SNACK_CALORIE, defaultValue: 0))", categories: totalcategoriesModelDataSource)
    }
}
