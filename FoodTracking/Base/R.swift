//
//  R.swift
//  EveraveUpdate
//
//  Created by Ubuntu on 12/18/19.
//  Copyright © 2019 Ubuntu. All rights reserved.
//

import Foundation
import Foundation

struct R {
    
    /* WORKOUT,
    PROGRAM,
    NUTRITION,
    NUTRITION_VIDEO,
    COACHING_VIDEO,
    COACHING,
    SEARCH1,
    SEARCH2,
    SEARCH3,*/
    
    struct string {
        static let SETTINGS = "Settings"
        static let WORKOUT = "WORKOUT"
        static let NUTRITION = "NUTRITION"
        static let NUTRITION_VIDEO = "NUTRITION_VIDEO"
        static let COACHING_VIDEO = "COACHING_VIDEO"
        static let COACHING = "COACHING"
        static let PROGRAM = "PROGRAM"
    }
}

enum ProductType: String{
    case workout = "WORKOUT"
    case program = "PROGRAM"
    case nutrition = "NUTRITION"
    case nutrition_video = "NUTRITION_VIDEO"
    case coaching_video = "COACHING_VIDEO"
    case coaching = "COACHING"
}

enum WorkoutVideoType: String{
    case individual_videos = "INDIVIDUAL_VIDEOS"
    case completed_programs = "COMPLETED_PROGRAMS"
    case workout_search = "WORKOUT_SEARCH"
}

enum NutritionType: String{
    case nutrition_products = "NUTRITION_PRODUCTS"
    case nutrition_videos = "NUTRITION_VIDEOS"
    case nutrition_search = "NUTRITION_SEARCH"
}

enum CoachingType: String{
    case coaching_products = "COACHING_PRODUCTS"
    case coaching_videos = "COACHING_VIDEOS"
    case coaching_search = "COACHING_SEARCH"
}

enum FragmentType: String{
    case workout = "WORKOUT"
    case nutrition = "NUTRITION"
    case coaching = "COACHING"
}
enum FoodctrType: String{
    case breakfast = "BreakFastVC"
    case firstsnack = "FirstSnackVC"
    case lunch = "LunchVC"
    case secondsnack = "SecondSnackVC"
    case dinner = "DinnerVC"
    case thirdsnack = "ThirdSnackVC"
}

enum RemoveType: String{
    case tableView = "tableView"
    case collectionView = "collectionView"
    case customfoodadd = "customfoodadd"
    case foodalert = "foodalert"
}
