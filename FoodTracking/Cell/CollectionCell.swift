//
//  CollectionCell.swift
//  FoodTracking
//
//  Created by Mac on 4/4/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher

class CollectionCell: UICollectionViewCell {
    
    @IBOutlet var lbl_topTitle: UILabel!
    @IBOutlet var lbl_bottomTitle: UILabel!
    @IBOutlet var lbl_content: UILabel!
    @IBOutlet var lbl_price: UILabel!
    @IBOutlet weak var btn_detail: UIButton!
    @IBOutlet weak var imv_thumb: UIImageView!
    @IBOutlet weak var btn_buy: UIButton!
    
    @IBOutlet weak var lbl_purchased: UILabel!
    @IBOutlet weak var btn_purchased: UIButton!
    @IBOutlet weak var lbl_caption4program: UILabel!
    @IBOutlet weak var btn_cancelSubscription: UIButton!
    //@IBOutlet weak var cons_toptitleHeight: NSLayoutConstraint!
    
    @IBOutlet weak var cons_btnBuyTop: NSLayoutConstraint!
    @IBOutlet weak var imageBtn: UIButton!
    
    var buyBlock: (() -> Void)?
    
    var entity:WcnModel!{
        
        didSet{
            self.lbl_topTitle.text = entity.title
            //self.cons_toptitleHeight.constant =  CGFloat(lbl_topTitle.text?.count ?? 50 / 50 * 20)
            self.lbl_bottomTitle.text = entity.subtitle
            self.lbl_content.text = entity.minidescription
            
            if entity.price != "0"{
                self.lbl_price.text = "BUY - " + "$" + entity.price
            }else{
                self.lbl_price.text = "FREE"
            }
            
            let url = URL(string: entity.image)
            imv_thumb.kf.setImage(with: url,placeholder: nil)
            
            if entity.status == "yes" && entity.uri == ""{// three button
                self.cons_btnBuyTop.constant = 70
            } else if entity.status == "yes" && entity.uri != "" {// purchased button
                self.cons_btnBuyTop.constant = 70
            }else{//only buy button
                self.cons_btnBuyTop.constant = 14
            }
        }
    }
    
    /*@IBAction func btnBuyAction(_ sender: Any) {
        if let buyBlock = buyBlock {
            buyBlock()
        }
    }*/
}
