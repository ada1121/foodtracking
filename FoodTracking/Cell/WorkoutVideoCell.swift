//
//  WorkoutVideoCell.swift
//  FoodTracking
//
//  Created by Mac on 4/13/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//
import Foundation
import UIKit
import Kingfisher

class WorkoutVideoCell: UICollectionViewCell {
    
    @IBOutlet var lbl_topTitle: UILabel!
    @IBOutlet var lbl_bottomTitle: UILabel!
    @IBOutlet var lbl_content: UILabel!
   
    @IBOutlet weak var btn_detail: UIButton!
    @IBOutlet weak var imv_thumb: UIImageView!
    
    @IBOutlet weak var btn_play: UIButton!
    @IBOutlet weak var btn_delete: UIButton!
    @IBOutlet weak var btn_cancelsubscription: UIButton!
    @IBOutlet weak var btn_programvides: UIButton!
    @IBOutlet weak var cons_toptitleHeight: NSLayoutConstraint!
    
    @IBOutlet weak var imageBtn: UIButton!
    
    var buyBlock: (() -> Void)?
    
    var entity:WcnModel!{
        
        didSet{
            self.lbl_topTitle.text = entity.title
            
            self.cons_toptitleHeight.constant = 50
            if lbl_topTitle.text!.count > 50{
                self.cons_toptitleHeight.constant =  CGFloat(lbl_topTitle.text?.count ?? 50 / 50 * 100)
            }
            
            self.lbl_bottomTitle.text = entity.subtitle
            self.lbl_content.text = entity.minidescription
            
            let url = URL(string: entity.image)
            imv_thumb.kf.setImage(with: url,placeholder: nil)
        }
    }
    
    /*@IBAction func btnBuyAction(_ sender: Any) {
        if let buyBlock = buyBlock {
            buyBlock()
        }
    }*/
}


