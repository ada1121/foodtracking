
//
//  CheckoutCell.swift
//  FoodTracking
//
//  Created by Mac on 4/9/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//


import Foundation
import UIKit
import Kingfisher

class CheckoutCell: UICollectionViewCell {
    
    @IBOutlet var lbl_topTitle: UILabel!
    @IBOutlet var lbl_price: UILabel!
    @IBOutlet weak var imv_thumb: UIImageView!
    @IBOutlet weak var btn_cancel: UIButton!
    @IBOutlet weak var txf_quantity: UITextField!
    
    var buyBlock: (() -> Void)?
    
    var entity:WcnModel!{
        
        didSet{
            self.lbl_topTitle.text = entity.title
            
            self.lbl_price.text = "PRICE - " + "$" + entity.price + "\n 30 DAYS SUBSCRIPTION"
            let url = URL(string: entity.image)
            imv_thumb.kf.setImage(with: url,placeholder: nil)
        }
    }
    
    /*@IBAction func btnBuyAction(_ sender: Any) {
        if let buyBlock = buyBlock {
            buyBlock()
        }
    }*/
}
