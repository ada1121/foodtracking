//
//  NormalCell.swift
//  FoodTracking
//
//  Created by Ubuntu on 12/29/19.
//  Copyright © 2019 Ubuntu. All rights reserved.
//

import Foundation
import UIKit

class NormalCell: UITableViewCell {
    
    @IBOutlet weak var foodName: UILabel!
    @IBOutlet weak var halfVal: UILabel!
    @IBOutlet weak var oneVal: UILabel!
    @IBOutlet weak var otherTxf: UITextField!
    
}


