//
//  CustomizationExampleUsedClasses.swift
//  ExpyTableView
//
//  Created by Okhan on 22/06/2017.
//  Copyright © 2017 CocoaPods. All rights reserved.
//

import UIKit
import ExpyTableView

//MARK: Used Table View Classes

class SelCatCell: UITableViewCell, ExpyTableViewHeaderCell{
    
    @IBOutlet weak var labelCatName: UILabel!
    @IBOutlet weak var imageViewArrow: UIImageView!
    
    func changeState(_ state: ExpyState, cellReuseStatus cellReuse: Bool) {
        
        switch state {
        case .willExpand:
            //print("")
            hideSeparator()
            arrowDown(animated: !cellReuse)
            
        case .willCollapse:
            //print("")
            arrowTop(animated: !cellReuse)
            
        case .didExpand:
            print("")
            
        case .didCollapse:
            showSeparator()
            //print("")
        }
    }
    
    private func arrowDown(animated: Bool) {
        UIView.animate(withDuration: (animated ? 0.3 : 0)) {
            self.imageViewArrow.transform = CGAffineTransform(rotationAngle: 0)
        }
    }
    
    private func arrowTop(animated: Bool) {
        UIView.animate(withDuration: (animated ? 0.3 : 0)) {
            self.imageViewArrow.transform = CGAffineTransform(rotationAngle: (CGFloat.pi / 1))
        }
    }
}



class DetailValTableViewCell: UITableViewCell {
    @IBOutlet weak var labelselectedName: UILabel!
    @IBOutlet weak var labelselectedvalue: UILabel!
}

class DetailValTableViewCell1: UITableViewCell {
    
    @IBOutlet weak var foodName: UILabel!
    @IBOutlet weak var cal: UILabel!
    @IBOutlet weak var carbs: UILabel!
    @IBOutlet weak var fat: UILabel!
    @IBOutlet weak var protein: UILabel!
    @IBOutlet weak var fiber: UILabel!
    
}

class BuyTableViewCell: UITableViewCell {}

extension UITableViewCell {

    func showSeparator() {
        DispatchQueue.main.async {
            self.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        }
    }
    
    func hideSeparator() {
        DispatchQueue.main.async {
            self.separatorInset = UIEdgeInsets(top: 0, left: self.bounds.size.width, bottom: 0, right: 0)
        }
    }
}
