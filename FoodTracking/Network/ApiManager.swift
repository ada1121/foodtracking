//
//  ApiManager.swift
//  PortTrucker
//
//  Created by PSJ on 9/12/19.
//  Copyright © 2019 LiMing. All rights reserved.
//

import Alamofire
import Foundation
import SwiftyJSON
import UIKit

// ************************************************************************//
                                // Food Tracking //
// ************************************************************************//


//let API = "http://psycheastrologyapp.com/api/"
let API = "http://54.144.130.66/api/"

let LOGIN = API + "signin"
let SIGNUP = API + "signup"
let FORGOT = API + "forgot"
let GETFOODLIST = API + "getFoodList"
let UPLOADMYCALORIE = API + "uploadMyCalorie"
let GETSAMPLELIST = API + "getSampleList"
let UPLOADHISTORY = API + "uploadHistory"
let GETHISTORY = API + "getHistory"
let GETHISTORYDETAIL = API + "getHistoryDetail"
let GETMYDETIALS = API + "getMydetails"
let REMOVEMYCALORIE = API + "removeMyCalorie"
let WORKOUTVIDEO = API + "getWorkoutVideos"
let GETNUTRIONIST = API + "getNutritionists"
let GETCOATCHINGSERVICE = API + "getCoachingServices"
let SETCHECKOUT = API + "setCheckout"
let GETWORKOUTVIDEOCHECKOUT = API + "getWorkoutVideoCheckout"
let GETNUTRITIONISTCHECKOUT = API + "getNutritionistCheckout"
let GETCOATCHINGSERVICECHECKOUT = API + "getCoachingServiceCheckout"
let SETNUTRITIONISTCHECKOUT = API + "setNutritionistCheckout"
let SETCOACHINGSERVIECHECKOUT = API + "setCoachingServiceCheckout"
let REMOVECHECKOUT = API + "removeCheckout"
let REMOVENUTRIONISTCHECKOUT = API + "removeNutritionistCheckout"
let REMOVECOATCHINGSERVICECHECKOUT = API + "removeCoachingServiceCheckout"
let PAYMENTPROCESS = API + "paymentProcess"
let SETNUTRITIONIST = API + "setNutritionist"
let SETNEWCOATCHINGSERVICE = API + "setNewCoachingService"
let SETWORKOUTVIDEO = API + "setWorkoutVideo"
let REMOVEWORKOUTVIDEO = API + "removeWorkoutVideo"
let REMOVEVIDEO = API + "removeVideo"
let GETPROGRAMVIDEOS = API + "getProgramVideos"
let SETCOATCHINGSERVICE = API + "setCoachingService"
let UPDATEDNUTRITIONISTCHECKOUT = API + "updateNutritionistCheckout"
let SETWORKOUTVIDEOURI = API + "setWorkoutVideoUri"
let SETPROGRAMVIDEOURI = API + "setProgramVideoUri"
let REMOVEPROGRAMVIDEO1 = API + "removeProgramVideo1"
let REGISTERTOKEN = API + "registerToken"
let REMOVEALLPROGRAMVIDEO = API + "removeAllProgramVideo"
let ONVERYFYDEVICE = API + "verifyDevice"
let GETVIDEOS = API + "getVideos"
let GETLOCALVIDEO = API + "getLocalVideo"
let GETCHECKOUT = API + "getCheckout"
let SETPURCHASE = API + "setPurchase"
let SETVIDEOURI = API + "setVideoUri"
let REMOVESUBSCRIPTION = API + "removeSubscription"

struct PARAMS {
    
    static let STATUS = "result_code"
    static let TRUE = "0"
    static let USERINFO = "user_info"
    static let EMAIL = "email"
    static let USERNAME = "username"
    static let PASSWORD = "password"
    static let FOODLIST = "food_list"
    static let ID = "id"
    static let TITLE = "title"
    static let RECOMMENDED = "recomended"
    static let CALORIELIST = "calorie_list"
    static let CAL = "cal"
    static let CABS = "carbs"
    static let FAT = "fat"
    static let PROTEIN = "protein"
    static let FIBER = "fiber"
    static let USERID = "user_id"
    static let CATEGORYID = "category_id"
    static let MYCALORIEID = "mycalorie_id"
    static let SAMPLELIST = "sample_list"
    static let TB_SAMPLEBREAKFAST = "tb_samplebreakfast"
    static let TB_SAMPLEFIRSTSNACK = "tb_samplefirstsnack"
    static let TB_SAMPLELUNCH = "tb_samplelunch"
    static let TB_SAMPLEBSECONDSNACK = "tb_samplesecondsnack"
    static let TB_SAMPLEDINNER = "tb_sampledinner"
    static let TB_SAMPLETHIRDSNACK = "tb_samplethirdsnack"
    static let TABLENAME = "table_name"
    static let HITORYINFO = "history_info"
    static let HISTORYDETAILINFO = "historydetail_info"
    static let HISTORYID = "history_id"
    static let MEALTYPE = "meal_type"
    static let PINCODE = "pincode"
    static let QUANTITY = "quantity"
    
    static let SCORE = "score"
    static let GRADE = "grade"
    static let DATE = "date"
    static let TOTAL = "total"
    static let REMAIN = "remain"
    static let BREAKFAST = "breakfast"
    static let LUNCH = "lunch"
    static let DINNER = "dinner"
    static let FIRSTSNACK = "firstsnack"
    static let SECONDSNACK = "secondsnack"
    static let THIRDSNACK = "thirdsnack"
    static let CATGORIES = "categories"
    static let FOODTYPE = "food_type"
    static let SUBCATEGORIES = "subcategories"
    static let APPROVE = "approve"
    
    // for video part
    static let SUBTITLE = "subtitle"
    static let MINIDESCRIPTION = "minidescription"
    static let FULLDESCRIPTION = "fulldescription"
    static let PRICE = "price"
    static let PERIOD = "period"
    static let IMAGE = "image"
    static let VIDEO = "video"
    static let VIDEOSTATUS = "status"
    static let ENDDAY = "endDay"
    static let URI = "uri"
    static let PHONE = "phone"
    static let NAME = "name"
    static let TIMES = "times"
    static let WORKOUTVIDEOLIST = "workoutvideo_list"
    static let NUTRIONISTLIST = "nutritionist_list"
    static let COATCHINGSERVICELIST = "coachingservice_list"
    static let CHECKOUT_ID = "checkout_id"
    static let CHECKOUTLIST = "checkout_list"
    
    // new added params
    static let DEVICETYPE = "deviceType"
    static let DEVICETOKEN = "deviceToken"
    static let VIDEO_LIST = "video_list"
}
struct TYPES {
    static let WORKOUTVIDEO = "WorkoutVideo"
    static let NUTRITIONIST = "Nutritionist"
    static let COATCHINGSERVICE = "CoachingService"
}


class ApiManager {
    
    class func login(useremail : String, password : String, completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        
        let params = ["email" : useremail, "password" : password, PARAMS.DEVICETYPE : Constants.deviceType, PARAMS.DEVICETOKEN : Constants.deviceToken] as [String : Any]
        Alamofire.request(LOGIN, method:.post, parameters:params)
        .responseJSON { response in
            switch response.result {
                case .failure:
                    completion(false, nil)
                case .success(let data):
                    let dict = JSON(data)
                    let status = dict[PARAMS.STATUS].stringValue // 0-success,1-Non Exist Email ,2- password incorrect, 3- new device
                    let user_info = dict[PARAMS.USERINFO].object
                    let userdata = JSON(user_info)
                    if status == PARAMS.TRUE {
                        completion(true, userdata)
                    }else if status == "1"{
                        completion(false, status)
                    }else if status == "2"{
                        completion(false, status)
                    }
                    else {
                        completion(false, status)
                    }
            }
        }
    }
     
    class func signup( username : String, email : String, password : String, completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
          let requestURL = SIGNUP
                 Alamofire.upload(
                     multipartFormData: { multipartFormData in
                        multipartFormData.append( username.data(using:String.Encoding.utf8)!, withName: PARAMS.USERNAME)
                        multipartFormData.append( email.data(using:String.Encoding.utf8)!, withName: PARAMS.EMAIL)
                        multipartFormData.append( password.data(using:String.Encoding.utf8)!, withName: PARAMS.PASSWORD)

                 },
                     to: requestURL,
                     encodingCompletion: { encodingResult in
                         switch encodingResult {
                             case .success(let upload, _, _):
                                 upload.responseJSON { response in
                                     switch response.result {
                                         case .failure: completion(false, nil)
                                         case .success(let data):
                                             let dict = JSON(data)
                                             print("")
                                             let status = dict[PARAMS.STATUS].stringValue
                                             if status == PARAMS.TRUE {
                                                 completion(true, status)
                                             } else {
                                                 completion(false, status)
                                             }
                                         }
                                 }
                         case .failure( _):
                                 print("")
                                 completion(false, nil)
                             }
                 })
    }

    class func forgot(email : String, completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
            let params = [PARAMS.EMAIL: email] as [String : Any]
            Alamofire.request( FORGOT, method:.post, parameters:params)
                .responseJSON { response in
                    switch response.result {
                        case .failure:
                            completion(false, nil)
                        case .success(let data):
                            let dict = JSON(data)
                            let status = dict[PARAMS.STATUS].stringValue
                            let pincode = dict["pincode"].stringValue
                            if status == PARAMS.TRUE {
                                completion(true, pincode)
                            } else {
                                completion(false, status)
                            }
                        }
            }
        }
    
    class func getFoodList(user_id : Int,completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        let params = ["user_id" : user_id] as [String : Any]
        Alamofire.request(GETFOODLIST,method:.post, parameters:params )
        .responseJSON { response in
            switch response.result {
                case .failure:
                    completion(false, nil)
                case .success(let data):
                    let dict = JSON(data)
                    let status = dict[PARAMS.STATUS].stringValue // 0,1,2
                    let food_cat = dict[PARAMS.FOODLIST].arrayObject
                    let fooddata = JSON(food_cat as Any)
                    if status == PARAMS.TRUE {
                        //print("this is fooddata =====",fooddata as Any)
                        completion(true, fooddata)
                        
                    } else {
                        completion(false, status)
                    }
                }
        }
    }
    
    class func removeMyFood(mycalorie_id : Int,completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        let params = [PARAMS.MYCALORIEID : mycalorie_id] as [String : Any]
        Alamofire.request(REMOVEMYCALORIE,method:.post, parameters:params )
        .responseJSON { response in
            switch response.result {
                case .failure:
                    completion(false, nil)
                case .success(let data):
                    let dict = JSON(data)
                    let status = dict[PARAMS.STATUS].intValue // 0,1,2
                    
                    if status == 0 {
                        //print("this is fooddata =====",fooddata as Any)
                        completion(true, status)
                        
                    } else {
                        completion(false, status)
                    }
                }
        }
    }
    
    class func getSampleLists(_ tableName : String,completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        let params = ["table_name": tableName] as [String : Any]
        Alamofire.request(GETSAMPLELIST,method:.post, parameters:params )
        .responseJSON { response in
            switch response.result {
                case .failure:
                    completion(false, nil)
                case .success(let data):
                    let dict = JSON(data)
                    //print("This is first serverdata",dict)
                    let status = dict[PARAMS.STATUS].stringValue // 0,1,2
                    let sampleListJSON = dict[PARAMS.SAMPLELIST].arrayObject
                    let sampleList = JSON(sampleListJSON as Any)
                    ///print("this is serversamplelist",sampleList)
                    if status == PARAMS.TRUE {
                        //print("this is fooddata =====",fooddata as Any)
                        completion(true, sampleList)
                    } else {
                        completion(false, status)
                    }
                }
        }
    }
    
    class func uploadCustomFoodList(user_id : Int ,category_id : String,title : String, cal: String, carbs: String, fat: String, protein: String, fiber: String,completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        let params = [PARAMS.USERID : user_id,PARAMS.CATEGORYID : category_id,PARAMS.TITLE : title,PARAMS.CAL : cal, PARAMS.CABS: carbs,PARAMS.FAT : fat,PARAMS.PROTEIN : protein,PARAMS.FIBER :fiber ] as [String : Any]
        Alamofire.request(UPLOADMYCALORIE,method:.post, parameters:params )
        .responseJSON { response in
            switch response.result {
                case .failure:
                    completion(false, nil)
                case .success(let data):
                    let dict = JSON(data)
                    let status = dict[PARAMS.STATUS].stringValue // 0,1,2
                    let mycalorieID = dict[PARAMS.MYCALORIEID].intValue
                    
                    if status == PARAMS.TRUE {
                        //print("this is fooddata =====",fooddata as Any)
                        completion(true, mycalorieID)
                        
                    } else {
                        completion(false, status)
                    }
                }
        }
    }
    
    class func uploadHistory( foodHistory:TotalFoodHistoryModel!,completion: @escaping ( _ success: Bool, _ response: Any? ) -> ()) {
        
        var foodHistoryDic : [String: Any] = foodHistory.toDictionary()
               var categoriesDic = [String: Any]()
               var categoriesDicArray = [[String: Any]]()
               var subcategoriesDic = [String: Any]()
               var subcategoriesDicArray = [[String: Any]]()
               
               categoriesDicArray.removeAll()
               
               for one in foodHistory.categories{
                subcategoriesDicArray.removeAll()
                   for two in one.subcategories{
                       subcategoriesDic = two.toDictionary() as! [String : String]
                       subcategoriesDicArray.append(subcategoriesDic)
                   }
                   
                  categoriesDic = one.toDictionary()
                  categoriesDic["subcategories"] = subcategoriesDicArray
                  categoriesDicArray.append(categoriesDic)
               }
               
               foodHistoryDic["categories"] = categoriesDicArray
 
        let params = foodHistoryDic as [String : Any]
      print(params)
        Alamofire.request(UPLOADHISTORY,method:.post, parameters:params,encoding: JSONEncoding.default)
           .responseJSON { response in
                 switch response.result {
                   case .failure:
                       completion(false, nil)
                   case .success(let data):
                       let dict = JSON(data)
                       let status = dict[PARAMS.STATUS].stringValue // 0,1,2
                       
                       
                       if status == PARAMS.TRUE {
                           //print("this is fooddata =====",fooddata as Any)
                           completion(true, status)
                           
                       } else {
                           completion(false, status)
                       }
                   }
         }
      }
   
    
    class func getHistory(email : String,completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        let params = [PARAMS.EMAIL : email] as [String : Any]
           Alamofire.request(GETHISTORY,method:.post, parameters:params )
           .responseJSON { response in
               switch response.result {
                   case .failure:
                       completion(false, nil)
                   case .success(let data):
                       let dict = JSON(data)
                       let status = dict[PARAMS.STATUS].stringValue // 0,1,2
                       let history = dict[PARAMS.HITORYINFO].arrayObject
//                       let history = JSON(history_data as Any)
                       if status == PARAMS.TRUE {
                           //print("this is fooddata =====",fooddata as Any)
                           completion(true, history)
                           
                       } else {
                           completion(false, status)
                       }
            }
        }
    }
    
     class func getHistoryDetail(history_id : String,meal_type : String ,completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        let params = [PARAMS.HISTORYID: history_id,PARAMS.MEALTYPE:meal_type] as [String : Any]
               Alamofire.request(GETHISTORYDETAIL,method:.post, parameters:params )
               .responseJSON { response in
                   switch response.result {
                       case .failure:
                           completion(false, nil)
                       case .success(let data):
                           let dict = JSON(data)
                           let status = dict[PARAMS.STATUS].stringValue // 0,1,2
                           let historyDetail = dict[PARAMS.HISTORYDETAILINFO].arrayObject
    //                       let history = JSON(history_data as Any)
                           if status == PARAMS.TRUE {
                               //print("this is fooddata =====",fooddata as Any)
                               completion(true, historyDetail)
                               
                           } else {
                               completion(false, status)
                           }
                       }
               }
       }
    
    class func getMydetails(completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        
        let email = UserDefault.getString(key: PARAMS.EMAIL)
       
        let params = [PARAMS.EMAIL :email ?? "", PARAMS.DEVICETOKEN: Constants.deviceToken, PARAMS.DEVICETYPE : Constants.deviceType] as [String : Any]
        
               Alamofire.request(GETMYDETIALS,method:.post, parameters:params )
               .responseJSON { response in
                   switch response.result {
                       case .failure:
                           completion(false, nil)
                       case .success(let data):
                           let dict = JSON(data)
                           let status = dict[PARAMS.STATUS].intValue // 0,1,2
                           let userinfo = dict[PARAMS.USERINFO].object
                           let userinfoJSON = JSON(userinfo as Any)
                           if status == 0 {
                               print("this is userinfoJSON =====",userinfoJSON as Any)
                               completion(true, userinfoJSON)
                               
                           } else {
                               completion(false, status)
                           }
                       }
               }
   }
    
    class func getWorkoutVideo(user_id : Int,completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        let params = ["user_id" : user_id] as [String : Any]
        Alamofire.request(WORKOUTVIDEO,method:.post, parameters:params )
        .responseJSON { response in
            switch response.result {
                case .failure:
                    completion(false, nil)
                case .success(let data):
                    let dict = JSON(data)
                    let status = dict[PARAMS.STATUS].stringValue // 0,1,2
                    let workout_videos = dict[PARAMS.WORKOUTVIDEOLIST].arrayObject
                    let workoutdata = JSON(workout_videos as Any)
                    if status == PARAMS.TRUE {
                        //print("this is workout_videos =====",workoutdata as Any)
                        completion(true, workoutdata)
                        
                    } else {
                        completion(false, status)
                    }
                }
        }
    }
    
    class func getNutrionist(user_id : Int,completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        let params = ["user_id" : user_id] as [String : Any]
        Alamofire.request(GETNUTRIONIST,method:.post, parameters:params )
        .responseJSON { response in
            switch response.result {
                case .failure:
                    completion(false, nil)
                case .success(let data):
                    let dict = JSON(data)
                    let status = dict[PARAMS.STATUS].stringValue // 0,1,2
                    let nutrionist = dict[PARAMS.NUTRIONISTLIST].arrayObject
                    let nutrionistdata = JSON(nutrionist as Any)
                    if status == PARAMS.TRUE {
                        //print("this is workout_videos =====",workoutdata as Any)
                        completion(true, nutrionistdata)
                        
                    } else {
                        completion(false, status)
                    }
                }
        }
    }
    
    class func getCoachingService(user_id : Int,completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        let params = ["user_id" : user_id] as [String : Any]
        Alamofire.request(GETCOATCHINGSERVICE,method:.post, parameters:params )
        .responseJSON { response in
            switch response.result {
                case .failure:
                    completion(false, nil)
                case .success(let data):
                    let dict = JSON(data)
                    let status = dict[PARAMS.STATUS].stringValue // 0,1,2
                    let coachingserviceList = dict[PARAMS.COATCHINGSERVICELIST].arrayObject
                    let coachingserviceListdata = JSON(coachingserviceList as Any)
                    if status == PARAMS.TRUE {
                        //print("this is workout_videos =====",workoutdata as Any)
                        completion(true, coachingserviceListdata)
                        
                    } else {
                        completion(false, status)
                    }
                }
        }
    }
    
    class func getWorkoutVideoCheckout(user_id : Int,completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        let params = ["user_id" : user_id] as [String : Any]
        Alamofire.request(GETWORKOUTVIDEOCHECKOUT,method:.post, parameters:params )
        .responseJSON { response in
            switch response.result {
                case .failure:
                    completion(false, nil)
                case .success(let data):
                    let dict = JSON(data)
                    let status = dict[PARAMS.STATUS].stringValue// 0,1,2
                    
                    let checkoutlist = dict[PARAMS.CHECKOUTLIST].arrayObject
                    let checkoutlistdata = JSON(checkoutlist as Any)
                    if status == PARAMS.TRUE {
                        //print("this is workout_videos =====",workoutdata as Any)
                        completion(true, checkoutlistdata)
                        
                    } else {
                        completion(false, status)
                    }
                }
        }
    }
    class func getNutritionistCheckout(user_id : Int,completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        let params = ["user_id" : user_id] as [String : Any]
        Alamofire.request(GETNUTRITIONISTCHECKOUT,method:.post, parameters:params )
        .responseJSON { response in
            switch response.result {
                case .failure:
                    completion(false, nil)
                case .success(let data):
                    let dict = JSON(data)
                    let status = dict[PARAMS.STATUS].stringValue// 0,1,2
                    
                    let checkoutlist = dict[PARAMS.CHECKOUTLIST].arrayObject
                    let checkoutlistdata = JSON(checkoutlist as Any)
                    if status == PARAMS.TRUE {
                        //print("this is workout_videos =====",workoutdata as Any)
                        completion(true, checkoutlistdata)
                        
                    } else {
                        completion(false, status)
                    }
                }
        }
    }
    
    class func getCoachingServiceCheckout(user_id : Int,completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        let params = ["user_id" : user_id] as [String : Any]
        Alamofire.request(GETCOATCHINGSERVICECHECKOUT,method:.post, parameters:params )
        .responseJSON { response in
            switch response.result {
                case .failure:
                    completion(false, nil)
                case .success(let data):
                    let dict = JSON(data)
                    let status = dict[PARAMS.STATUS].stringValue// 0,1,2
                    
                    let checkoutlist = dict[PARAMS.CHECKOUTLIST].arrayObject
                    let checkoutlistdata = JSON(checkoutlist as Any)
                    if status == PARAMS.TRUE {
                        //print("this is workout_videos =====",workoutdata as Any)
                        completion(true, checkoutlistdata)
                        
                    } else {
                        completion(false, status)
                    }
                }
        }
    }
    
    class func setCheckout(user_id : Int,product_id: Int, quantity: Int,completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        let params = ["user_id" : user_id,"product_id" : product_id, "quantity": quantity] as [String : Any]
        Alamofire.request(SETCHECKOUT,method:.post, parameters:params )
        .responseJSON { response in
            switch response.result {
                case .failure:
                    completion(false, nil)
                case .success(let data):
                    let dict = JSON(data)
                    let status = dict[PARAMS.STATUS].stringValue// 0,1,2
                    let checkout_id = dict[PARAMS.CHECKOUT_ID].stringValue
                    
                    if status == PARAMS.TRUE {
                        //print("this is workout_videos =====",workoutdata as Any)
                        completion(true, checkout_id)
                        
                    } else {
                        completion(false, status)
                    }
                }
        }
    }
    
    class func setNutritionistCheckout(user_id : Int,product_id: Int, quantity: Int,completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        let params = ["user_id" : user_id, "product_id": product_id, "quantity":quantity ] as [String : Any]
        Alamofire.request(SETNUTRITIONISTCHECKOUT,method:.post, parameters:params )
        .responseJSON { response in
            switch response.result {
                case .failure:
                    completion(false, nil)
                case .success(let data):
                    let dict = JSON(data)
                    let status = dict[PARAMS.STATUS].stringValue// 0,1,2
                    let checkout_id = dict["checkout_id"].stringValue
                    
                    if status == PARAMS.TRUE {
                        completion(true, checkout_id)
                        
                    } else {
                        completion(false, status)
                    }
                }
        }
    }
    
    class func setCoatchingServiceCheckout(user_id : Int,product_id: Int, quantity: Int,default_times: Int,completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        let params = ["user_id" : user_id, "product_id": product_id, "quantity":quantity, "default_times": default_times ] as [String : Any]
        Alamofire.request(SETCOACHINGSERVIECHECKOUT,method:.post, parameters:params )
        .responseJSON { response in
            switch response.result {
                case .failure:
                    completion(false, nil)
                case .success(let data):
                    let dict = JSON(data)
                    let status = dict[PARAMS.STATUS].stringValue// 0,1,2
                    let checkout_id = dict["checkout_id"].stringValue
                    
                    if status == PARAMS.TRUE {
                        completion(true, checkout_id)
                        
                    } else {
                        completion(false, status)
                    }
                }
        }
    }
    
    class func removeCheckout(user_id : Int,product_id: Int,completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        let params = ["user_id" : user_id, "product_id": product_id ] as [String : Any]
        Alamofire.request(REMOVECHECKOUT,method:.post, parameters:params )
        .responseJSON { response in
            switch response.result {
                case .failure:
                    completion(false, nil)
                case .success(let data):
                    let dict = JSON(data)
                    let status = dict[PARAMS.STATUS].stringValue// 0,1,2
                    
                    if status == PARAMS.TRUE {
                        completion(true, status)
                        
                    } else {
                        completion(false, status)
                    }
                }
        }
    }
    
    class func removeNutrionistCheckout(user_id : Int,product_id: Int,completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        let params = ["user_id" : user_id, "product_id": product_id ] as [String : Any]
        Alamofire.request(REMOVENUTRIONISTCHECKOUT,method:.post, parameters:params )
        .responseJSON { response in
            switch response.result {
                case .failure:
                    completion(false, nil)
                case .success(let data):
                    let dict = JSON(data)
                    let status = dict[PARAMS.STATUS].stringValue// 0,1,2
                    
                    if status == PARAMS.TRUE {
                        completion(true, status)
                        
                    } else {
                        completion(false, status)
                    }
                }
        }
    }
    
    class func removeCoatchingServiceCheckout(user_id : Int,product_id: Int,completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        let params = ["user_id" : user_id, "product_id": product_id ] as [String : Any]
        Alamofire.request(REMOVECOATCHINGSERVICECHECKOUT,method:.post, parameters:params )
        .responseJSON { response in
            switch response.result {
                case .failure:
                    completion(false, nil)
                case .success(let data):
                    let dict = JSON(data)
                    let status = dict[PARAMS.STATUS].stringValue// 0,1,2
                    
                    if status == PARAMS.TRUE {
                        completion(true, status)
                        
                    } else {
                        completion(false, status)
                    }
                }
        }
    }
    
    /* .addBodyParameter("pay_type", "manual")
    .addBodyParameter("productID", productID)
    .addBodyParameter("type", String.valueOf(fragmentType))
    .addBodyParameter("user_id", Commons.thisUser.getId())
    .addBodyParameter("country", country)
    .addBodyParameter("first_name", firstName)
    .addBodyParameter("last_name", lastName)
    .addBodyParameter("address", address)
    .addBodyParameter("city", city)
    .addBodyParameter("state", state)
    .addBodyParameter("zipcode", zipCode)
    .addBodyParameter("user_email", email)
    .addBodyParameter("user_phone", phone)
    .addBodyParameter("cardNumber", cardNumber)
    .addBodyParameter("expirationDate", expirationDate)
    .addBodyParameter("cardCode", cardCode)
    .addBodyParameter("amount", String.valueOf(subTotal))
    .addBodyParameter("description", description)*/
    class func paymentProcess(pay_type: String, productID : String, type: String, user_id : Int,first_name: String,last_name:String, user_email: String, user_phone: String, country: String, address: String,city: String,state: String,zipcode: String,cardNumber: String, expirationDate: String, cardCode:String, amount: String,description: String, completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        
        let params = ["pay_type":pay_type, "productID":productID, "type" : type, "user_id": user_id, "first_name": first_name, "last_name": last_name, "user_email": user_email, "user_phone": user_phone, "country": country, "address": address, "city": city, "state": state, "zipcode":zipcode,"cardNumber":cardNumber, "expirationDate":expirationDate, "cardCode" : cardCode, "amount" : amount,"description" : description] as [String : Any]
        
        Alamofire.request(PAYMENTPROCESS,method:.post, parameters:params )
        .responseJSON { response in
            switch response.result {
                case .failure:
                    completion(false, nil)
                case .success(let data):
                    let dict = JSON(data)
                    let status = dict[PARAMS.STATUS].stringValue// 0,1,2
                    
                    if status == PARAMS.TRUE {
                        completion(true, status)
                        
                    } else {
                        completion(false, status)
                    }
                }
        }
    }
    /*AndroidNetworking.post(Constants.SERVER_URL + "setNutritionist")
    .addBodyParameter("user_id", Commons.thisUser.getId())
    .addBodyParameter("nutritionist_id", nutritionistId)
    .addBodyParameter("status", "yes")
    .addBodyParameter("email",email)
    .setTag(this)*/
    class func setNutritionist(user_id : Int,nutritionist_id: String,status: String,email: String,completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        let params = ["user_id" : user_id, "nutritionist_id": nutritionist_id, "status":status,"email" :email  ] as [String : Any]
        Alamofire.request(SETNUTRITIONIST,method:.post, parameters:params )
        .responseJSON { response in
            switch response.result {
                case .failure:
                    completion(false, nil)
                case .success(let data):
                    let dict = JSON(data)
                    let status = dict[PARAMS.STATUS].stringValue// 0,1,2
                    
                    if status == PARAMS.TRUE {
                        completion(true, status)
                        
                    } else {
                        completion(false, status)
                    }
                }
        }
    }
    /*setNewCoachingService
     .addBodyParameter("user_id", Commons.thisUser.getId())
    .addBodyParameter("coachingservice_id", coachingServiceId)
    .addBodyParameter("phone", phone)
    .addBodyParameter("times", defaultTimes)*/
    class func setNewCoatchingService(user_id : Int,coachingservice_id: String,phone: String,times: String,completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        let params = ["user_id" : user_id, "coachingservice_id": coachingservice_id, "phone":phone ,"times":times] as [String : Any]
        Alamofire.request(SETNEWCOATCHINGSERVICE,method:.post, parameters:params )
        .responseJSON { response in
            switch response.result {
                case .failure:
                    completion(false, nil)
                case .success(let data):
                    let dict = JSON(data)
                    let status = dict[PARAMS.STATUS].stringValue// 0,1,2
                    
                    if status == PARAMS.TRUE {
                        completion(true, status)
                        
                    } else {
                        completion(false, status)
                    }
                }
        }
    }
    
    class func setCoatchingService(user_id : Int,coachingservice_id: String,times: String,completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        let params = ["user_id" : user_id, "coachingservice_id": coachingservice_id,"times":times] as [String : Any]
        Alamofire.request(SETCOATCHINGSERVICE,method:.post, parameters:params )
        .responseJSON { response in
            switch response.result {
                case .failure:
                    completion(false, nil)
                case .success(let data):
                    let dict = JSON(data)
                    let status = dict[PARAMS.STATUS].stringValue// 0,1,2
                    
                    if status == PARAMS.TRUE {
                        completion(true, status)
                        
                    } else {
                        completion(false, status)
                    }
                }
        }
    }
    
    /*AndroidNetworking.post(Constants.SERVER_URL + "setWorkoutVideo")
    .addBodyParameter("user_id", Commons.thisUser.getId())
    .addBodyParameter("workoutvideo_id", workoutvideoId)
    .addBodyParameter("endDay",endDay)
    .addBodyParameter("status", "yes")*/
    class func setWorkoutVideo(user_id : Int,workoutvideo_id: String,endDay: String,status: String,completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        let params = ["user_id" : user_id, "workoutvideo_id": workoutvideo_id, "endDay":endDay ,"status":status] as [String : Any]
        Alamofire.request(SETWORKOUTVIDEO,method:.post, parameters:params )
        .responseJSON { response in
            switch response.result {
                case .failure:
                    completion(false, nil)
                case .success(let data):
                    let dict = JSON(data)
                    let status = dict[PARAMS.STATUS].stringValue// 0,1,2
                    
                    if status == PARAMS.TRUE {
                        completion(true, status)
                        
                    } else {
                        completion(false, status)
                    }
                }
        }
    }
    
    /*AndroidNetworking.post(Constants.SERVER_URL + "removeWorkoutVideo")
    .addBodyParameter("user_id", Commons.thisUser.getId())
    .addBodyParameter("workoutvideo_id", String.valueOf(workoutvideoId))
    .setTag(this)*/
    
    class func removeWorkoutVideo(user_id : Int,workoutvideo_id: String,completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        let params = ["user_id" : user_id, "workoutvideo_id": workoutvideo_id] as [String : Any]
        Alamofire.request(REMOVEWORKOUTVIDEO,method:.post, parameters:params )
        .responseJSON { response in
            switch response.result {
                case .failure:
                    completion(false, nil)
                case .success(let data):
                    let dict = JSON(data)
                    let status = dict[PARAMS.STATUS].stringValue// 0,1,2
                    
                    if status == PARAMS.TRUE {
                        completion(true, status)
                        
                    } else {
                        completion(false, status)
                    }
                }
        }
    }
    
    class func removeWorkoutVideo1(user_id : Int,product_id: String,completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        let params = ["user_id" : user_id, "product_id": product_id] as [String : Any]
        Alamofire.request(REMOVEVIDEO,method:.post, parameters:params )
        .responseJSON { response in
            switch response.result {
                case .failure:
                    completion(false, nil)
                case .success(let data):
                    let dict = JSON(data)
                    let status = dict[PARAMS.STATUS].stringValue// 0,1,2
                    
                    if status == PARAMS.TRUE {
                        completion(true, status)
                        
                    } else {
                        completion(false, status)
                    }
                }
        }
    }
    
    /*AndroidNetworking.post(Constants.SERVER_URL + "getProgramVideos")
    .addBodyParameter("user_id", Commons.thisUser.getId())
    .addBodyParameter("workoutvideo_id", String.valueOf(workoutvideo_id))
    .setTag(this)*/
    class func getProgramVideos(user_id : Int,workoutvideo_id: String,completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        let params = ["user_id" : user_id, "product_id": workoutvideo_id] as [String : Any]
        Alamofire.request(GETPROGRAMVIDEOS,method:.post, parameters:params )
        .responseJSON { response in
            switch response.result {
                case .failure:
                    completion(false, nil)
                case .success(let data):
                    let dict = JSON(data)
                    let status = dict[PARAMS.STATUS].stringValue// 0,1,2
                    
                    let programvideo_list = dict["programvideo_list"].arrayObject
                    let programvideolist = JSON(programvideo_list as Any)
                    if status == PARAMS.TRUE {
                        //print("this is workout_videos =====",workoutdata as Any)
                        completion(true, programvideolist)
                        
                    } else {
                        completion(false, status)
                    }
                }
        }
    }
    
    /*AndroidNetworking.post(Constants.SERVER_URL + "updateNutritionistCheckout")
    .addBodyParameter("user_id", Commons.thisUser.getId())
    .addBodyParameter("product_id", String.valueOf(productId))
    .addBodyParameter("quantity", quantity)*/
    class func updateNutritionistCheckout(user_id : Int,product_id: String,quantity: String, completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        let params = ["user_id" : user_id, "product_id": product_id,"quantity" : quantity] as [String : Any]
        Alamofire.request(UPDATEDNUTRITIONISTCHECKOUT,method:.post, parameters:params )
        .responseJSON { response in
            switch response.result {
                case .failure:
                    completion(false, nil)
                case .success(let data):
                    let dict = JSON(data)
                    let status = dict[PARAMS.STATUS].stringValue// 0,1,2
                    if status == PARAMS.TRUE {
                        //print("this is workout_videos =====",workoutdata as Any)
                        completion(true,status )
                        
                    } else {
                        completion(false, status)
                    }
                }
        }
    }
   
    
    /*AndroidNetworking.post(Constants.SERVER_URL + "setWorkoutVideoUri")
    .addBodyParameter("user_id", Commons.thisUser.getId())
    .addBodyParameter("workoutvideo_id", String.valueOf(workoutvideoId))
    .addBodyParameter("uri", localUrl)*/
    
    class func setWorkoutVideoUri(user_id : Int,workoutvideo_id: String,uri: String, completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        let params = ["user_id" : user_id, "workoutvideo_id": workoutvideo_id,"uri" : uri] as [String : Any]
        Alamofire.request(SETWORKOUTVIDEOURI,method:.post, parameters:params )
        .responseJSON { response in
            switch response.result {
                case .failure:
                    completion(false, nil)
                case .success(let data):
                    let dict = JSON(data)
                    let status = dict[PARAMS.STATUS].stringValue// 0,1,2
                    if status == PARAMS.TRUE {
                        //print("this is workout_videos =====",workoutdata as Any)
                        completion(true,status )
                        
                    } else {
                        completion(false, status)
                    }
                }
        }
    }
    /*void removeProgramVideo1(int programvideoId){
    AndroidNetworking.post(Constants.SERVER_URL + "removeProgramVideo1")
            .addBodyParameter("user_id", Commons.thisUser.getId())
            .addBodyParameter("programvideo_id", String.valueOf(programvideoId))
            .setTag(this)*/
    class func removeProgramVideo1(user_id : Int,programvideo_id: String,completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        let params = ["user_id" : user_id, "programvideo_id": programvideo_id] as [String : Any]
        Alamofire.request(REMOVEPROGRAMVIDEO1,method:.post, parameters:params )
        .responseJSON { response in
            switch response.result {
                case .failure:
                    completion(false, nil)
                case .success(let data):
                    let dict = JSON(data)
                    let status = dict[PARAMS.STATUS].stringValue// 0,1,2
                    
                    if status == PARAMS.TRUE {
                        completion(true, status)
                        
                    } else {
                        completion(false, status)
                    }
                }
        }
    }
    /*AndroidNetworking.post(Constants.SERVER_URL + "setProgramVideoUri")
    .addBodyParameter("user_id", Commons.thisUser.getId())
    .addBodyParameter("programvideo_id", String.valueOf(programvideoId))
    .addBodyParameter("uri", localUrl)*/
    class func setProgramVideoUri(user_id : Int,programid: String,uri: String, completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        let params = ["user_id" : user_id, "programid": programid,"uri" : uri] as [String : Any]
        Alamofire.request(SETPROGRAMVIDEOURI,method:.post, parameters:params )
        .responseJSON { response in
            switch response.result {
                case .failure:
                    completion(false, nil)
                case .success(let data):
                    let dict = JSON(data)
                    let status = dict[PARAMS.STATUS].stringValue// 0,1,2
                    if status == PARAMS.TRUE {
                        //print("this is workout_videos =====",workoutdata as Any)
                        completion(true,status )
                        
                    } else {
                        completion(false, status)
                    }
                }
        }
    }
    
    class func registerToken(email : String, completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        let params = ["email" : email, PARAMS.DEVICETOKEN: Constants.deviceToken,PARAMS.DEVICETYPE: Constants.deviceType] as [String : Any]
        Alamofire.request(REGISTERTOKEN,method:.post, parameters:params )
        .responseJSON { response in
            switch response.result {
                case .failure:
                    completion(false, nil)
                case .success(let data):
                    let dict = JSON(data)
                    let status = dict[PARAMS.STATUS].stringValue// 0,1,2
                    if status == PARAMS.TRUE {
                        //print("this is workout_videos =====",workoutdata as Any)
                        completion(true,status )
                        
                    } else {
                        completion(false, status)
                    }
                }
        }
    }
    
    /*removeAllProgramVideo(int workoutvideoId){
    AndroidNetworking.post(Constants.SERVER_URL + "removeAllProgramVideo")
            .addBodyParameter("user_id", Commons.thisUser.getId())
            .addBodyParameter("workoutvideo_id", String.valueOf(workoutvideoId))
            .setTag(this)*/
    
    class func removeAllProgramVideo(user_id : Int,product_id: String, completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        let params = ["user_id" : user_id, "product_id": product_id] as [String : Any]
        Alamofire.request(REMOVEALLPROGRAMVIDEO,method:.post, parameters:params )
        .responseJSON { response in
            switch response.result {
                case .failure:
                    completion(false, nil)
                case .success(let data):
                    let dict = JSON(data)
                    let status = dict[PARAMS.STATUS].stringValue// 0,1,2
                    let programs = dict["programs"].arrayObject
                    if status == PARAMS.TRUE {
                        //print("this is workout_videos =====",workoutdata as Any)
                        completion(true,programs )
                        
                    } else {
                        completion(false, status)
                    }
                }
        }
    }
    
    /*AndroidNetworking.post(Constants.SERVER_URL + "verifyDevice")
    .addBodyParameter("email", editEmail.getText().toString())
    .setTag(this)
    .setPriority(Priority.HIGH)
    .build()
    .getAsJSONObject(new JSONObjectRequestListener() {
        @Override
        public void onResponse(JSONObject response) {
            progressBar.setVisibility(View.GONE);
            try {
                String result = response.getString("result_code");
                if (result.equals("0")) {
                    String pinCode = response.getString("pincode");
                    showVerifyDialog(pinCode);
                } else {
                    showToast("Network error");*/
    
    class func onVerify(email : String, completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        let params = ["email" : email] as [String : Any]
        Alamofire.request(ONVERYFYDEVICE,method:.post, parameters:params )
        .responseJSON { response in
            switch response.result {
                case .failure:
                    completion(false, nil)
                case .success(let data):
                    let dict = JSON(data)
                    let status = dict[PARAMS.STATUS].stringValue// 0,1,2
                    let pincode = dict[PARAMS.PINCODE].stringValue
                    if status == PARAMS.TRUE {
                        completion(true,pincode)
                        print("serverpincode=====",pincode)
                        
                    } else {
                        completion(false, status)
                    }
                }
        }
    }
    
    /* AndroidNetworking.post(Constants.SERVER_URL + "getVideos")
    .addBodyParameter("user_id", Commons.thisUser.getId())
    .addBodyParameter("video_type", String.valueOf(productType))*/
    class func getVideos(video_type: String , completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        
        let user_id = user?.id
        let params = ["user_id" : user_id ?? 0, "video_type": video_type] as [String : Any]
        Alamofire.request(GETVIDEOS,method:.post, parameters:params )
        .responseJSON { response in
            switch response.result {
                case .failure:
                    completion(false, nil)
                case .success(let data):
                    let dict = JSON(data)
                    let status = dict[PARAMS.STATUS].stringValue// 0,1,2
                    
                    let video_list = dict[PARAMS.VIDEO_LIST].arrayObject
                    let video_listdata = JSON(video_list as Any)
                    if status == PARAMS.TRUE {
                        //print("this is nutrition_videos =====",video_listdata as Any)
                        completion(true, video_listdata)
                        
                    } else {
                        completion(false, status)
                    }
                }
        }
    }
    
    class func getLocalVideo(type: String , completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        
        let user_id = user?.id
        let params = ["user_id" : user_id ?? 0, "type": type] as [String : Any]
        Alamofire.request(GETLOCALVIDEO,method:.post, parameters:params )
        .responseJSON { response in
            switch response.result {
                case .failure:
                    completion(false, nil)
                case .success(let data):
                    let dict = JSON(data)
                    let status = dict[PARAMS.STATUS].stringValue// 0,1,2
                    
                    let video_list = dict["local_list"].arrayObject
                    let video_listdata = JSON(video_list as Any)
                    if status == PARAMS.TRUE {
                        //print("this is workout_videos =====",workoutdata as Any)
                        completion(true, video_listdata)
                        
                    } else {
                        completion(false, status)
                    }
                }
        }
    }
    
    class func getCheckout(product_type: String , completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        
        let user_id = user?.id
        let params = ["user_id" : user_id ?? 0, "product_type": product_type] as [String : Any]
        Alamofire.request(GETCHECKOUT,method:.post, parameters:params )
        .responseJSON { response in
            switch response.result {
                case .failure:
                    completion(false, nil)
                case .success(let data):
                    let dict = JSON(data)
                    let status = dict[PARAMS.STATUS].stringValue// 0,1,2
                    
                    let checkout_List = dict["checkout_list"].arrayObject
                    let checkout_Listdata = JSON(checkout_List as Any)
                    if status == PARAMS.TRUE {
                        //print("this is workout_videos =====",workoutdata as Any)
                        completion(true, checkout_Listdata)
                        
                    } else {
                        completion(false, status)
                    }
                }
        }
    }
    
    /* void setPurchaseProduct(String ids, String endDay){
    AndroidNetworking.post(Constants.SERVER_URL + "setPurchase")
            .addBodyParameter("user_id", Commons.thisUser.getId())
            .addBodyParameter("purchase_id", ids)
            .addBodyParameter("endDay", endDay)
            .addBodyParameter("status", "yes")*/
    
    class func setPurchaseProduct(ids: String , endDay: String, completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        
        let user_id = user?.id
        let params = ["user_id" : user_id ?? 0, "purchase_id": ids, "endDay":endDay, "status":"yes"] as [String : Any]
        Alamofire.request(SETPURCHASE,method:.post, parameters:params )
        .responseJSON { response in
            switch response.result {
                case .failure:
                    completion(false, nil)
                case .success(let data):
                    let dict = JSON(data)
                    let status = dict[PARAMS.STATUS].stringValue// 0,1,2
                    
                    if status == PARAMS.TRUE {
                        //print("this is workout_videos =====",workoutdata as Any)
                        completion(true, status)
                    } else {
                        completion(false, status)
                    }
                }
        }
    }
    
    /* AndroidNetworking.post(Constants.SERVER_URL + "setVideoUri")
    .addBodyParameter("user_id", Commons.thisUser.getId())
    .addBodyParameter("video_id", String.valueOf(videoId))
    .addBodyParameter("uri", localUrl)
*/
    class func setVideoUri(video_id: String , uri: String, completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        
        let user_id = user?.id
        let params = ["user_id" : user_id ?? 0, "video_id": video_id, "uri":uri] as [String : Any]
        Alamofire.request(SETVIDEOURI,method:.post, parameters:params )
        .responseJSON { response in
            switch response.result {
                case .failure:
                    completion(false, nil)
                case .success(let data):
                    let dict = JSON(data)
                    let status = dict[PARAMS.STATUS].stringValue// 0,1,2
                    
                    if status == PARAMS.TRUE {
                        //print("this is workout_videos =====",workoutdata as Any)
                        completion(true, status)
                    } else {
                        completion(false, status)
                    }
                }
        }
    }
    
    /*AndroidNetworking.post(Constants.SERVER_URL + "removeSubscription")
    .addBodyParameter("user_id", Commons.thisUser.getId())
    .addBodyParameter("product_id", String.valueOf(workoutvideoId))*/
    class func removeSubscription(product_id: String ,completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        
        let user_id = user?.id
        let params = ["user_id" : user_id ?? 0, "product_id": product_id] as [String : Any]
        Alamofire.request(REMOVESUBSCRIPTION,method:.post, parameters:params )
        .responseJSON { response in
            switch response.result {
                case .failure:
                    completion(false, nil)
                case .success(let data):
                    let dict = JSON(data)
                    let status = dict[PARAMS.STATUS].stringValue// 0,1,2
                    
                    if status == PARAMS.TRUE {
                        //print("this is workout_videos =====",workoutdata as Any)
                        completion(true, status)
                    } else {
                        completion(false, status)
                    }
                }
        }
    }
    
}
