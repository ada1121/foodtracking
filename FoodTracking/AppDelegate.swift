//
//  AppDelegate.swift
//  FoodTracking
//
//  Created by Ubuntu on 12/24/19.
//  Copyright © 2019 Ubuntu. All rights reserved.
//

import UIKit
import CoreData
import IQKeyboardManagerSwift
import RealmSwift
import SwiftyUserDefaults
import SwiftyJSON

var user:UserModel?
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        IQKeyboardManager.shared.enable = true
        user = UserModel()
        
        NotificationCenter.default.addObserver(self, selector: #selector(gotoUserCheckAppDelegate), name: NSNotification.Name(rawValue: "gotoUserCheck"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(gotoWorkoutVideo), name: NSNotification.Name(rawValue: "gotoWorkoutVideo"), object: nil)
        
        return true
    }
    
    @objc func gotoWorkoutVideo(){
        print("workoutvideo")
    }
    
    @objc func gotoUserCheckAppDelegate() {
               
       let email = UserDefault.getString(key: PARAMS.EMAIL)
       if email != nil && email != ""{
           ApiManager.getMydetails(){
               (issuccess,data) in
               if issuccess{
                   let userinfoJson = JSON(data as Any)
                   user = UserModel(userinfoJson)
                   user?.saveUserInfo()
                   let timeNow = Int(NSDate().timeIntervalSince1970) * 1000
                   let lasttimestamp = UserDefault.getString(key: Constants.LASTTIMEDATE,defaultValue: "\(timeNow)")
                   print("lastdate ==========================================",getStrDate(lasttimestamp!))
                   print("nowdate ==========================================",getStrDate("\(timeNow)"))
                   
                   
                   let lasttimedate : String = UserDefault.getString(key: Constants.LASTTIMEDATE,defaultValue: "0")!
                   if self.dayDifference(from: lasttimedate.toDouble()! / 1000)
                   {
                       if DBHelper.shared.getTotalHistoy().count != 0{
                           self.uploadHistoryToServer()
                           let timeNow = Int(NSDate().timeIntervalSince1970) * 1000
                           UserDefault.setString(key: Constants.LASTTIMEDATE, value: "\(timeNow)")
                       }
                       else{
                           UserDefault.setInt(key: Constants.TOTAL_BREAKFAST_CALORIE,value: 0)
                           UserDefault.setInt(key: Constants.TOTAL_FIRST_SNACK_CALORIE,value: 0)
                           UserDefault.setInt(key: Constants.TOTAL_LUNCH_CALORIE,value: 0)
                           UserDefault.setInt(key: Constants.TOTAL_SECOND_SNACK_CALORIE,value: 0)
                           UserDefault.setInt(key: Constants.TOTAL_DINNER_CALORIE,value: 0)
                           UserDefault.setInt(key: Constants.TOTAL_THIRD_SNACK_CALORIE,value: 0)
                           UserDefault.setFloat(key: Constants.DAILY_SCORE, value: 0)
                           UserDefault.setString(key: Constants.DAILY_GRADE, value: "0")
                           totalfoodHistoyModel = TotalFoodHistoryModel()
                           DBHelper.shared.removeall()
                           
                           let logout = UserDefault.getBool(key: Constants.LOGOUT)
                           if logout{
                               let storyboard = UIStoryboard(name: "LoginVC", bundle: nil)
                               let loginVC = storyboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                               UIApplication.shared.keyWindow?.rootViewController = loginVC
                               networkcondition1 = 1
                           }
                           else{
                               if user!.isValid{
                                   let storyboard = UIStoryboard(name: "MainVC", bundle: nil)
                                   let mainVC = storyboard.instantiateViewController(withIdentifier: "MainVC") as! MainVC
                                   UIApplication.shared.keyWindow?.rootViewController = mainVC
                               }
                               else{
                                   let storyboard = UIStoryboard(name: "LoginVC", bundle: nil)
                                   let loginVC = storyboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                                   UIApplication.shared.keyWindow?.rootViewController = loginVC
                               }
                               networkcondition1 = 1
                           }
                           
                       }
                   }
                   else{
                       let logout = UserDefault.getBool(key: Constants.LOGOUT)
                       if logout{
                           let storyboard = UIStoryboard(name: "LoginVC", bundle: nil)
                           let loginVC = storyboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                           UIApplication.shared.keyWindow?.rootViewController = loginVC
                           networkcondition1 = 1
                       }
                       else{
                           if user!.isValid{
                               let storyboard = UIStoryboard(name: "MainVC", bundle: nil)
                               let mainVC = storyboard.instantiateViewController(withIdentifier: "MainVC") as! MainVC
                               UIApplication.shared.keyWindow?.rootViewController = mainVC
                           }
                           else{
                               let storyboard = UIStoryboard(name: "LoginVC", bundle: nil)
                               let loginVC = storyboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                               UIApplication.shared.keyWindow?.rootViewController = loginVC
                           }
                           networkcondition1 = 1
                       }
                   }
               }
               else{
//                   UserDefault.setString(key: Constants.OLD_VALUE,value: "0")
//                   UserDefault.setInt(key: Constants.TOTAL_BREAKFAST_CALORIE,value: 0)
//                   UserDefault.setInt(key: Constants.TOTAL_FIRST_SNACK_CALORIE,value: 0)
//                   UserDefault.setInt(key: Constants.TOTAL_LUNCH_CALORIE,value: 0)
//                   UserDefault.setInt(key: Constants.TOTAL_SECOND_SNACK_CALORIE,value: 0)
//                   UserDefault.setInt(key: Constants.TOTAL_DINNER_CALORIE,value: 0)
//                   UserDefault.setInt(key: Constants.TOTAL_THIRD_SNACK_CALORIE,value: 0)
//                   UserDefault.setFloat(key: Constants.DAILY_SCORE, value: 0)
//                   UserDefault.setString(key: Constants.DAILY_GRADE, value: "0")
//                   totalfoodHistoyModel = TotalFoodHistoryModel()
//                   DBHelper.shared.removeall()
//                   let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                   let loginVC = storyboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
//                   UIApplication.shared.keyWindow?.rootViewController = loginVC
//                  networkcondition1 = 1
                return
               }
           }
       }
       else{
           UserDefault.setString(key: Constants.OLD_VALUE,value: "0")
           UserDefault.setInt(key: Constants.TOTAL_BREAKFAST_CALORIE,value: 0)
           UserDefault.setInt(key: Constants.TOTAL_FIRST_SNACK_CALORIE,value: 0)
           UserDefault.setInt(key: Constants.TOTAL_LUNCH_CALORIE,value: 0)
           UserDefault.setInt(key: Constants.TOTAL_SECOND_SNACK_CALORIE,value: 0)
           UserDefault.setInt(key: Constants.TOTAL_DINNER_CALORIE,value: 0)
           UserDefault.setInt(key: Constants.TOTAL_THIRD_SNACK_CALORIE,value: 0)
           UserDefault.setFloat(key: Constants.DAILY_SCORE, value: 0)
           UserDefault.setString(key: Constants.DAILY_GRADE, value: "0")
           totalfoodHistoyModel = TotalFoodHistoryModel()
           DBHelper.shared.removeall()
           let storyboard = UIStoryboard(name: "LoginVC", bundle: nil)
           let loginVC = storyboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
           UIApplication.shared.keyWindow?.rootViewController = loginVC
           networkcondition1 = 1
       }
    }
    
    func dayDifference(from interval : TimeInterval) -> Bool
    {
        let calendar = Calendar.current
        let date = Date(timeIntervalSince1970: interval)
        if calendar.isDateInYesterday(date) {
            print("Yesterday")
            return true }
        else if calendar.isDateInToday(date) {
            print("Today")
            return false }
        else if calendar.isDateInTomorrow(date) {
            print("Tomorrow")
            return false }
        else {
            let startOfNow = calendar.startOfDay(for: Date())
            let startOfTimeStamp = calendar.startOfDay(for: date)
            let components = calendar.dateComponents([.day], from: startOfNow, to: startOfTimeStamp)
            let day = components.day!
            if day < 1 {
                print("\(-day) days ago")
                return true }
            else {
                print("In \(day) days")
                return false }
        }
    }
    
    func uploadHistoryToServer() {
            DBHelper.shared.getTotalUploadHistoryFromRealm()
            ApiManager.uploadHistory(foodHistory: totalfoodHistoyModel){
                (issuccess, data) in
                if issuccess{
                    
                    print("==============******* serverupload success **************========")
                    UserDefault.setInt(key: Constants.TOTAL_BREAKFAST_CALORIE,value: 0)
                    UserDefault.setInt(key: Constants.TOTAL_FIRST_SNACK_CALORIE,value: 0)
                    UserDefault.setInt(key: Constants.TOTAL_LUNCH_CALORIE,value: 0)
                    UserDefault.setInt(key: Constants.TOTAL_SECOND_SNACK_CALORIE,value: 0)
                    UserDefault.setInt(key: Constants.TOTAL_DINNER_CALORIE,value: 0)
                    UserDefault.setInt(key: Constants.TOTAL_THIRD_SNACK_CALORIE,value: 0)
                    UserDefault.setFloat(key: Constants.DAILY_SCORE, value: 0)
                    UserDefault.setString(key: Constants.DAILY_GRADE, value: "0")
                    totalfoodHistoyModel = TotalFoodHistoryModel()
                    DBHelper.shared.removeall()
                    
                        let logout = UserDefault.getBool(key: Constants.LOGOUT)
                        if logout{
                            let storyboard = UIStoryboard(name: "LoginVC", bundle: nil)
                            let loginVC = storyboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                            UIApplication.shared.keyWindow?.rootViewController = loginVC
                            networkcondition1 = 1
                        }
                        else{
                            if user!.isValid{
                                print("valid go to main no upload History")
                                let storyboard = UIStoryboard(name: "MainVC", bundle: nil)
                                let mainVC = storyboard.instantiateViewController(withIdentifier: "MainVC") as! MainVC
                                UIApplication.shared.keyWindow?.rootViewController = mainVC
                            }
                            else{
                                print("non valid show contact coach no upload History")
                                let storyboard = UIStoryboard(name: "LoginVC", bundle: nil)
                                let loginVC = storyboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                                UIApplication.shared.keyWindow?.rootViewController = loginVC
                            }
                            networkcondition1 = 1
                        }
                }
            }
    }
        
        func clearLocalDatabaseSplash(_ item : Bool){
               
               if item {
                   UserDefault.setString(key: Constants.OLD_VALUE,value: "0")
                   UserDefault.setInt(key: Constants.TOTAL_BREAKFAST_CALORIE,value: 0)
                   UserDefault.setInt(key: Constants.TOTAL_FIRST_SNACK_CALORIE,value: 0)
                   UserDefault.setInt(key: Constants.TOTAL_LUNCH_CALORIE,value: 0)
                   UserDefault.setInt(key: Constants.TOTAL_SECOND_SNACK_CALORIE,value: 0)
                   UserDefault.setInt(key: Constants.TOTAL_DINNER_CALORIE,value: 0)
                   UserDefault.setInt(key: Constants.TOTAL_THIRD_SNACK_CALORIE,value: 0)
                   UserDefault.setFloat(key: Constants.DAILY_SCORE, value: 0)
                   UserDefault.setString(key: Constants.DAILY_GRADE, value: "0")
                   totalfoodHistoyModel = TotalFoodHistoryModel()
                   DBHelper.shared.removeall()
               }
               else{
                   UserDefault.setInt(key: Constants.TOTAL_BREAKFAST_CALORIE,value: 0)
                   UserDefault.setInt(key: Constants.TOTAL_FIRST_SNACK_CALORIE,value: 0)
                   UserDefault.setInt(key: Constants.TOTAL_LUNCH_CALORIE,value: 0)
                   UserDefault.setInt(key: Constants.TOTAL_SECOND_SNACK_CALORIE,value: 0)
                   UserDefault.setInt(key: Constants.TOTAL_DINNER_CALORIE,value: 0)
                   UserDefault.setInt(key: Constants.TOTAL_THIRD_SNACK_CALORIE,value: 0)
                   UserDefault.setFloat(key: Constants.DAILY_SCORE, value: 0)
                   UserDefault.setString(key: Constants.DAILY_GRADE, value: "0")
                   totalfoodHistoyModel = TotalFoodHistoryModel()
                   DBHelper.shared.removeall()
               }
           }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        print("applicationWillTerminate ")
        let timeNow = Int(NSDate().timeIntervalSince1970) * 1000
        UserDefault.setString(key: Constants.LASTTIMEDATE, value: "\(timeNow)")
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let SplashVC = storyboard.instantiateViewController(withIdentifier: "SplashVC") as! SplashVC
        UIApplication.shared.keyWindow?.rootViewController = SplashVC
       print("applicationDidEnterBackground ")
    }
    func applicationDidBecomeActive(_ application: UIApplication) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let SplashVC = storyboard.instantiateViewController(withIdentifier: "SplashVC") as! SplashVC
        UIApplication.shared.keyWindow?.rootViewController = SplashVC
        print("applicationDidBecomeActive ")
    }
//    func applicationDidEnterBackground(_ application: UIApplication)  {
//
//    }
    
    var restrictRotation:UIInterfaceOrientationMask = .portrait
    
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask
    {
        return self.restrictRotation
    }
   

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentCloudKitContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentCloudKitContainer(name: "FoodTracking")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

}

extension Notification.Name {
    static let stop_recording = Notification.Name("screenrecording")
}

