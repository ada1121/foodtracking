//
//  HistoryUploadStruct.swift
//  FoodTracking
//
//  Created by Ubuntu on 1/14/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import Foundation

struct FoodHistory : Codable {
    var email: String
    var score: String
    var grade: String
    var date: String
    var total: String
    var remain: String
    var breakfast: String
    var lunch: String
    var dinner: String
    var firstsnack: String
    var secondsnack: String
    var thirdsnack: String
    var categories:[[Categories]]
    struct Categories: Codable {
        var meal_type:String
        var food_type:String
        var subcategories:[[Subcategories]]
        struct Subcategories: Codable {
            var title:String
            var cal:String
            var carbs:String
            var fat:String
            var protein:String
            var fiber:String
        }
    }
}
