//
//  TotalFoodHistoryModel.swift
//  FoodTracking
//
//  Created by Ubuntu on 2/1/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift
// ========================  TotalFoodHistoryModel ===========================//

class TotalFoodHistoryModel : NSObject {
   
    var id : String?
    var email : String?
    var score : String?
    var grade : String?
    var date : String?
    var total: String?
    var remain: String?
    var breakfast: String?
    var lunch: String?
    var dinner: String?
    var firstsnack: String?
    var secondsnack: String?
    var thirdsnack: String?
    var categories =  [TotalCategoriesModel]()
    
    override init() {
        super.init()
        id = ""
        email = ""
        score = ""
        grade = ""
        date = ""
        total = ""
        remain = ""
        breakfast = ""
        lunch = ""
        dinner = ""
        firstsnack = ""
        secondsnack = ""
        thirdsnack = ""
        categories = [TotalCategoriesModel()]
    }
    
    init(id: String,email :String,score:String,grade:String,date:String,total:String,remain:String,breakfast:String,lunch:String,dinner:String,firstsnack:String,secondsnack:String,thirdsnack:String,categories:[TotalCategoriesModel] ) {
        self.id = id
        self.email = email
        self.score = score
        self.grade = grade
        self.date = date
        self.total = total
        self.remain = remain
        self.breakfast = breakfast
        self.lunch = lunch
        self.dinner = dinner
        self.firstsnack = firstsnack
        self.secondsnack = secondsnack
        self.thirdsnack = thirdsnack
       
       let categoiesTemp = categories
        
       for one in categoiesTemp{
        var subcategoriestemp = [TotalSubcategoriesModel]()
           for two in one.subcategories {
            subcategoriestemp.append(TotalSubcategoriesModel(id: two.id!, title: two.title!, cal: two.cal!, carbs: two.carbs!, fat: two.fat!, protein: two.protein!, fiber: two.fiber!))
           }
        self.categories.append(TotalCategoriesModel(id: one.id!, meal_type: one.meal_type!, food_type: one.food_type!, subcategories: subcategoriestemp))
           
       }
   }
  
   func toDictionary() -> [String : Any] {

       var dictionary = [String:Any]()
       let otherSelf = Mirror(reflecting: self)
       for child in otherSelf.children {
           if let key = child.label {
               dictionary[key] = child.value
           }
       }
       print("FoodHistory_DICTIONARY :: \(dictionary.description)")
       return dictionary
   }
}

 
class TotalCategoriesModel: NSObject {
    
    var id : String?
    var meal_type:String?
    var food_type:String?
    var subcategories = [TotalSubcategoriesModel]()

    override init() {
    
    super.init()
        
        id = ""
        meal_type = ""
        food_type = ""
        subcategories = [TotalSubcategoriesModel()]
    }
    
    init(id:String,meal_type:String,food_type:String,subcategories:[TotalSubcategoriesModel]) {
        self.id = id
        self.meal_type = meal_type
        self.food_type = food_type
        
        let subcategoriestemp = subcategories
        for one in subcategoriestemp{
            self.subcategories.append(TotalSubcategoriesModel(id: one.id!, title: one.title!, cal: one.cal!, carbs: one.carbs!, fat: one.fat!, protein: one.protein!, fiber: one.fiber!))
         }
    }
    
    func toDictionary() -> [String : Any] {

        var dictionary = [String:Any]()
        let otherSelf = Mirror(reflecting: self)

        for child in otherSelf.children {

            if let key = child.label {
                dictionary[key] = child.value
            }
        }
        print("Categories_DICTIONARY :: \(dictionary.description)")
        return dictionary
    }
}

 class TotalSubcategoriesModel: NSObject {
   
 var id : String?
 var title:String?
 var cal:String?
 var carbs:String?
 var fat:String?
 var protein:String?
 var fiber:String?
   
   override init() {
   super.init()
      id = ""
      title = ""
      cal = ""
      carbs = ""
      fat = ""
      protein = ""
      fiber = ""
   }
    
    init(id:String,title:String,cal:String,carbs:String,fat:String,protein:String,fiber:String) {
        self.id = id
        self.title = title
        self.cal = cal
        self.carbs = carbs
        self.fat = fat
        self.protein = protein
        self.fiber = fiber
    }

   func toDictionary() -> [String : Any] {

       var dictionary = [String:Any]()
       let otherSelf = Mirror(reflecting: self)

       for child in otherSelf.children {

           if let key = child.label {
               dictionary[key] = child.value
           }
       }
       print("Subcategories_DICTIONARY :: \(dictionary.description)")
       return dictionary
   }
}
