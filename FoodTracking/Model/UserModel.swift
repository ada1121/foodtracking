//
//  UserModel.swift
//  EveraveUpdate
//
//  Created by Ubuntu on 1/18/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import Foundation
import SwiftyJSON

class UserModel: NSObject {
        var id: Int!
        var email: String?
        var username: String?
        var approve: String?
    
       override init() {
           super.init()
           id = 0
           username = ""
           email = ""
           approve = ""
       }
       
    init(_ json : JSON) {
        
        self.id = json[PARAMS.USERID].stringValue.toInt()
        self.email = json[PARAMS.EMAIL].stringValue
        self.username = json[PARAMS.USERNAME].stringValue
        self.approve = json[PARAMS.APPROVE].stringValue
    }
    // Check and returns if user is valid user or not
   var isValid: Bool {
       return id != nil && id != 0 && approve != "" && approve != "0"
   }
    
    // Recover user credential from UserDefault
    func loadUserInfo() {
        id = UserDefault.getInt(key: PARAMS.ID, defaultValue: 0)
        email = UserDefault.getString(key: PARAMS.EMAIL, defaultValue: "")
        username = UserDefault.getString(key: PARAMS.USERNAME, defaultValue: "")
        approve = UserDefault.getString(key: PARAMS.APPROVE, defaultValue: "")
    }
    // Save user credential to UserDefault
    func saveUserInfo() {
        UserDefault.setInt(key: PARAMS.ID, value: id)
        UserDefault.setString(key: PARAMS.EMAIL, value: email)
        UserDefault.setString(key: PARAMS.USERNAME, value: username)
        UserDefault.setString(key: PARAMS.APPROVE, value: approve)
        
    }
    // Clear save user credential
    func clearUserInfo() {
        
        id = 0
        username = ""
        email = ""
        approve = ""
        
       UserDefault.setInt(key: PARAMS.ID, value: 0)
       UserDefault.setString(key: PARAMS.EMAIL, value: nil)
       UserDefault.setString(key: PARAMS.USERNAME, value: nil)
       UserDefault.setString(key: PARAMS.APPROVE, value: nil)
    }
    
    func updateUserInfo(user: UserModel) {
        id = user.id
        username = user.username
        email = user.email
        approve = user.approve
    }
}

