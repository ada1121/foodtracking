//
//  Cal_listModel.swift
//  FoodTracking
//
//  Created by Ubuntu on 1/6/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import Foundation
import SwiftyJSON

class Cal_listModel {
    var id : String = ""
    var user_id : String = ""
    var title : String = ""
    var cal : String = ""
    var carbs : String = ""
    var fat  : String = ""
    var protein : String = ""
    var fiber : String = ""
    var other : String = ""
    var selected_State = 0
    var longpress = 0
    
    init(_ catJSON : JSON) {
        self.id = catJSON[PARAMS.ID].stringValue
        self.user_id = catJSON[PARAMS.USERID].stringValue
        self.title = catJSON [PARAMS.TITLE].stringValue
        self.cal = catJSON [PARAMS.CAL].stringValue
        self.carbs = catJSON [PARAMS.CABS].stringValue
        self.fat = catJSON [PARAMS.FAT].stringValue
        self.protein = catJSON [PARAMS.PROTEIN].stringValue
        self.fiber = catJSON[PARAMS.FIBER].stringValue
        self.selected_State = 0
        self.other = ""
        self.longpress = 0
    }
    
    init(id : String , title : String,cal: String,carbs : String,fat : String,protein : String,fiber: String,other: String,selected_Stated : Int,longPress : Int) {
        
        self.id = id
        self.user_id = "\(user!.id!)"
        self.title = title
        self.cal = cal
        self.carbs = carbs
        self.fat = fat
        self.protein = protein
        self.fiber = fiber
        self.selected_State = 0
        self.other = ""
        self.longpress = 0
    }
}
