//
//  VncModel.swift
//  FoodTracking
//
//  Created by Mac on 4/4/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import Foundation
import SwiftyJSON

class WcnModel {
    
    var id : String = ""
    var title : String = ""
    var subtitle : String = ""
    var minidescription : String = ""
    var fulldescription : String = ""
    var price : String = ""
    var period : String = ""
    var image : String = ""
    var video : String = ""
    
    var status : String = ""
    var endDay : String = ""
    var uri : String = ""
    var phone : String = ""
    var name : String = ""
    var times : String = ""
    var email : String = ""
    var default_times:String = ""
    var type: String = ""
    var quantity: String = "0"
    
    /* vncModel.setId(atObj.getInt("id"));
    vncModel.setTitle(atObj.getString("title"));
    vncModel.setSubtitle(atObj.getString("subtitle"));
    vncModel.setMinides(atObj.getString("minidescription"));
    vncModel.setFulldes(atObj.getString("fulldescription"));
    vncModel.setPrice(Float.parseFloat(atObj.getString("price")));
    vncModel.setPeriod(atObj.getString("period"));
    vncModel.setImage(atObj.getString("image"));
    vncModel.setVideo(atObj.getString("video"));
    vncModel.setEmail(atObj.getString("email"));
    vncModel.setName(atObj.getString("name"));
    vncModel.setPhone(atObj.getString("phone"));
    vncModel.setType(atObj.getString("type"));
    String str = atObj.getString("endDay");
    if(str.equals("null")){
        str = "0";
    }
    vncModel.setEndDay(Long.parseLong(str));
    str = atObj.getString("uri");
    if(str.equals("null")){
        str = "";
    }
    vncModel.setUri(str);
    str = atObj.getString("status");
    if(str.equals("null")){
        str = "";
    }*/
    
    
    init(_ json: JSON) {
        self.id = json[PARAMS.ID].stringValue
        self.subtitle = json[PARAMS.SUBTITLE].stringValue
        self.title = json[PARAMS.TITLE].stringValue
        self.minidescription = json[PARAMS.MINIDESCRIPTION].stringValue
        self.fulldescription = json[PARAMS.FULLDESCRIPTION].stringValue
        self.price = json[PARAMS.PRICE].stringValue
        self.period = json[PARAMS.PERIOD].stringValue
        self.image = json[PARAMS.IMAGE].stringValue
        self.video = json[PARAMS.VIDEO].stringValue
        self.status = json[PARAMS.VIDEOSTATUS].stringValue
        self.endDay = json[PARAMS.ENDDAY].stringValue
        self.uri = json[PARAMS.URI].stringValue
        self.phone = json[PARAMS.PHONE].stringValue
        self.name = json[PARAMS.NAME].stringValue
        self.times = json[PARAMS.TIMES].stringValue
        self.email = json[PARAMS.EMAIL].stringValue
        self.default_times = json["default_times"].stringValue
        self.type = json["type"].stringValue
        self.quantity = json[PARAMS.QUANTITY].stringValue
    }
    
    init() {
        self.id = ""
        self.title = ""
        self.subtitle = ""
        self.minidescription = ""
        self.fulldescription = ""
        self.price = ""
        self.period = ""
        self.image = ""
        self.video = ""
        self.status = ""
        self.endDay = ""
        self.uri = ""
        self.phone = ""
        self.name = ""
        self.times = ""
        self.email = ""
        self.default_times = ""
        self.type = ""
        self.quantity = "0"
    }
}

/*class CoatchingSM {
    
    var id : String = ""
    var title : String = ""
    var subtitle : String = ""
    var minidescription : String = ""
    var fulldescription : String = ""
    var price : String = ""
    var period : String = ""
    var image : String = ""
    var phone : String = ""
    var name : String = ""
    var times : String = ""
    
    init(_ json: JSON) {
        self.id = ""
        self.title = json[PARAMS.TITLE].stringValue
        self.subtitle = json[PARAMS.SUBTITLE].stringValue
        self.minidescription = json[PARAMS.MINIDESCRIPTION].stringValue
        self.fulldescription = json[PARAMS.FULLDESCRIPTION].stringValue
        self.price = json[PARAMS.PRICE].stringValue
        self.period = json[PARAMS.PERIOD].stringValue
        self.image = json[PARAMS.IMAGE].stringValue
        self.phone = json[PARAMS.PHONE].stringValue
        self.name = json[PARAMS.NAME].stringValue
        self.times = json[PARAMS.TIMES].stringValue
    }
    
    init() {
        self.id = ""
        self.title = ""
        self.subtitle = ""
        self.minidescription = ""
        self.fulldescription = ""
        self.price = ""
        self.period = ""
        self.image = ""
        self.phone = ""
        self.name = ""
        self.times = ""
    }
}

class NutrionLM {
    
    var id : String = ""
    var title : String = ""
    var subtitle : String = ""
    var minidescription : String = ""
    var fulldescription : String = ""
    var price : String = ""
    var period : String = ""
    var image : String = ""
    var email : String = ""
    var name : String = ""
    var status : String = ""
    
    init(_ json: JSON) {
        self.id = ""
        self.title = json[PARAMS.TITLE].stringValue
        self.subtitle = json[PARAMS.SUBTITLE].stringValue
        self.minidescription = json[PARAMS.MINIDESCRIPTION].stringValue
        self.fulldescription = json[PARAMS.FULLDESCRIPTION].stringValue
        self.price = json[PARAMS.PRICE].stringValue
        self.period = json[PARAMS.PERIOD].stringValue
        self.image = json[PARAMS.IMAGE].stringValue
        self.email = json[PARAMS.EMAIL].stringValue
        self.name = json[PARAMS.NAME].stringValue
        self.status = json[PARAMS.STATUS].stringValue
    }
    
    init() {
        self.id = ""
        self.title = ""
        self.subtitle = ""
        self.minidescription = ""
        self.fulldescription = ""
        self.price = ""
        self.period = ""
        self.image = ""
        self.email = ""
        self.name = ""
        self.status = ""
    }
}*/


