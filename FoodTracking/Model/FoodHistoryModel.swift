

import Foundation
import UIKit
import RealmSwift

class FoodHistoryModel: Object {
    
    @objc dynamic var id = ""
    let categories = List<CategoriesModel>()

        convenience init( id:String,categories: List<CategoriesModel> ){
        
        self.init()
        self.id = id
        
        let categoiesTemp = categories
        for one in categoiesTemp{
            let subcategoriestemp = List<SubcategoriesModel>()
            for two in one.subcategories{
                subcategoriestemp.append(SubcategoriesModel(id: two.id, title: two.title, cal: two.cal, carbs: two.carbs, fat: two.fat, protein: two.protein, fiber: two.fiber))
            }
            self.categories.append(CategoriesModel(id: one.id, meal_type: one.meal_type, food_type: one.food_type, subcategories: subcategoriestemp))
            
        }
    }
    override static func primaryKey() -> String? {
        return "id"
    }
    
    func toDictionary() -> [String : Any] {

        var dictionary = [String:Any]()
        let otherSelf = Mirror(reflecting: self)
        for child in otherSelf.children {
            if let key = child.label {
                dictionary[key] = child.value
            }
        }
        print("FoodHistory_DICTIONARY :: \(dictionary.description)")
        return dictionary
    }
}

@objcMembers class CategoriesModel: Object {
dynamic var id : String = ""
dynamic var meal_type:String = ""
dynamic var food_type:String = ""
 let subcategories = List<SubcategoriesModel>()
 
    convenience init( id:String,meal_type:String, food_type:String, subcategories:List<SubcategoriesModel>) {
     
     self.init()
     self.id = id
     self.meal_type = meal_type
     self.food_type = food_type
     let subcategoriestemp = subcategories
     for one in subcategoriestemp{
        self.subcategories.append(SubcategoriesModel(id: one.id, title: one.title, cal: one.cal, carbs: one.carbs, fat: one.fat, protein: one.protein, fiber: one.fiber))
     }
 }

 func toDictionary() -> [String : Any] {

     var dictionary = [String:Any]()
     let otherSelf = Mirror(reflecting: self)

     for child in otherSelf.children {

         if let key = child.label {
             dictionary[key] = child.value
         }
     }
     //print("Categories_DICTIONARY :: \(dictionary.description)")
     return dictionary
 }
}

@objcMembers class SubcategoriesModel: Object {
    
dynamic var id : String = ""
dynamic var title:String = ""
dynamic var cal:String = ""
dynamic var carbs:String = ""
dynamic var fat:String = ""
dynamic var protein:String = ""
dynamic var fiber:String = ""
    
    convenience init(id:String,title:String,cal:String,carbs:String,fat:String,protein:String,fiber:String) {
    
    self.init()
    self.id = id
    self.title = title
    self.cal = cal
    self.carbs = carbs
    self.fat = fat
    self.protein = protein
    self.fiber = fiber
   }
    
    func toDictionary() -> [String : Any] {

        var dictionary = [String:Any]()
        let otherSelf = Mirror(reflecting: self)

        for child in otherSelf.children {

            if let key = child.label {
                dictionary[key] = child.value
            }
        }

        //print("Subcategories_DICTIONARY :: \(dictionary.description)")

        return dictionary
    }
}

    class tempFoodModel {
        
        var id : String = ""
        var meal_type : String = ""
        var food_type :String = ""
        var miniFood = [miniFoodModel]()
       
        init(id :String, meal_type:String,food_type: String , miniFood: [miniFoodModel]) {
            self.id = id
            self.meal_type = meal_type
            self.food_type = food_type
            let min = miniFood
            for one in min{
                self.miniFood.append(miniFoodModel(id:one.id,foodName: one.foodName,value: one.value,carbs:one.carbs,fat:one.fat,protein:one.protein,fiber:one.fiber))
            }
        }
    }

   


