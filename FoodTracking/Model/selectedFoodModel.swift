//
//  selectedFoodModel.swift
//  FoodTracking
//
//  Created by Ubuntu on 1/11/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import Foundation
import SwiftyJSON

class selectedFoodModel {
    
    var id : String = ""
    var header_title : String = ""
    var miniFood = [miniFoodModel]()
   
    init(id :String, header_title: String , miniFood: [miniFoodModel]) {
        self.id = id
        self.header_title = header_title
        self.miniFood = miniFood
    }
    init(_ selJson : JSON) {
        self.id = selJson[PARAMS.ID].stringValue
        self.header_title = selJson[PARAMS.FOODTYPE].stringValue
        let minifood_List = selJson[PARAMS.CATGORIES].arrayObject
        for one in minifood_List!{
            let jsonone = JSON(one as Any)
            let miniModel = miniFoodModel(jsonone)
            miniFood.append(miniModel)
        }
    }
}

class miniFoodModel {
    var id :String = ""
    var foodName : String = ""
    var value : String = ""
    var carbs:String = ""
    var fat :String = ""
    var protein:String = ""
    var fiber:String = ""
    init( id:String,foodName: String,value: String,carbs:String,fat:String,protein:String,fiber:String) {
        self.id = id
        self.foodName = foodName
        self.value = value
        self.carbs = carbs
        self.fat = fat
        self.protein = protein
        self.fiber = fiber
    }
    init(_ json: JSON ) {
        self.id = json[PARAMS.ID].stringValue
        self.foodName = json[PARAMS.TITLE].stringValue
        self.value = json[PARAMS.CAL].stringValue
        self.carbs = json[PARAMS.CABS].stringValue
        self.fat = json[PARAMS.FAT].stringValue
        self.protein = json[PARAMS.PROTEIN].stringValue
        self.fiber = json[PARAMS.FIBER].stringValue
    }
}
