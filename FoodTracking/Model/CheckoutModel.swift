//
//  VncModel.swift
//  FoodTracking
//
//  Created by Mac on 4/4/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import Foundation
import SwiftyJSON

class CheckoutModel {
    
    var id : String = ""
    var title : String = ""
    var quantity : String = ""
    var image : String = ""
    
    init(id: String, title: String, quantity: String,image: String) {
        self.id = id
        self.title = title
        self.image = image
        self.quantity = quantity
    }
    
    init() {
        self.id = ""
        self.title = ""
        self.image = ""
        self.quantity = ""
    }
}



