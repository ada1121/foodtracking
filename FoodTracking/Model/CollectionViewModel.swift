//
//  CollectionViewModel.swift
//  FoodTracking
//
//  Created by Ubuntu on 1/6/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import Foundation
import SwiftyJSON

class CollectionViewModel{
    var id : String = ""
    var title : String = ""
    var recomended : String = ""
    var calorie_list = [Cal_listModel]()
    
    init(_ catJSON : JSON) {
        self.id = catJSON[PARAMS.ID].stringValue
        self.title = catJSON[PARAMS.TITLE].stringValue
        self.recomended = catJSON[PARAMS.RECOMMENDED].stringValue
        let calorie_List = catJSON[PARAMS.CALORIELIST].arrayObject
        
        for one in calorie_List! {
            let jsonone = JSON(one as Any)
            let miniModel = Cal_listModel(jsonone)
            calorie_list.append(miniModel)
        }
    }
}
