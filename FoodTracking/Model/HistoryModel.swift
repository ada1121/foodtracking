//
//  HistoryModel.swift
//  FoodTracking
//
//  Created by Ubuntu on 1/15/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//
import Foundation
import SwiftyJSON

class HistoryModel{
    var id : String = ""
    var date : String = ""
    var total: String = ""
    var remain: String = ""
    var score: String = ""
    var grade: String = ""
    var breakfast: String = ""
    var lunch: String = ""
    var dinner: String = ""
    var firstsnack: String = ""
    var secondsnack: String = ""
    var thirdsnack: String = ""
    
    init(_ json : JSON){
        
        self.id = json[PARAMS.ID].stringValue
        self.score = json[PARAMS.SCORE].stringValue
        self.grade = json[PARAMS.GRADE].stringValue
        self.date = json[PARAMS.DATE].stringValue
        self.total = json[PARAMS.TOTAL].stringValue
        self.remain = json[PARAMS.REMAIN].stringValue
        self.breakfast = json[PARAMS.BREAKFAST].stringValue
        self.lunch = json[PARAMS.LUNCH].stringValue
        self.dinner = json[PARAMS.DINNER].stringValue
        self.firstsnack = json[PARAMS.FIRSTSNACK].stringValue
        self.secondsnack = json[PARAMS.SECONDSNACK].stringValue
        self.thirdsnack = json[PARAMS.THIRDSNACK].stringValue
    }
    
}
