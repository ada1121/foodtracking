//
//  MessageAttachDilaog.swift
//  FoodTracking
//
//  Created by Mac on 6/15/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import UIKit
import MessageUI

public let VC_MessageAttachDilaog = "MessageAttachDilaog"

class MessageAttachDilaog: BaseVC1 , MFMessageComposeViewControllerDelegate{

    @IBOutlet weak var txf_county: UITextField!
    @IBOutlet weak var txf_firstName: UITextField!
    @IBOutlet weak var txf_lastName: UITextField!
    @IBOutlet weak var txf_content: UITextField!
    var messageBody: String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        initTxf()
    }
    
    func initTxf()  {
        self.txf_county.addPadding(.left(10))
        self.txf_firstName.addPadding(.left(10))
        self.txf_lastName.addPadding(.left(10))
        self.txf_content.addPadding(.left(10))
    }
    @IBAction func closeBtnClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func okBtnClicked(_ sender: Any) {
        if self.txf_county.text!.isEmpty || self.txf_firstName.text!.isEmpty || self.txf_lastName.text!.isEmpty || self.txf_content.text!.isEmpty
        {
            self.progShowInfo(true, msg: "Input Error!")
        }
        else{
            messageBody = self.txf_county.text! + "\n" + self.txf_firstName.text! + "\n" + self.txf_lastName.text! + "\n" + self.txf_content.text!
            if let messagePhoneNumber = messagePhoneNumber{
                self.sendNewIMessage(messagePhoneNumber)
            }
        }
    }
    
    func sendNewIMessage(_ phonenumber : String) {
           let messageVC = MFMessageComposeViewController()
           messageVC.body = messageBody
           messageVC.recipients = [phonenumber]
           messageVC.messageComposeDelegate = self
           self.present(messageVC, animated: true, completion: nil)
    }
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        switch (result) {
        case .cancelled:
            print("Message was cancelled")
            dismiss(animated: true, completion: nil)
        case .failed:
            print("Message failed")
            dismiss(animated: true, completion: nil)
        case .sent:
            print("Message was sent")
            dismiss(animated: true, completion: nil)
        default:
            break
        }
    }
}
