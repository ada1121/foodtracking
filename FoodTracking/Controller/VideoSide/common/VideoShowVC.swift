//
//  VideoShowVC.swift
//  FoodTracking
//
//  Created by Mac on 4/13/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import Foundation
import UIKit
import AVKit
import AVFoundation
import Photos

class VideoShowVC: UIViewController {

    @IBOutlet weak var blockView: UIView!
    @IBOutlet weak var videoPreviewLayer: UIView!
    private var player: AVPlayer!
    private var playerViewController = AVPlayerViewController()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        (UIApplication.shared.delegate as! AppDelegate).restrictRotation = .all
        playVideoInView()
        NotificationCenter.default.addObserver(self, selector: #selector(preventScreenRecording2), name: NSNotification.Name.stop_recording, object: nil)
    }

    func playVideoInView() {
        
        if let model = downloadModel{
            guard let videoURL = URL(string: model.video) else {
                return
            }
            let player = AVPlayer(url: videoURL)
            playerViewController = AVPlayerViewController()
            playerViewController.player = player
            playerViewController.view.frame = videoPreviewLayer.frame
            self.addChild(playerViewController)
            self.view.addSubview(playerViewController.view)
        }
    }
    
    @objc func preventScreenRecording2(_ notification : Notification) {
        
        print("screen recording")
        DispatchQueue.main.asyncAfter(deadline: .now() + 15.0, execute: {
            let isCaptured = UIScreen.main.isCaptured
            if (isCaptured) {
                print("yes")
                self.dismiss(animated: true, completion: nil)
            }
            else {
                print("no")
                self.blockView.isHidden = true
            }
        })
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if let fragmenttype = fragmentType{
            if fragmenttype == FragmentType.workout.rawValue{
                let storyboard = UIStoryboard(name: VC_WorkoutVideoVC , bundle: nil)
                let MyVideoLibVC = storyboard.instantiateViewController(withIdentifier: VC_WorkoutVideoVC ) as! WorkoutVideoVC
                UIApplication.shared.keyWindow?.rootViewController = MyVideoLibVC
            }else if fragmenttype == FragmentType.nutrition.rawValue{
                let storyboard = UIStoryboard(name: VC_NutritionistVC, bundle: nil)
                let MyVideoLibVC = storyboard.instantiateViewController(withIdentifier: VC_NutritionistVC) as! NutrionistVC
                UIApplication.shared.keyWindow?.rootViewController = MyVideoLibVC
            }else{
                let storyboard = UIStoryboard(name: VC_contact, bundle: nil)
                let MyVideoLibVC = storyboard.instantiateViewController(withIdentifier: VC_contact) as! ContactVC
                UIApplication.shared.keyWindow?.rootViewController = MyVideoLibVC
            }
        }
        NotificationCenter.default.post(name: Notification.Name(rawValue: "gotoWorkoutVideo"), object: nil)
    }
}

extension Notification.Name {
    static let wechat_success = Notification.Name("wechat_success")
}
