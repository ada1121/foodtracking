//
//  VideoDownloadGuideVC.swift
//  FoodTracking
//
//  Created by Mac on 4/13/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import Foundation
import UIKit
import AVKit
import AVFoundation
import Photos

class VideoDownloadGuideVC: BaseVC1 {
    
    @IBOutlet weak var lbl_content: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lbl_content.text = "In order to download videos to your device you will need to provide permission. We do not access ANY your personsal files (images, videos etc.). \n Please click the OK button, then click the ALLOW link."
    }
   
    @IBAction func okBtnClicked(_ sender: Any) {
        if let model = downloadModel{
            let documentsUrl:URL =  (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first as URL?)!
            let name = randomString(length: 3) + ".mp4"
            let destinationFileUrl = documentsUrl.appendingPathComponent(name)
            dump(model,name: "download model========>")
            //Create URL to the source file you want to download
            let fileURL = URL(string: model.video)
            let sessionConfig = URLSessionConfiguration.default
            let session = URLSession(configuration: sessionConfig)
            let request = URLRequest(url:fileURL!)
            let task = session.downloadTask(with: request) { (tempLocalUrl, response, error) in
                if let tempLocalUrl = tempLocalUrl, error == nil {
                    // Success
                    if let statusCode = (response as? HTTPURLResponse)?.statusCode {
                        print("Successfully downloaded. Status code: \(statusCode)")
                        ApiManager.setVideoUri(video_id: model.id, uri: name) { (isSuccess, data) in
                            if isSuccess{
                            }else{
                            }
                        }
                    }

                    do {
                        try FileManager.default.copyItem(at: tempLocalUrl, to: destinationFileUrl)
                    } catch (let writeError) {
                        print("Error creating a file \(destinationFileUrl) : \(writeError)")
                    }
                } else {
                    print("Error took place while downloading a file. Error description: %@", error?.localizedDescription as Any);
                }
            }
            task.resume()
        }
        self.gotoVC("VideoShowVC")
    }
}

