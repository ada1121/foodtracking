//
//  ProductGuidDialog.swift
//  FoodTracking
//
//  Created by Mac on 6/15/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import UIKit

public let VC_ProductGuideDialog = "ProductGuidDialog"
class ProductGuidDialog: BaseVC1 {
    @IBOutlet weak var lbl_dialogTtl: UILabel!
    var completionHandle : (() -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let nutritionmodel = nutritionModel{
            self.lbl_dialogTtl.text = nutritionmodel.title
        }
        self.completionHandle = {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                self.progShowInfo(true, msg: "You have carted already")
            })
        }
    }
    
    @IBAction func okBtnClicked(_ sender: Any) {
        if let nutritionistmodel = nutritionModel{
            ApiManager.setCheckout(user_id: (user?.id)!, product_id: nutritionistmodel.id.toInt() ?? 0, quantity: 1){
            (isSuccess,data) in
                if isSuccess{
                   /*let storyboard = UIStoryboard(name: "Main", bundle: nil)
                   let controller = storyboard.instantiateViewController(withIdentifier: "CartDialogVC")
                   controller.modalPresentationStyle = .fullScreen
                   self.present(controller, animated: false, completion: nil)*/
                    self.gotoVC("CartDialogVC")
                }
                else{
                    self.dismiss(animated: true, completion: self.completionHandle)
                }
            }
        }
    }
    @IBAction func cancelBtnClicked(_ sender: Any) {
        //self.gotoStoryBoardVC(VC_NutritionistVC, fullscreen: true)
        self.dismiss(animated: true,completion: nil)
    }
}
