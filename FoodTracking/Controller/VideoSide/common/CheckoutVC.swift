//
//  CheckoutVC.swift
//  FoodTracking
//
//  Created by Mac on 4/9/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import UIKit
import GDCheckbox

class CheckoutVC: BaseVC1 {
    
    @IBOutlet weak var txf_county: UITextField!
    @IBOutlet weak var txf_firstName: UITextField!
    @IBOutlet weak var txf_lastName: UITextField!
    @IBOutlet weak var txf_address: UITextField!
    @IBOutlet weak var txf_town: UITextField!
    @IBOutlet weak var txf_state: UITextField!
    @IBOutlet weak var txf_zipcode: UITextField!
    @IBOutlet weak var txf_email: UITextField!
    @IBOutlet weak var txf_phone: UITextField!
    
    @IBOutlet weak var checkout: GDCheckbox!
    @IBOutlet weak var lbl_ship: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let fragmentype = fragmentType{
            if fragmentype != FragmentType.workout.rawValue{
                self.checkout.isHidden = false
                self.lbl_ship.isHidden = false
            }
            else{
                self.checkout.isHidden = true
                self.lbl_ship.isHidden = true
            }
        }
        self.initTxf()
    }
    
    func initTxf()  {
        self.txf_county.addPadding(.left(10))
        self.txf_firstName.addPadding(.left(10))
        self.txf_lastName.addPadding(.left(10))
        self.txf_address.addPadding(.left(10))
        self.txf_town.addPadding(.left(10))
        self.txf_state.addPadding(.left(10))
        self.txf_zipcode.addPadding(.left(10))
        self.txf_email.addPadding(.left(10))
        self.txf_phone.addPadding(.left(10))
    }
    @IBAction func okBtnClicked(_ sender: Any) {
       
        if self.txf_county.text!.isEmpty || self.txf_firstName.text!.isEmpty || self.txf_lastName.text!.isEmpty || self.txf_address.text!.isEmpty || self.txf_town.text!.isEmpty || self.txf_state.text!.isEmpty || self.txf_zipcode.text!.isEmpty || self.txf_email.text!.isEmpty || self.txf_phone.text!.isEmpty
        {
            self.progShowInfo(true, msg: "Input Error!")
        }
        else{
            checkoutcountry = self.txf_county.text
            checkoutfirstName = self.txf_firstName.text
            checkoutlastName = self.txf_lastName.text
            checkoutaddress = self.txf_address.text
            checkouttown = self.txf_town.text
            checkoutstate = self.txf_state.text
            checkoutzipcode = self.txf_zipcode.text
            checkoutemail = self.txf_email.text
            checkoutphone = self.txf_phone.text
            if self.checkout.isOn{
                self.gotoVC("ShippingVC")
            }
            else{
                self.gotoVC("CardCheckVC")
            }
        }
    }
    @IBAction func viewCartBtnClicked(_ sender: Any) {
        
    }
    @IBAction func closeBtnClicked(_ sender: Any) {
        if let fragmentype = fragmentType{
            if fragmentype == FragmentType.workout.rawValue{
                self.gotoVC("ViewCartVC")
            }
            else if fragmentype == FragmentType.nutrition.rawValue{
                self.gotoVC("NutrioinCheckoutVC")
            }
            else {
                self.gotoVC("ContactCheckVC")
            }
        }
    }
}
