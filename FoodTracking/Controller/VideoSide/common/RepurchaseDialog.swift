//
//  RepurchaseDialog.swift
//  FoodTracking
//
//  Created by Mac on 6/15/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import UIKit
public let VC_RepurchaseDialog = "RepurchaseDialog"
class RepurchaseDialog: BaseVC1 {
    
    @IBOutlet weak var lbl_dialgoTtl: UILabel!
    @IBOutlet weak var lbl_dialogContent: UILabel!
    var completionHandle : (() -> Void)?

    override func viewDidLoad() {
        super.viewDidLoad()
        if let nutritionistmodel = nutritionModel{
            lbl_dialgoTtl.text = "\(nutritionistmodel.title)"
            lbl_dialogContent.text = "You have pruchased this product already. Please check in your email for your purchase receipt. This product will be shipped within 7 business days. If you want to purchase again, please click BUY PRODUCT \n \n To contact us, send a text \n to 774-240-5287 or email \n info@universiyforweightlossscience.com."
        }
        self.completionHandle = {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                self.progShowInfo(true, msg: "You have carted already")
            })
        }
    }
    @IBAction func okBtnClicked(_ sender: Any) {
        //TODO:
        if let nutritionistmodel = nutritionModel{
            ApiManager.setCheckout(user_id: (user?.id)!, product_id: nutritionistmodel.id.toInt() ?? 0, quantity: 1){
            (isSuccess,data) in
                if isSuccess{
                   /*let storyboard = UIStoryboard(name: "Main", bundle: nil)
                   let controller = storyboard.instantiateViewController(withIdentifier: "CartDialogVC")
                   controller.modalPresentationStyle = .fullScreen
                   self.present(controller, animated: false, completion: nil)*/
                    self.gotoVC("CartDialogVC")
                }
                else{
                    self.dismiss(animated: true, completion: self.completionHandle)
                }
            }
        }
    }
    @IBAction func cancelBtnClicked(_ sender: Any) {
        self.dismiss(animated: true
            , completion: nil)
    }
}
