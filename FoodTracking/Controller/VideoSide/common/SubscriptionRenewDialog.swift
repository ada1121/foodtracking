//
//  SubscriptionRenewDialog.swift
//  FoodTracking
//
//  Created by Mac on 6/15/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import UIKit
public let VC_SubscriptionRenewDialog = "SubscriptionRenewDialog"
class SubscriptionRenewDialog: BaseVC1 {
    
    @IBOutlet weak var lbl_dialgoTtl: UILabel!
    @IBOutlet weak var lbl_dialogContent: UILabel!
    var completionHandle : (() -> Void)?
    override func viewDidLoad() {
        super.viewDidLoad()
        if let nutritionistmodel = nutritionModel{
            lbl_dialgoTtl.text = "\(nutritionistmodel.title) \n \n Subscription: \(nutritionistmodel.period)days \n Price: $\(nutritionistmodel.price)"
            lbl_dialogContent.text = "The subscription has an automatic renewal. The automatic renewal can be cancelled at anytime. The cost is deducted in advance in charging intervals of \(nutritionistmodel.period) days."
        }
        self.completionHandle = {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                self.progShowInfo(true, msg: "You have carted already")
            })
        }
    }
    @IBAction func okBtnClicked(_ sender: Any) {
        if let nutritionistmodel = nutritionModel{
            ApiManager.setCheckout(user_id: (user?.id)!, product_id: nutritionistmodel.id.toInt() ?? 0, quantity: 1){
            (isSuccess,data) in
                if isSuccess{
                    self.gotoVC("CartDialogVC")
                }
                else{
                    self.dismiss(animated: true, completion: self.completionHandle)
                }
            }
        }
    }
    
    @IBAction func cancelBtnClicked(_ sender: Any) {
        self.dismiss(animated: true
            , completion: nil)
    }
}
