//
//  ShippingVC.swift
//  FoodTracking
//
//  Created by Mac on 4/16/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import UIKit

class ShippingVC: BaseVC1 {
    
    @IBOutlet weak var txf_county: UITextField!
    @IBOutlet weak var txf_firstName: UITextField!
    @IBOutlet weak var txf_lastName: UITextField!
    @IBOutlet weak var txf_address: UITextField!
    @IBOutlet weak var txf_town: UITextField!
    @IBOutlet weak var txf_state: UITextField!
    @IBOutlet weak var txf_zipcode: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initTxf()
        shippingVC = true
    }
    
    func initTxf()  {
        self.txf_county.addPadding(.left(10))
        self.txf_firstName.addPadding(.left(10))
        self.txf_lastName.addPadding(.left(10))
        self.txf_address.addPadding(.left(10))
        self.txf_town.addPadding(.left(10))
        self.txf_state.addPadding(.left(10))
        self.txf_zipcode.addPadding(.left(10))
    }
    
    @IBAction func okBtnClicked(_ sender: Any) {
        if self.txf_county.text!.isEmpty || self.txf_firstName.text!.isEmpty || self.txf_lastName.text!.isEmpty || self.txf_address.text!.isEmpty || self.txf_town.text!.isEmpty || self.txf_state.text!.isEmpty || self.txf_zipcode.text!.isEmpty
        {
            self.progShowInfo(true, msg: "Input Error!")
        }
        else{
            shippingcountry = self.txf_county.text
            shippingfirstName = self.txf_firstName.text
            shippinglastName = self.txf_lastName.text
            shippingaddress = self.txf_address.text
            shippingtown = self.txf_town.text
            shippingstate = self.txf_state.text
            shippingzipcode = self.txf_zipcode.text
            self.gotoVC("CardCheckVC")
        }
    }
    @IBAction func closeBtnClicked(_ sender: Any) {
        self.dismiss(animated: true)
    }
}
