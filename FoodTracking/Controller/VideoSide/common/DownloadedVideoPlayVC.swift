//
//  DownloadedVideoPlayVC.swift
//  FoodTracking
//
//  Created by Mac on 4/23/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//



import Foundation
import UIKit
import AVKit
import AVFoundation
import Photos
public let VC_DownloadedVideoPlayVC = "DownloadedVideoPlayVC"
class DownloadedVideoPlayVC: UIViewController {

    @IBOutlet weak var videoPreviewLayer: UIView!
    @IBOutlet weak var blockView: UIView!
    private var player: AVPlayer!
    private var playerViewController = AVPlayerViewController()

    override func viewDidLoad() {
        super.viewDidLoad()
        (UIApplication.shared.delegate as! AppDelegate).restrictRotation = .all
        playVideoInView()
        NotificationCenter.default.addObserver(self, selector: #selector(preventScreenRecording1), name: NSNotification.Name.stop_recording, object: nil)
    }
    
    @objc func preventScreenRecording1(_ notification : Notification) {
        print("screen recording")
        DispatchQueue.main.asyncAfter(deadline: .now() + 15.0, execute: {
            let isCaptured = UIScreen.main.isCaptured
            if (isCaptured) {
                print("yes")
                self.dismiss(animated: true, completion: nil)
            }
            else {
                print("no")
                self.blockView.isHidden = true
            }
        })
    }

    func playVideoInView() {
        if let uri = videoUri{
            let baseUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
            let assetUrl = baseUrl.appendingPathComponent(uri)
            let url = assetUrl
            print(url)
            let avAssest = AVAsset(url: url)
            let playerItem = AVPlayerItem(asset: avAssest)
            let player = AVPlayer(playerItem: playerItem)
            playerViewController = AVPlayerViewController()
            playerViewController.player = player
            playerViewController.view.frame = videoPreviewLayer.frame
            self.addChild(playerViewController)
            self.view.addSubview(playerViewController.view)
            
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        if let fragmenttype = fragmentType{
            if fragmenttype == FragmentType.workout.rawValue{
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let MyVideoLibVC = storyboard.instantiateViewController(withIdentifier: "MyVideoLibVC") as! MyVideoLibVC
                UIApplication.shared.keyWindow?.rootViewController = MyVideoLibVC
            }else if fragmenttype == FragmentType.nutrition.rawValue{
                let storyboard = UIStoryboard(name: VC_MyVideoLib4NutritionVC, bundle: nil)
                let MyVideoLibVC = storyboard.instantiateViewController(withIdentifier: VC_MyVideoLib4NutritionVC) as! MyVideoLib4NutritionVC
                UIApplication.shared.keyWindow?.rootViewController = MyVideoLibVC
            }else{
                let storyboard = UIStoryboard(name: VC_MyVideoLib4Coaching, bundle: nil)
                let MyVideoLibVC = storyboard.instantiateViewController(withIdentifier: VC_MyVideoLib4Coaching) as! MyVideoLib4Coaching
                UIApplication.shared.keyWindow?.rootViewController = MyVideoLibVC
            }
        }
    }
}
