//
//  CardCheckVC.swift
//  FoodTracking
//
//  Created by Mac on 4/9/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import UIKit

class CardCheckVC: BaseVC1 {
    
    @IBOutlet weak var txf_cardNum: UITextField!
    @IBOutlet weak var txf_month: UITextField!
    @IBOutlet weak var txf_year: UITextField!
    @IBOutlet weak var txf_cvv: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.initTxf()
    }
    
    func initTxf()  {
        self.txf_cardNum.addPadding(.left(10))
        self.txf_month.addPadding(.left(10))
        self.txf_year.addPadding(.left(10))
        self.txf_cvv.addPadding(.left(10))
    }
    
    @IBAction func payBtnClicked(_ sender: Any) {
        if ds_checkoutModel.count != 0{
            if let fragmentype = fragmentType{
                var descriptionn = ""
                var productID = ""
                var count = 0
                for one in ds_checkoutModel{
                    count += 1
                    descriptionn = descriptionn + "Title : " + one.title + ", Type : " + one.type + ", Price : $" + one.price + ", Quantity : " + one.quantity
                    if count == ds_checkoutModel.count{
                        productID += one.id
                    }
                    else{
                        productID += one.id + "_"
                    }
                }
                if self.txf_cardNum.text!.isEmpty || self.txf_month.text!.isEmpty || self.txf_year.text!.isEmpty || self.txf_cvv.text!.isEmpty{
                    self.progShowInfo(true, msg: "Input Error!")
                }else{
                    if self.txf_year.text!.count != 2  || self.txf_year.text!.toInt() ?? 1 <= 0 || self.txf_month.text!.count != 2 || self.txf_month.text!.toInt() ?? 1 > 12 || self.txf_month.text!.toInt() ?? 1 <= 0 {
                        self.progShowInfo(true, msg: "Please input card expire date correctly")
                    }else{
                        let experationdate = "20" + self.txf_year.text! + "-" + self.txf_month.text!
                        let checkoutcardnumber = self.txf_cardNum.text ?? ""
                        let checkoutcardcode = self.txf_cvv.text ?? ""
                        
                        self.showProgress()
                        ApiManager.paymentProcess(pay_type:"manual" , productID: productID, type: fragmentype, user_id: user!.id, first_name: checkoutfirstName ?? "", last_name:checkoutlastName ?? "" , user_email: checkoutemail ?? "", user_phone: checkoutphone ?? "", country: checkoutcountry ?? "", address: checkoutaddress ?? "", city: checkouttown ?? "", state: checkoutstate ?? "", zipcode: checkoutzipcode ?? "", cardNumber: checkoutcardnumber, expirationDate: experationdate , cardCode:checkoutcardcode, amount: checkoutamount ?? "", description: descriptionn){
                            (isSuccess, data) in
                            self.hideProgress()
                            if isSuccess{
                                var count = 0
                                var ids = ""
                                var endday = ""
                                
                                for one in ds_checkoutModel{
                                    count += 1
                                    if count == ds_checkoutModel.count{
                                        ids += one.id
                                        let timeNow = Int(NSDate().timeIntervalSince1970) * 1000
                                        var enddayDouble: Int = 0
                                        if let period = one.period.toInt(){
                                            enddayDouble =  Int(timeNow + period  * 86400000)
                                        }
                                        endday += "\(enddayDouble)"
                                    }
                                    else{
                                        ids += one.id + "_"
                                        let timeNow = Int(NSDate().timeIntervalSince1970) * 1000
                                        let enddayDouble: Int =  Int(timeNow + one.period.toInt()! * 86400000)
                                        endday += "\(enddayDouble)" + "_"
                                    }
                                }
                                
                                // for saving card information
                                UserDefault.setString(key: Constants.CARD_FIRSTNAME, value: checkoutfirstName)
                                UserDefault.setString(key: Constants.CARD_LASTNAME, value: checkoutlastName)
                                UserDefault.setString(key: Constants.CARD_EMAIL, value: checkoutemail)
                                UserDefault.setString(key: Constants.CARD_PHONE, value: checkoutphone)
                                UserDefault.setString(key: Constants.CARD_COUNTRY, value: checkoutcountry)
                                UserDefault.setString(key: Constants.CARD_CARDCITY, value: checkouttown)
                                UserDefault.setString(key: Constants.CARD_STATE, value: checkoutstate)
                                UserDefault.setString(key: Constants.CARD_ZIPCODE, value: checkoutzipcode)
                                UserDefault.setString(key: Constants.CARD_ADDRESS, value: checkoutaddress)
                                UserDefault.setString(key: Constants.CARD_NUMBER, value: checkoutcardnumber)
                                UserDefault.setString(key: Constants.CARD_CODE, value: checkoutcardcode)
                                UserDefault.setString(key: Constants.CARD_EXPIERATIONDATE, value: experationdate)
                                
                                ApiManager.setPurchaseProduct(ids: ids, endDay: endday) { (isSuccess1, data1) in
                                    if isSuccess1{
                                        if fragmentype == FragmentType.workout.rawValue{
                                            self.gotoStoryBoardVC(VC_WorkoutVideoVC, fullscreen: true)
                                        }
                                        else if fragmentype == FragmentType.nutrition.rawValue{
                                            self.gotoStoryBoardVC(VC_NutritionistVC, fullscreen: true)
                                        }
                                        else {
                                            self.gotoStoryBoardVC(VC_contact, fullscreen: true)
                                        }
                                    }
                                    else{
                                        self.progShowInfo(true, msg: "Something wrong, Please try again.")
                                    }
                                }
                            }
                            else{
                                self.alertDisplay(alertController: self.alertMake("Something went wrong"))
                            }
                        }
                    }
                }
            }
        }
    }
    @IBAction func closeBtnClicked(_ sender: Any) {
        self.dismiss(animated: true)
    }
}
