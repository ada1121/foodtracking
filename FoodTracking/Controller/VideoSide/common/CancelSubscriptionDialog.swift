//
//  CancelSubscriptionDialog.swift
//  FoodTracking
//
//  Created by Mac on 4/13/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import UIKit
import SwiftyJSON
public let VC_CancelSubscriptionDialog = "CancelSubscriptionDialog"
class CancelSubscriptionDialog: BaseVC1 {

    @IBOutlet weak var lbl_toptitle: UILabel!
    @IBOutlet weak var lbl_content: UILabel!
    var ds_nutrionist = [WcnModel]()// for programvidoes
    override func viewDidLoad() {
        super.viewDidLoad()
        lbl_toptitle.text = "ARE YOU SURE?\n SUBSCRIPTION CACELLATION"
        lbl_content.text = "Are you sure you want to cancel this \n subscription? \nYou can not see video anymore. \n If you want to see again, you must \n purchase again."
    }
    
    @IBAction func cancelSubscriptionClicked(_ sender: Any) {
        if let model = cancelsubsriptionModel{
            if model.type != ProductType.program.rawValue {// general workoutvideo subscription cancel
                self.remove(fileName:model.uri)
                if let id = cancelsubsriptionModel?.id{
                    ApiManager.removeSubscription(product_id: id) { (isSuccess, data) in
                        if isSuccess{
                            if let fragmenttype = fragmentType{
                                if fragmenttype == FragmentType.workout.rawValue{
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    let MyVideoLibVC = storyboard.instantiateViewController(withIdentifier: "MyVideoLibVC") as! MyVideoLibVC
                                    UIApplication.shared.keyWindow?.rootViewController = MyVideoLibVC
                                }else if fragmenttype == FragmentType.nutrition.rawValue{
                                    let storyboard = UIStoryboard(name: VC_MyVideoLib4NutritionVC, bundle: nil)
                                    let MyVideoLibVC = storyboard.instantiateViewController(withIdentifier: VC_MyVideoLib4NutritionVC) as! MyVideoLib4NutritionVC
                                    UIApplication.shared.keyWindow?.rootViewController = MyVideoLibVC
                                }else{
                                    let storyboard = UIStoryboard(name: VC_MyVideoLib4Coaching, bundle: nil)
                                    let MyVideoLibVC = storyboard.instantiateViewController(withIdentifier: VC_MyVideoLib4Coaching) as! MyVideoLib4Coaching
                                    UIApplication.shared.keyWindow?.rootViewController = MyVideoLibVC
                                }
                            }
                        }else{
                            self.dismiss(animated: true, completion: nil)
                        }
                    }
                    
                }
            }
            else{ // programvideo subscriptioncancel
                ApiManager.removeAllProgramVideo(user_id: user!.id, product_id: model.id){
                    (isSuccess,data) in
                    self.hideProgress()
                    if isSuccess{
                        let dict = JSON(data as Any)
                        print(dict)
                        var num = 0
                        if dict.count == 0{
                            //self.dismiss(animated: true, completion: nil)
                        }
                        else{
                            for one in dict{
                                num += 1
                                let miniJSON = JSON(one.1)
                                self.ds_nutrionist.append(WcnModel(miniJSON))
                            }
                        }
                    }
                }
                
                
                if self.ds_nutrionist.count != 0{ // cancel subscription action for programvideo
                    var num = 0
                    for one in self.ds_nutrionist{
                        num += 1
                        if one.uri != ""{
                            self.remove(fileName: one.uri)
                        }
                        if num == self.ds_nutrionist.count{
                            ApiManager.removeSubscription(product_id: model.id) { (issuccess, data) in
                                if issuccess{
                                    self.gotoVC("MyVideoLibVC")
                                }
                                else{
                                    self.dismiss(animated: true, completion: nil)
                                }
                            }
                        }
                    }
                }
                else{
                    ApiManager.removeSubscription(product_id: model.id) { (issuccess, data) in
                        if issuccess{
                            self.gotoVC("MyVideoLibVC")
                        }
                        else{
                            self.dismiss(animated: true, completion: nil)
                        }
                    }
                }
            }
        }
    }
    
    @IBAction func cancelBtnClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func remove(fileName: String){
        let fileManager = FileManager.default
        let baseUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let assetUrl = baseUrl.appendingPathComponent(fileName)
        let url = assetUrl
        do {
            try fileManager.removeItem(at: url)
            print("removesuccess")
        } catch {
            print("Could not delete file: \(error)")
        }
    }
}
