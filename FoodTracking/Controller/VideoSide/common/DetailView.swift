//
//  DetailView.swift
//  FoodTracking
//
//  Created by Mac on 4/6/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import UIKit

var bottomtitle = ""
var imageURL = ""
var content = ""

class DetailView: BaseVC1 {

    @IBOutlet weak var txv_content: UITextView!
    @IBOutlet weak var lbl_topTitle: UILabel!
    @IBOutlet weak var lbl_bottomTitle: UILabel!
    @IBOutlet weak var imv_thumb: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lbl_topTitle.text = sub_title
        self.lbl_bottomTitle.text = bottomtitle
        self.txv_content.text = content
        let url = URL(string: imageURL)
        imv_thumb.kf.setImage(with: url,placeholder: nil)
    }
}
