//
//  CartDialogVC.swift
//  FoodTracking
//
//  Created by Mac on 4/7/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import UIKit

class CartDialogVC: BaseVC1 {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func continueBtnClick(_ sender: Any) {// when nothanks btn clicked

        if let fragmenttype = fragmentType{
            if fragmenttype == FragmentType.workout.rawValue{
                self.gotoStoryBoardVC(VC_WorkoutVideoVC, fullscreen: true)
            }else if fragmenttype == FragmentType.nutrition.rawValue{
                self.gotoStoryBoardVC(VC_NutritionistVC, fullscreen: true)
            }else{
                self.gotoStoryBoardVC(VC_contact, fullscreen: true)
            }
        }
        
    }
    
    @IBAction func viewCartBtnClicked(_ sender: Any) { // buy subscribtionbtn clickecd
 
        if let fragmenttype = fragmentType{
            if fragmenttype == FragmentType.workout.rawValue{
                self.gotoVC("ViewCartVC")
            }else if fragmenttype == FragmentType.nutrition.rawValue{
                self.gotoVC("NutrioinCheckoutVC")
            }else{
                self.gotoVC("ContactCheckVC")
            }
        }
    }
}
