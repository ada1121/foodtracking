//
//  ProgramVideoGuides.swift
//  FoodTracking
//
//  Created by Mac on 4/13/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import UIKit

class ProgramVideoGuides: BaseVC1 {
    
    @IBOutlet weak var lbl_content: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lbl_content.text = "Each video you purchase must be downloaded so that it will appear in MY VIDEO LIBRARY Click PROGRAM VIDEOS to begin your download."
    }
   
    @IBAction func okBtnClicked(_ sender: Any) {
        self.gotoVC("ProgramVideosVC")
    }
}
