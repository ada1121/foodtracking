//
//  ProgramVideosVC.swift
//  FoodTracking
//
//  Created by Mac on 4/13/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import UIKit
import SwiftyJSON
import SwiftyUserDefaults
import Alamofire
import Foundation

import AVKit
import AVFoundation
import Photos

class ProgramVideosVC: BaseVC1 {
    
    @IBOutlet weak var ui_collectionView: UICollectionView!
    var ds_nutrionist = [WcnModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(false)
        self.getProgramVideos()
    }
    
    @IBAction func detailBtnClicked(_ sender: Any) {
        let button: UIButton = sender as! UIButton
        let index = button.tag
        sub_title = self.ds_nutrionist[index].title
        bottomtitle = self.ds_nutrionist[index].subtitle
        content = self.ds_nutrionist[index].minidescription + "\n" + "\n" + self.ds_nutrionist[index].fulldescription
        imageURL = self.ds_nutrionist[index].image
        
        self.gotoStoryBoardVCModal("DetailView")
    }
    
    @IBAction func downBtnClicked(_ sender: Any) {
        
        let button: UIButton = sender as! UIButton
        let index = button.tag
        if rootVC == 1{// download action
             let model = self.ds_nutrionist[index]
             
            if model.uri == ""{
                
                downloadModel = model
                
                let documentsUrl:URL =  (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first as URL?)!
              let name = randomString(length: 3) + ".mp4"
              let destinationFileUrl = documentsUrl.appendingPathComponent(name)
              
              //Create URL to the source file you want to download
              let fileURL = URL(string: model.video)
              
              let sessionConfig = URLSessionConfiguration.default
              let session = URLSession(configuration: sessionConfig)

              let request = URLRequest(url:fileURL!)
              
              let task = session.downloadTask(with: request) { (tempLocalUrl, response, error) in
                  if let tempLocalUrl = tempLocalUrl, error == nil {
                      // Success
                      if let statusCode = (response as? HTTPURLResponse)?.statusCode {
                          print("Successfully downloaded. Status code: \(statusCode)")
                        /*ApiManager.setProgramVideoUri(user_id: user!.id, programvideo_id: model.id, uri: name){
                             (isSuccess,data) in
                             if isSuccess{
                                 
                             }
                             else{
                                 
                             }
                         }*/
                        ApiManager.setProgramVideoUri(user_id: user!.id, programid: model.id, uri: name) { (isSuccess, data) in
                            
                            if isSuccess{
                                
                            }
                            else{
                                
                            }
                        }
                      }
                      
                      do {
                          try FileManager.default.copyItem(at: tempLocalUrl, to: destinationFileUrl)
                      } catch (let writeError) {
                          print("Error creating a file \(destinationFileUrl) : \(writeError)")
                      }
                      
                  } else {
                      print("Error took place while downloading a file. Error description: %@", error?.localizedDescription);
                  }
              }
              task.resume()
            }
            self.gotoVC("VideoShowVC")
        }
            
        else{ // play action
            let button: UIButton = sender as! UIButton
            let index = button.tag
            videoUri = self.ds_nutrionist[index].uri
            self.gotoVC("DownloadedVideoPlayVC")
        }
        
        
        /*if ds_nutrionist[index].uri == ""{
            let video = self.ds_nutrionist[index].video
            guard let videoURL = URL(string: video ) else {
                return
            }
            let player = AVPlayer(url: videoURL)
            let vc = AVPlayerViewController()
            vc.player = player
            present(vc, animated: true) {
                player.play()
            }
        }
        else{
            
        }*/
    }
    
    @IBAction func closeBtnClicked(_ sender: Any) {
        if rootVC == 1{
            
            if dialogIndex == 0{
                //self.gotoVC("WorkoutVideoVC")
                self.gotoStoryBoardVC("WorkoutVideoVC", fullscreen: true)
            }
            
            else if dialogIndex == 11{
                //self.gotoVC("CompleteProgramsVC")
                self.gotoStoryBoardVC("WorkoutVideoVC", fullscreen: true)
            }
                
            else if dialogIndex == 12{
                //self.gotoVC("SearchVC")
                self.gotoStoryBoardVC("WorkoutVideoVC", fullscreen: true)
            }
            
        }
            
        else if rootVC == 2{
            self.gotoVC("MyVideoLibVC")
        }
    }
    
    @IBAction func cancelSubscriptionBtnClicked(_ sender: Any) {// programvideoonefile delete action
        let button: UIButton = sender as! UIButton
        let index = button.tag
        let id = ds_nutrionist[index].id
        ApiManager.removeProgramVideo1(user_id: user!.id, programvideo_id: id){
            (isSuccess, data) in
            if isSuccess{
                self.getProgramVideos()
            }
            else{
                
            }
        }
    }
    
    func getProgramVideos() {
        
        self.showProgress()
        
        if let id = programvideoID{
            ApiManager.getProgramVideos(user_id: user!.id, workoutvideo_id: id){
                (isSuccess,data) in
                self.hideProgress()
                if isSuccess{
                    let dict = JSON(data as Any)
                    //print(dict)
                    self.ds_nutrionist.removeAll()
                    var num = 0
                    if dict.count == 0{
                        if rootVC == 1{
                            
                            if dialogIndex == 0{
                                //self.gotoVC("WorkoutVideoVC")
                                self.gotoStoryBoardVC("WorkoutVideoVC", fullscreen: true)
                            }
                            
                            else if dialogIndex == 11{
                                self.gotoStoryBoardVC("WorkoutVideoVC", fullscreen: true)
                            }
                                
                            else if dialogIndex == 12{
                                self.gotoStoryBoardVC("WorkoutVideoVC", fullscreen: true)
                            }
                            
                        }
                            
                        else if rootVC == 2{
                            self.gotoVC("MyVideoLibVC")
                        }
                        
                        //self.dismiss(animated: true, completion: nil)
                    }
                    else{
                        for one in dict{
                            num += 1
                            let miniJSON = JSON(one.1)
                            if rootVC == 1{// for workoutVideo
                                self.ds_nutrionist.append(WcnModel(miniJSON))
                            }
                            else if rootVC == 2{// for myVideoLib
                                if miniJSON["uri"].stringValue != ""{
                                    self.ds_nutrionist.append(WcnModel(miniJSON))
                                }
                            }
                            
                            if num == dict.count{
                                if self.ds_nutrionist.count == 0{
                                    self.dismiss(animated: true, completion: nil)
                                }
                                else{
                                    self.ui_collectionView.reloadData()
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

extension ProgramVideosVC : UICollectionViewDelegate, UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return ds_nutrionist.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionCell", for: indexPath) as! CollectionCell
        cell.entity = ds_nutrionist[indexPath.row]
        cell.btn_buy.tag = indexPath.row
        cell.btn_detail.tag = indexPath.row
        cell.btn_cancelSubscription.tag = indexPath.row
        cell.imageBtn.tag = indexPath.row
        
        if rootVC == 1{
            if ds_nutrionist[indexPath.row].uri == ""{
                //cell.btn_buy.setImage(UIImage.init(named: "btn_back"), for: .normal)
                cell.lbl_caption4program.text = "DOWNLOAD NOW!"
            }
            else{
                //cell.btn_buy.setImage(UIImage.init(named: "btn_back2"), for: .normal)
                cell.btn_buy.backgroundColor = UIColor.init(named: "ColorPurchased")
                cell.lbl_caption4program.text = "DOWNLOADED"
            }
        }
        else if rootVC == 2{
            cell.lbl_caption4program.text = "PLAY"
            cell.btn_cancelSubscription.isHidden = false
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    }
}

extension ProgramVideosVC : UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
       let w = collectionView.frame.size.width
        let h = collectionView.frame.size.height / 2.2
          
        return CGSize(width: w, height: h)
    }
}

