//
//  WorkoutVideoVC.swift
//  FoodTracking
//
//  Created by Mac on 4/23/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//


import UIKit
import SwiftyJSON
import SwiftyUserDefaults
import Alamofire

var rootVC: Int! = 0
public let VC_WorkoutVideoVC = "WorkoutVideoVC"
class WorkoutVideoVC : BaseVC1 {
    
    @IBOutlet weak var ui_collectionView: UICollectionView!
    @IBOutlet weak var str_myVideoTab: UILabel!
    @IBOutlet weak var str_mylibraryTab: UILabel!
    @IBOutlet weak var str_contacTab: UILabel!
    
    @IBOutlet weak var str_videosTab: UILabel!
    @IBOutlet weak var str_programmevideosTab: UILabel!
    @IBOutlet weak var str_searchtab: UILabel!
    
    @IBOutlet weak var str_middleTab: UILabel!
    @IBOutlet weak var uiv_tab1: UIView!
    @IBOutlet weak var uiv_tab2: UIView!
    @IBOutlet weak var uiv_tab3: UIView!
    
    @IBOutlet weak var uiv_myvideotab: UIView!
    @IBOutlet weak var uiv_mylibrary: UIView!
    @IBOutlet weak var uiv_viewcart: UIView!
    
    @IBOutlet weak var cons_collectionViewTop: NSLayoutConstraint!
    
    @IBOutlet weak var txf_search: UITextField!
    var ds_filtered = [WcnModel]()
    
    //Variables for search
    var searchActive : Bool = false
    var data :[String] = []
    var data1 :[String] = []
    var data2 :[String] = []
    var data3 :[String] = []
    var data4 :[String] = []
    var filtered:[String] = []
    var filtered1:[String] = []
    var filtered2:[String] = []
    var filtered3:[String] = []
    var filtered4:[String] = []
    
    var ds_workoutVideo = [WcnModel]()
    var detailDelegate: onDetailDelegate!
    var indexpPathrow = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dialogIndex = 0
        rootVC = 1
        str_myVideoTab.text = "MY VIDEO \n STORE"
        str_mylibraryTab.text = "MY VIDEOS \n LIBRARY"
        str_contacTab.text = "CONTACT \n COATCH"
        str_videosTab.text = "INDIVIDUAL \n VIDEOS"
        str_programmevideosTab.text = "COMPLETE \n PROGRAMS"
        str_middleTab.text = "WORKOUT ANYWHERE ANY DEVICE \n Online - Offline"
        
        txf_search.addPadding(.left(16))
        txf_search.delegate = self
        txf_search.addTarget(self, action: #selector(SearchVC.textFieldDidChange(_:)),
        for: UIControl.Event.editingChanged)
        txf_search.isHidden = true
        fragmentType = FragmentType.workout.rawValue
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(false)
        
        if subtabType != nil{
            if subtabType == WorkoutVideoType.individual_videos.rawValue{
                self.getWorkoutVideo()
                setTopBarView(1)
                setBottomBarView(1)
                subtabType = WorkoutVideoType.individual_videos.rawValue
            }else if subtabType == WorkoutVideoType.completed_programs.rawValue{
                self.getProgramVideos()
                setTopBarView(1)
                setBottomBarView(2)
                subtabType = WorkoutVideoType.completed_programs.rawValue
            }else if subtabType == WorkoutVideoType.workout_search.rawValue{
                setTopBarView(1)
                setBottomBarView(3)
                self.getWorkoutVideo()
                self.setSearchTab(true)
                subtabType = WorkoutVideoType.workout_search.rawValue
            }else{
                self.getWorkoutVideo()
                setTopBarView(1)
                setBottomBarView(1)
                subtabType = WorkoutVideoType.individual_videos.rawValue
            }
        }
        else{
            self.getWorkoutVideo()
            setTopBarView(1)
            setBottomBarView(1)
            subtabType = WorkoutVideoType.individual_videos.rawValue
        }
    }
    
    func setTopBarView(_ tabnum : Int) {
        if tabnum == 1{
            self.uiv_myvideotab.isHidden = false
            self.uiv_mylibrary.isHidden = true
            self.uiv_viewcart.isHidden = true
        }
        else if tabnum == 2{
            self.uiv_myvideotab.isHidden = true
            self.uiv_mylibrary.isHidden = false
            self.uiv_viewcart.isHidden = true
        }
        else if tabnum == 3{
            self.uiv_myvideotab.isHidden = true
            self.uiv_mylibrary.isHidden = true
            self.uiv_viewcart.isHidden = false
        }
    }
    
    func setBottomBarView(_ tabnum : Int) {
        if tabnum == 1{
            self.uiv_tab1.isHidden = false
            self.uiv_tab2.isHidden = true
            self.uiv_tab3.isHidden = true
            self.str_videosTab.textColor = .white
            self.str_programmevideosTab.textColor = .lightGray
            self.str_searchtab.textColor = .lightGray
        }
        else if tabnum == 2{
            self.uiv_tab1.isHidden = true
            self.uiv_tab2.isHidden = false
            self.uiv_tab3.isHidden = true
            self.str_videosTab.textColor = .lightGray
            self.str_programmevideosTab.textColor = .white
            self.str_searchtab.textColor = .lightGray
        }
        else if tabnum == 3{
            self.uiv_tab1.isHidden = true
            self.uiv_tab2.isHidden = true
            self.uiv_tab3.isHidden = false
            self.str_videosTab.textColor = .lightGray
            self.str_programmevideosTab.textColor = .lightGray
            self.str_searchtab.textColor = .white
        }
    }
    
    func getWorkoutVideo() {
        self.showProgress()
        ApiManager.getVideos(video_type: R.string.WORKOUT) { (isSuccess, data) in
            self.hideProgress()
            
            if isSuccess{
                let dict = JSON(data as Any)
                //print(dict)
                var num = 0
                self.ds_workoutVideo.removeAll()
                for one in dict{
                    num += 1
                    let miniJSON = JSON(one.1)
                    if miniJSON[PARAMS.VIDEO].stringValue != ""{
                        self.ds_workoutVideo.append(WcnModel(miniJSON))
                        self.data.append(miniJSON[PARAMS.SUBTITLE].stringValue)
                        self.data1.append(miniJSON[PARAMS.TITLE].stringValue)
                        self.data2.append(miniJSON[PARAMS.FULLDESCRIPTION].stringValue)
                        self.data3.append(miniJSON[PARAMS.PRICE].stringValue)
                        self.data4.append(miniJSON[PARAMS.MINIDESCRIPTION].stringValue)
                    }
                    if num == dict.count{
                        self.ui_collectionView.reloadData()
                    }
                }
            }
        }
    }
    
    func getProgramVideos() {
            self.showProgress()
        ApiManager.getVideos(video_type: R.string.PROGRAM) { (isSuccess, data) in
                self.hideProgress()
                
                if isSuccess{
                    let dict = JSON(data as Any)
                    //print(dict)
                    var num = 0
                    self.ds_workoutVideo.removeAll()
                    for one in dict{
                        num += 1
                        let miniJSON = JSON(one.1)
                        if miniJSON[PARAMS.VIDEO].stringValue == ""{
                            self.ds_workoutVideo.append(WcnModel(miniJSON))
                        }
                        if num == dict.count{
                            self.setBottomBarView(2)
                            self.ui_collectionView.reloadData()
                        }
                    }
                }
            }
        }
    
    @IBAction func back(_ sender: Any) {
        self.gotoStoryBoardVC("MainVC", fullscreen: true)
    }
    
    @IBAction func gotoMyVideo(_ sender: Any) { // My video store redirect
        self.setTopBarView(1)
    }
    
    @IBAction func gotoMylab(_ sender: Any) { // goto my video library
        //self.setTopBarView(2)
        self.gotoVC("MyVideoLibVC")
    }
    
    @IBAction func viewCartBtnClicked(_ sender: Any) { // view cart
        self.setTopBarView(3)
        fragmentType = FragmentType.workout.rawValue
        self.gotoVC("ViewCartVC")
    }
    
    @IBAction func goContactcoach(_ sender: Any) { // view cart
        creatNav1()
        self.openMenu1()
    }

    @IBAction func buyBtnClicked(_ sender: Any) {
        let button: UIButton = sender as! UIButton
        let index = button.tag
        if self.ds_workoutVideo[index].status == "yes"{
            if ds_workoutVideo[index].video != ""{// downloadguide action
                downloadvideoID = self.ds_workoutVideo[index].id
                downloadModel = self.ds_workoutVideo[index]
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let controller = storyboard.instantiateViewController(withIdentifier: "VideoDownloadGuideVC")
                controller.modalPresentationStyle = .fullScreen
                self.present(controller, animated: false, completion: nil)
            }
            else{ // programvideo guides
                programvideoID = self.ds_workoutVideo[index].id
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let controller = storyboard.instantiateViewController(withIdentifier: "ProgramVideoGuides")
                controller.modalPresentationStyle = .fullScreen
                self.present(controller, animated: false, completion: nil)
                //self.gotoVC("ProgramVideoGuides")
            }
        }
        else{
            nutritionModel = self.ds_workoutVideo[index]
            self.gotoVCModal(VC_SubscriptionRenewDialog)
        }
    }
    
    @IBAction func detailBtnClicked(_ sender: Any) {
        let button: UIButton = sender as! UIButton
        let index = button.tag
        //dump(ds_workoutVideo[index],name: "")
        sub_title = self.ds_workoutVideo[index].title
        bottomtitle = self.ds_workoutVideo[index].subtitle
        content = self.ds_workoutVideo[index].minidescription + "\n" + "\n" + self.ds_workoutVideo[index].fulldescription
        imageURL = self.ds_workoutVideo[index].image
        self.gotoStoryBoardVCModal("DetailView")
    }
    
    @IBAction func gotoSubtab1(_ sender: Any) {// videos tab
        if subtabType != WorkoutVideoType.individual_videos.rawValue{
            setBottomBarView(1)
            self.setSearchTab(false)
            self.getWorkoutVideo()
        }else{
            return
        }
        subtabType = WorkoutVideoType.individual_videos.rawValue
    }
    
    @IBAction func gotoSubtab2(_ sender: Any) {// programs tab
        //self.gotoVC("CompleteProgramsVC")
        if subtabType != WorkoutVideoType.completed_programs.rawValue{
            self.setSearchTab(false)
            setBottomBarView(2)
            self.getProgramVideos()
        }else{
            return
        }
        subtabType = WorkoutVideoType.completed_programs.rawValue
    }
    
    @IBAction func gotoSubtab3(_ sender: Any) {// search tab
        //self.gotoVC("SearchVC")
        if subtabType != WorkoutVideoType.workout_search.rawValue{
            setBottomBarView(3)
            self.getWorkoutVideo()
            self.setSearchTab(true)
        }else{
            return
        }
        subtabType = WorkoutVideoType.workout_search.rawValue
    }
    
    func setSearchTab(_ showstate: Bool) {
        if showstate{
            self.ui_collectionView.isHidden = true
            self.cons_collectionViewTop.constant = 90
            self.txf_search.isHidden = false
        }
        else{
            self.ui_collectionView.isHidden = false
            self.cons_collectionViewTop.constant = 0
            self.txf_search.isHidden = true
        }
    }
}

extension WorkoutVideoVC : UICollectionViewDelegate, UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return ds_workoutVideo.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionCell", for: indexPath) as! CollectionCell
        cell.entity = ds_workoutVideo[indexPath.row]
        indexpPathrow = indexPath.row
        
        if ds_workoutVideo[indexPath.row].status == "yes"
        {
            if ds_workoutVideo[indexPath.row].video != ""{
                if ds_workoutVideo[indexPath.row].uri == ""{
                    cell.lbl_price.text = "DOWNLOAD NOW!"
                    cell.lbl_price.isHidden = false
                    cell.btn_buy.isHidden = false
                }
                else{
                    cell.lbl_price.isHidden = true
                    cell.btn_buy.isHidden = true
                    
                    /*let timeNow : Double = Double(Int(NSDate().timeIntervalSince1970) * 1000)
                    if timeNow > ds_workoutVideo[indexPath.row].endDay.toDouble()!{
                        self.auto_paymentProcess(ds_workoutVideo[indexPath.row])
                    }*/
                }
            }
            else{
                cell.lbl_price.text = "PRGRAM VIDEOS"
            }
            
            cell.btn_purchased.isHidden = false
            cell.lbl_purchased.isHidden = false
        }
        else{
            cell.btn_purchased.isHidden = true
            cell.lbl_purchased.isHidden = true
            cell.lbl_price.isHidden = false
            cell.btn_buy.isHidden = false
        }
        
        cell.btn_buy.tag = indexPath.row
        cell.btn_detail.tag = indexPath.row
        cell.imageBtn.tag = indexPath.row
        
        /*cell.buyBlock = {
            print("click Buy button")
            
            self.doBuyAction(indexPath.row)
        }*/
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
    /*fileprivate func doBuyAction(_ index: Int) {
        /*let vc = storyboard?.instantiateViewController(withIdentifier: "SubscriptionDialog") as! SubscriptionDialog*/
        sub_price = ds_workoutVideo[index].price
        sub_title = ds_workoutVideo[index].subtitle
        //self.gotoVC("SubscriptionDialog")
    }*/
}

extension WorkoutVideoVC : UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        switch UIDevice.current.userInterfaceIdiom {
           case .phone:
            let w = collectionView.frame.size.width
            let h = collectionView.frame.size.height / 2.2
            return CGSize(width: w, height: h)
            
           case .pad:
               /*let w = collectionView.frame.size.width
               let h = collectionView.frame.size.height / 1.6
                 
               return CGSize(width: w, height: h)*/
            let w = collectionView.frame.size.width
            let formatter = NumberFormatter()
            formatter.numberStyle = NumberFormatter.Style.decimal
            formatter.roundingMode = NumberFormatter.RoundingMode.ceiling
            formatter.maximumFractionDigits = 0
            
            let title = ds_workoutVideo[indexPath.row].title
            let font = UIFont.systemFont(ofSize: 30)
            let title_width = title.size(OfFont: font).width // size: {w: 98.912 h: 14.32}
            let title_height = title.size(OfFont: font).height
            let rate : CGFloat = title_width / (UIScreen.main.bounds.width - 30)
            
            let roundedValue1 = formatter.string(for: rate)
            let prefix_number = roundedValue1?.toInt()
            let toptitle_height = title_height * CGFloat(prefix_number!)
            
            //================================================================
            let subtitle = ds_workoutVideo[indexPath.row].subtitle
            let subfont = UIFont.systemFont(ofSize: 28)
            let subtitle_width = subtitle.size(OfFont: subfont).width // size: {w: 98.912 h: 14.32}
            let subtitle_height = subtitle.size(OfFont: subfont).height
            let subrate : CGFloat = subtitle_width / (UIScreen.main.bounds.width / 2)
            let roundedValue4subtitle = formatter.string(for: subrate)
            let prefix_number4subtitle = roundedValue4subtitle?.toInt()
            let subtitleheight = subtitle_height * CGFloat(prefix_number4subtitle!)
            let image_height: CGFloat = 380
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionCell", for: indexPath) as! CollectionCell
            
            print("imageHeight======>", cell.imageBtn.height)
            print("toptitleHeight======>", cell.lbl_topTitle.height)
            let h = toptitle_height + image_height + subtitleheight
            
            return CGSize(width: w, height: h)

           
        case .unspecified:
            let w = collectionView.frame.size.width
            let h = collectionView.frame.size.height / 2.5
              
            return CGSize(width: w, height: h)
        case .tv:
            let w = collectionView.frame.size.width
            let h = collectionView.frame.size.height / 2.5
              
            return CGSize(width: w, height: h)
        case .carPlay:
            let w = collectionView.frame.size.width
            let h = collectionView.frame.size.height / 2.5
              
            return CGSize(width: w, height: h)
        @unknown default:
            let w = collectionView.frame.size.width
            let h = collectionView.frame.size.height / 2.5
              
            return CGSize(width: w, height: h)
        }
    }
}

public protocol onDetailDelegate {
    func onDetail(subtitle: String, price: String)
}

enum UIUserInterfaceIdiom : Int {
    case unspecified

    case phone // iPhone and iPod touch style UI
    case pad   // iPad style UI (also includes macOS Catalyst)
}

extension WorkoutVideoVC: UITextFieldDelegate {
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        
        let searchText  = textField.text
        
        filtered = data.filter({ (text) -> Bool in
            let tmp: NSString = text as NSString
            let range = tmp.range(of: searchText!, options: NSString.CompareOptions.caseInsensitive)
            /*print("range =======",range)
            print("rangeLocation =======",range.location)
            print("Nsfound =======",range.location != NSNotFound)*/
            return range.location != NSNotFound
        })
        
        filtered1 = data1.filter({ (text) -> Bool in
            let tmp: NSString = text as NSString
            let range = tmp.range(of: searchText!, options: NSString.CompareOptions.caseInsensitive)
            /*print("range =======",range)
            print("rangeLocation =======",range.location)
            print("Nsfound =======",range.location != NSNotFound)*/
            return range.location != NSNotFound
        })
        
        filtered2 = data2.filter({ (text) -> Bool in
            let tmp: NSString = text as NSString
            let range = tmp.range(of: searchText!, options: NSString.CompareOptions.caseInsensitive)
            /*print("range =======",range)
            print("rangeLocation =======",range.location)
            print("Nsfound =======",range.location != NSNotFound)*/
            return range.location != NSNotFound
        })
        
        filtered3 = data3.filter({ (text) -> Bool in
            let tmp: NSString = text as NSString
            let range = tmp.range(of: searchText!, options: NSString.CompareOptions.caseInsensitive)
            /*print("range =======",range)
            print("rangeLocation =======",range.location)
            print("Nsfound =======",range.location != NSNotFound)*/
            return range.location != NSNotFound
        })
        
        filtered4 = data4.filter({ (text) -> Bool in
            let tmp: NSString = text as NSString
            let range = tmp.range(of: searchText!, options: NSString.CompareOptions.caseInsensitive)
            /*print("range =======",range)
            print("rangeLocation =======",range.location)
            print("Nsfound =======",range.location != NSNotFound)*/
            return range.location != NSNotFound
        })
        
        if(filtered.count == 0 && filtered1.count == 0 && filtered2.count == 0 && filtered3.count == 0 && filtered4.count == 0 ){
            searchActive = false;
        } else {
            searchActive = true;
        }
        if searchActive{
            ui_collectionView.isHidden = false
            self.ds_filtered.removeAll()
            
            let ids = self.getDataSourceidFromFilterd(filtered: filtered, type: PARAMS.SUBTITLE)
            + self.getDataSourceidFromFilterd(filtered: filtered1, type: PARAMS.TITLE)
            + self.getDataSourceidFromFilterd(filtered: filtered2, type: PARAMS.FULLDESCRIPTION)
            + self.getDataSourceidFromFilterd(filtered: filtered3, type: PARAMS.PRICE)
            + self.getDataSourceidFromFilterd(filtered: filtered4, type: PARAMS.MINIDESCRIPTION)
            
            
            for one in ids.removeDuplicates(){
                self.ds_filtered.append(self.getDataFromID(one))
                self.ui_collectionView.reloadData()
            }
            
        }
        else{
            ui_collectionView.isHidden = true
        }
    }
    
    func getDataSourceidFromFilterd(filtered: [String],type: String) -> [String] {
        var dataSourceid = [String]()
        var dataSourceid1 = [String]()
        var dataSourceid2 = [String]()
        var dataSourceid3 = [String]()
        var dataSourceid4 = [String]()
        
        for one in self.ds_workoutVideo{
            for two in filtered{
                if type == PARAMS.SUBTITLE{
                    if one.subtitle == two{
                        dataSourceid.append(one.id)
                    }
                }
                
                else if type == PARAMS.TITLE{
                    if one.title == two{
                        dataSourceid1.append(one.id)
                    }
                }
                
                else if type == PARAMS.FULLDESCRIPTION{
                    if one.fulldescription == two{
                        dataSourceid2.append(one.id)
                    }
                }
                
                else if type == PARAMS.MINIDESCRIPTION{
                    if one.minidescription == two{
                        dataSourceid3.append(one.id)
                    }
                }
                
                else if type == PARAMS.PRICE{
                    if one.price == two{
                        dataSourceid4.append(one.id)
                    }
                }
            }
        }
        
        dataSourceid += dataSourceid1
        dataSourceid += dataSourceid2
        dataSourceid += dataSourceid3
        dataSourceid += dataSourceid4
        
        print(dataSourceid.removeDuplicates())
        return dataSourceid.removeDuplicates()
    }
    
    func getDataFromID(_ id: String) -> WcnModel {
        var returnModel : WcnModel?
        for one in self.ds_workoutVideo{
            if id == one.id{
                returnModel = one
            }
        }
        return returnModel ?? WcnModel()
    }
}

extension Array where Element:Equatable {
    func removeDuplicates() -> [Element] {
        var result = [Element]()

        for value in self {
            if result.contains(value) == false {
                result.append(value)
            }
        }
        return result
    }
}




