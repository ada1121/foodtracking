//
//  SearchVC.swift
//  FoodTracking
//
//  Created by Mac on 4/23/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//



import UIKit
import SwiftyJSON
import SwiftyUserDefaults
import Alamofire

class SearchVC : BaseVC1 {
    
    @IBOutlet weak var ui_collectionView: UICollectionView!
    @IBOutlet weak var txf_search: UITextField!
    
    var ds_workoutVideo = [WcnModel]()
    var ds_filtered = [WcnModel]()
    
    
    //Variables for search
    var searchActive : Bool = false
    var data :[String] = []
    var data1 :[String] = []
    var data2 :[String] = []
    var data3 :[String] = []
    var data4 :[String] = []
    var filtered:[String] = []
    var filtered1:[String] = []
    var filtered2:[String] = []
    var filtered3:[String] = []
    var filtered4:[String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dialogIndex = 12
        //rootVC = 1
        txf_search.addPadding(.left(16))
        txf_search.delegate = self
        txf_search.addTarget(self, action: #selector(SearchVC.textFieldDidChange(_:)),
        for: UIControl.Event.editingChanged)
        ui_collectionView.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(false)
        self.getWorkoutVideo()
    }
    
    func getWorkoutVideo() {
        self.showProgress()
        ApiManager.getWorkoutVideo(user_id: user?.id ?? 0){
            (isSuccess,data) in
            self.hideProgress()
            if isSuccess{
                let dict = JSON(data as Any)
                //print(dict)
                var num = 0
                self.ds_workoutVideo.removeAll()
                self.data.removeAll()
                for one in dict{
                    num += 1
                    let miniJSON = JSON(one.1)
                    self.ds_workoutVideo.append(WcnModel(miniJSON))
                    self.data.append(miniJSON[PARAMS.SUBTITLE].stringValue)
                    self.data1.append(miniJSON[PARAMS.TITLE].stringValue)
                    self.data2.append(miniJSON[PARAMS.FULLDESCRIPTION].stringValue)
                    self.data3.append(miniJSON[PARAMS.PRICE].stringValue)
                    self.data4.append(miniJSON[PARAMS.MINIDESCRIPTION].stringValue)
                    if num == dict.count{
                        //self.ui_collectionView.reloadData()
                    }
                }
            }
        }
    }
    
    @IBAction func back(_ sender: Any) {
        self.gotoStoryBoardVC("MainVC", fullscreen: true)
    }
    @IBAction func gotoMylab(_ sender: Any) {
        self.gotoVC("MyVideoLibVC")
    }
    
    @IBAction func viewCartBtnClicked(_ sender: Any) {
        self.gotoVC("ViewCartVC")
    }

    @IBAction func buyBtnClicked(_ sender: Any) {
        
        let button: UIButton = sender as! UIButton
        let index = button.tag
        
        if self.ds_filtered[index].status == "yes"{
            if ds_filtered[index].video != ""{// downloadguide action
                
               downloadvideoID = self.ds_filtered[index].id
               downloadModel = self.ds_filtered[index]
               let storyboard = UIStoryboard(name: "Main", bundle: nil)
               let controller = storyboard.instantiateViewController(withIdentifier: "VideoDownloadGuideVC")
               controller.modalPresentationStyle = .fullScreen
               self.present(controller, animated: false, completion: nil)
            }
            else{ // programvideo guides
                programvideoID = self.ds_filtered[index].id
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let controller = storyboard.instantiateViewController(withIdentifier: "ProgramVideoGuides")
                controller.modalPresentationStyle = .pageSheet
                self.present(controller, animated: false, completion: nil)
                //self.gotoVC("ProgramVideoGuides")
            }
        }
        else{
            workoutVideoId = self.ds_filtered[index].id.toInt() ?? -1
            sub_price = ds_filtered[index].price
            sub_title = ds_filtered[index].subtitle
            //detailDelegate.onDetail(subtitle: subtitle, price: price)
            self.gotoVC("SubscriptionDialog")
        }
    }
    
    @IBAction func detailBtnClicked(_ sender: Any) {
        let button: UIButton = sender as! UIButton
        let index = button.tag
        
        sub_title = self.ds_filtered[index].subtitle
        bottomtitle = self.ds_filtered[index].title
        content = self.ds_filtered[index].minidescription + "\n" + "\n" + self.ds_filtered[index].fulldescription
        imageURL = self.ds_filtered[index].image
    }
    
    @IBAction func gotoSubtab1(_ sender: Any) {
        //self.gotoVC("WorkoutVideoVC")
        self.gotoStoryBoardVC("WorkoutVideoVC", fullscreen: true)
    }
    
    @IBAction func gotoSubtab2(_ sender: Any) {
        self.gotoVC("CompleteProgramsVC")
    }
    @IBAction func gotoSubtab3(_ sender: Any) {
        return
    }
}

extension SearchVC : UICollectionViewDelegate, UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return ds_filtered.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionCell", for: indexPath) as! CollectionCell
        if ds_filtered.count > indexPath.row{
            cell.entity = ds_filtered[indexPath.row]
            if ds_filtered[indexPath.row].status == "yes"
            {
                if ds_filtered[indexPath.row].video != ""{
                    if ds_filtered[indexPath.row].uri == ""{
                        cell.lbl_price.text = "DOWNLOAD NOW!"
                        cell.lbl_price.isHidden = false
                        cell.btn_buy.isHidden = false
                    }
                    else{
                        cell.lbl_price.isHidden = true
                        cell.btn_buy.isHidden = true
                    }
                }
                else{
                    cell.lbl_price.text = "PRGRAM VIDEOS"
                }
                cell.btn_purchased.isHidden = false
                cell.lbl_purchased.isHidden = false
            }
            else{
                cell.btn_purchased.isHidden = true
                cell.lbl_purchased.isHidden = true
                cell.lbl_price.isHidden = false
                cell.btn_buy.isHidden = false
            }
            
            cell.btn_buy.tag = indexPath.row
            cell.btn_detail.tag = indexPath.row
            
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    }
}

extension SearchVC : UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let w = collectionView.frame.size.width
        let h = collectionView.frame.size.height / 1.6
          
        return CGSize(width: w, height: h)
    }
}

extension SearchVC: UITextFieldDelegate {
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        
        let searchText  = textField.text
        
        filtered = data.filter({ (text) -> Bool in
            let tmp: NSString = text as NSString
            let range = tmp.range(of: searchText!, options: NSString.CompareOptions.caseInsensitive)
            /*print("range =======",range)
            print("rangeLocation =======",range.location)
            print("Nsfound =======",range.location != NSNotFound)*/
            return range.location != NSNotFound
        })
        
        filtered1 = data1.filter({ (text) -> Bool in
            let tmp: NSString = text as NSString
            let range = tmp.range(of: searchText!, options: NSString.CompareOptions.caseInsensitive)
            /*print("range =======",range)
            print("rangeLocation =======",range.location)
            print("Nsfound =======",range.location != NSNotFound)*/
            return range.location != NSNotFound
        })
        
        filtered2 = data2.filter({ (text) -> Bool in
            let tmp: NSString = text as NSString
            let range = tmp.range(of: searchText!, options: NSString.CompareOptions.caseInsensitive)
            /*print("range =======",range)
            print("rangeLocation =======",range.location)
            print("Nsfound =======",range.location != NSNotFound)*/
            return range.location != NSNotFound
        })
        
        filtered3 = data3.filter({ (text) -> Bool in
            let tmp: NSString = text as NSString
            let range = tmp.range(of: searchText!, options: NSString.CompareOptions.caseInsensitive)
            /*print("range =======",range)
            print("rangeLocation =======",range.location)
            print("Nsfound =======",range.location != NSNotFound)*/
            return range.location != NSNotFound
        })
        
        filtered4 = data4.filter({ (text) -> Bool in
            let tmp: NSString = text as NSString
            let range = tmp.range(of: searchText!, options: NSString.CompareOptions.caseInsensitive)
            /*print("range =======",range)
            print("rangeLocation =======",range.location)
            print("Nsfound =======",range.location != NSNotFound)*/
            return range.location != NSNotFound
        })
        
        if(filtered.count == 0 && filtered1.count == 0 && filtered2.count == 0 && filtered3.count == 0 && filtered4.count == 0 ){
            searchActive = false;
        } else {
            searchActive = true;
        }
        if searchActive{
            ui_collectionView.isHidden = false
            self.ds_filtered.removeAll()
            
            let ids = self.getDataSourceidFromFilterd(filtered: filtered, type: PARAMS.SUBTITLE)
            + self.getDataSourceidFromFilterd(filtered: filtered1, type: PARAMS.TITLE)
            + self.getDataSourceidFromFilterd(filtered: filtered2, type: PARAMS.FULLDESCRIPTION)
            + self.getDataSourceidFromFilterd(filtered: filtered3, type: PARAMS.PRICE)
            + self.getDataSourceidFromFilterd(filtered: filtered4, type: PARAMS.MINIDESCRIPTION)
            
            
            for one in ids.removeDuplicates(){
                self.ds_filtered.append(self.getDataFromID(one))
                self.ui_collectionView.reloadData()
            }
            
        }
        else{
            ui_collectionView.isHidden = true
        }
    }
    
    func getDataSourceidFromFilterd(filtered: [String],type: String) -> [String] {
        var dataSourceid = [String]()
        var dataSourceid1 = [String]()
        var dataSourceid2 = [String]()
        var dataSourceid3 = [String]()
        var dataSourceid4 = [String]()
        
        for one in self.ds_workoutVideo{
            for two in filtered{
                if type == PARAMS.SUBTITLE{
                    if one.subtitle == two{
                        dataSourceid.append(one.id)
                    }
                }
                
                else if type == PARAMS.TITLE{
                    if one.title == two{
                        dataSourceid1.append(one.id)
                    }
                }
                
                else if type == PARAMS.FULLDESCRIPTION{
                    if one.fulldescription == two{
                        dataSourceid2.append(one.id)
                    }
                }
                
                else if type == PARAMS.MINIDESCRIPTION{
                    if one.minidescription == two{
                        dataSourceid3.append(one.id)
                    }
                }
                
                else if type == PARAMS.PRICE{
                    if one.price == two{
                        dataSourceid4.append(one.id)
                    }
                }
            }
        }
        
        dataSourceid += dataSourceid1
        dataSourceid += dataSourceid2
        dataSourceid += dataSourceid3
        dataSourceid += dataSourceid4
        
        print(dataSourceid.removeDuplicates())
        return dataSourceid.removeDuplicates()
    }
    
    func getDataFromID(_ id: String) -> WcnModel {
        var returnModel : WcnModel?
        for one in self.ds_workoutVideo{
            if id == one.id{
                returnModel = one
            }
        }
        return returnModel ?? WcnModel()
    }
}

/*extension Array where Element:Equatable {
    func removeDuplicates() -> [Element] {
        var result = [Element]()

        for value in self {
            if result.contains(value) == false {
                result.append(value)
            }
        }
        return result
    }
}*/








