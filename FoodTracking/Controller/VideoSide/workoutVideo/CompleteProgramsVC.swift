//
//  CompleteProgramsVC.swift
//  FoodTracking
//
//  Created by Mac on 4/23/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//


import UIKit
import SwiftyJSON
import SwiftyUserDefaults
import Alamofire

class CompleteProgramsVC : BaseVC1 {
    
    @IBOutlet weak var ui_collectionView: UICollectionView!
    
    var ds_workoutVideo = [WcnModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dialogIndex = 11
        //rootVC = 1
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(false)
        self.getWorkoutVideo()
    }
    
    func getWorkoutVideo() {
        self.showProgress()
        ApiManager.getWorkoutVideo(user_id: user?.id ?? 0){
            (isSuccess,data) in
            self.hideProgress()
            if isSuccess{
                let dict = JSON(data as Any)
                //print(dict)
                var num = 0
                self.ds_workoutVideo.removeAll()
                for one in dict{
                    num += 1
                    let miniJSON = JSON(one.1)
                    
                    if miniJSON[PARAMS.VIDEO].stringValue == ""{
                        self.ds_workoutVideo.append(WcnModel(miniJSON))
                    }
                    
                    if num == dict.count{
                        self.ui_collectionView.reloadData()
                    }
                }
            }
        }
    }
    
    @IBAction func back(_ sender: Any) {
        self.gotoStoryBoardVC("MainVC", fullscreen: true)
    }
    @IBAction func gotoMylab(_ sender: Any) {
        self.gotoVC("MyVideoLibVC")
    }
    
    @IBAction func viewCartBtnClicked(_ sender: Any) {
        self.gotoVC("ViewCartVC")
    }

    @IBAction func buyBtnClicked(_ sender: Any) {
        
        let button: UIButton = sender as! UIButton
        let index = button.tag
        
        if self.ds_workoutVideo[index].status == "yes"{
            
            // programvideo guides
            programvideoID = self.ds_workoutVideo[index].id
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "ProgramVideoGuides")
            controller.modalPresentationStyle = .pageSheet
            self.present(controller, animated: false, completion: nil)
                //self.gotoVC("ProgramVideoGuides")
            
        }
        else{
            workoutVideoId = self.ds_workoutVideo[index].id.toInt() ?? -1
            sub_price = ds_workoutVideo[index].price
            sub_title = ds_workoutVideo[index].subtitle
            //detailDelegate.onDetail(subtitle: subtitle, price: price)
            self.gotoVC("SubscriptionDialog")
        }
    }
    
    @IBAction func detailBtnClicked(_ sender: Any) {
        let button: UIButton = sender as! UIButton
        let index = button.tag
        
        sub_title = self.ds_workoutVideo[index].title
        bottomtitle = self.ds_workoutVideo[index].subtitle
        content = self.ds_workoutVideo[index].minidescription + "\n" + "\n" + self.ds_workoutVideo[index].fulldescription
        imageURL = self.ds_workoutVideo[index].image
    }
    
    @IBAction func gotoSubtab1(_ sender: Any) {
        //self.gotoVC("WorkoutVideoVC")
        self.gotoStoryBoardVC("WorkoutVideoVC", fullscreen: true)
    }
    
    @IBAction func gotoSubtab2(_ sender: Any) {
        return
    }
    @IBAction func gotoSubtab3(_ sender: Any) {
        self.gotoVC("SearchVC")
    }
    
}

extension CompleteProgramsVC : UICollectionViewDelegate, UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return ds_workoutVideo.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionCell", for: indexPath) as! CollectionCell
        cell.entity = ds_workoutVideo[indexPath.row]
        if ds_workoutVideo[indexPath.row].status == "yes"
        {
            if ds_workoutVideo[indexPath.row].video != ""{
                if ds_workoutVideo[indexPath.row].uri == ""{
                    cell.lbl_price.text = "DOWNLOAD NOW!"
                    cell.lbl_price.isHidden = false
                    cell.btn_buy.isHidden = false
                }
                else{
                    cell.lbl_price.isHidden = true
                    cell.btn_buy.isHidden = true
                    
                    /*let timeNow : Double = Double(Int(NSDate().timeIntervalSince1970) * 1000)
                    if timeNow > ds_workoutVideo[indexPath.row].endDay.toDouble()!{
                        self.auto_paymentProcess(ds_workoutVideo[indexPath.row])
                    }*/
                }
            }
            else{
                cell.lbl_price.text = "PRGRAM VIDEOS"
            }
            
            cell.btn_purchased.isHidden = false
            cell.lbl_purchased.isHidden = false
        }
        else{
            cell.btn_purchased.isHidden = true
            cell.lbl_purchased.isHidden = true
            cell.lbl_price.isHidden = false
            cell.btn_buy.isHidden = false
        }
        
        cell.btn_buy.tag = indexPath.row
        cell.btn_detail.tag = indexPath.row
        
        /*cell.buyBlock = {
            print("click Buy button")
            
            self.doBuyAction(indexPath.row)
        }*/
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    }
    
    /*fileprivate func doBuyAction(_ index: Int) {
        /*let vc = storyboard?.instantiateViewController(withIdentifier: "SubscriptionDialog") as! SubscriptionDialog*/
        sub_price = ds_workoutVideo[index].price
        sub_title = ds_workoutVideo[index].subtitle
        //self.gotoVC("SubscriptionDialog")
    }*/
}

extension CompleteProgramsVC : UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let w = collectionView.frame.size.width
        let h = collectionView.frame.size.height / 1.6
          
        return CGSize(width: w, height: h)
    }
}









