

import UIKit
import SwiftyJSON
import SwiftyUserDefaults
import Alamofire
import Foundation

import AVKit
import AVFoundation
import Photos

class MyVideoLibVC : BaseVC1 {
    
    @IBOutlet weak var ui_collectionView: UICollectionView!
    
    @IBOutlet weak var str_myVideoTab: UILabel!
    @IBOutlet weak var str_mylibraryTab: UILabel!
    @IBOutlet weak var str_contacTab: UILabel!
    @IBOutlet weak var str_middleTab: UILabel!
    
    var ds_workoutVideo = [WcnModel]()
    var detailDelegate: onDetailDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //dialogIndex = 0
        rootVC = 2
        str_myVideoTab.text = "MY VIDEO \n STORE"
        str_mylibraryTab.text = "MY VIDEOS \n LIBRARY"
        str_contacTab.text = "CONTACT \n COATCH"
        str_middleTab.text = "WORKOUT ANYWHERE ANY DEVICE \n Online - Offline"
        fragmentType = FragmentType.workout.rawValue
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(false)
        self.getWorkoutVideo()
    }
    
    func getWorkoutVideo() {
        self.showProgress()
        
        ApiManager.getLocalVideo(type: FragmentType.workout.rawValue, completion: { (isSuccess,data) in
            self.hideProgress()
            if isSuccess{
                let dict = JSON(data as Any)
                print("myvideolibrary ===>",dict)
                var num = 0
                self.ds_workoutVideo.removeAll()
                for one in dict{
                    num += 1
                    let miniJSON = JSON(one.1)
                    
                    let con1: Bool = miniJSON["status"].stringValue == "yes" && miniJSON[PARAMS.URI].stringValue != ""// means local video
                    let con2: Bool = miniJSON["status"].stringValue == "yes" && miniJSON[PARAMS.VIDEO].stringValue == ""// means program video
                    
                    if con1 || con2{
                        self.ds_workoutVideo.append(WcnModel(miniJSON))
                    }
                    if num == dict.count{
                        self.ui_collectionView.reloadData()
                    }
                }
            }
        })
    }
    
    @IBAction func back(_ sender: Any) {
        self.gotoStoryBoardVC("MainVC", fullscreen: true)
    }

    @IBAction func buyBtnClicked(_ sender: Any) {
        
        let button: UIButton = sender as! UIButton
        let index = button.tag
        
        sub_price = ds_workoutVideo[index].price
        sub_title = ds_workoutVideo[index].title
        //detailDelegate.onDetail(subtitle: subtitle, price: price)
        self.gotoVC("SubscriptionDialog")
        
    }
    
    @IBAction func gotoVideoStore(_ sender: Any) {
        //self.gotoVC("WorkoutVideoVC")
        self.gotoStoryBoardVC("WorkoutVideoVC", fullscreen: true)
    }
    
    @IBAction func gotoViewCartVC(_ sender: Any) {
        self.gotoVC("ViewCartVC")
    }
    
    @IBAction func goContactcoach(_ sender: Any) { // view cart
        creatNav1()
        self.openMenu1()
    }
    
    @IBAction func playBtnClicked(_ sender: Any) {
        let button: UIButton = sender as! UIButton
        let index = button.tag
        videoUri = self.ds_workoutVideo[index].uri
        self.gotoVC("DownloadedVideoPlayVC")
    }
    
    @IBAction func cancelSubscritionBtnClicked(_ sender: Any) {
        
        let button: UIButton = sender as! UIButton
        let index = button.tag
        cancelsubsriptionModel = self.ds_workoutVideo[index]// for cancel subscription
        subscriptionId = self.ds_workoutVideo[index].id
        self.gotoVCModal(VC_CancelSubscriptionDialog)
    }
    
    @IBAction func deleteBtnClicked(_ sender: Any) {
        let button: UIButton = sender as! UIButton
        let index = button.tag
        let name = self.ds_workoutVideo[index].uri
        self.remove(fileName: name)
        ApiManager.removeWorkoutVideo1(user_id: user!.id, product_id: self.ds_workoutVideo[index].id){
            (issuccess, data) in
            if issuccess{
                self.getWorkoutVideo()
            }
            else{
                
            }
        }
    }
    
    func remove(fileName: String){

        let fileManager = FileManager.default
        
        let baseUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!

        let assetUrl = baseUrl.appendingPathComponent(fileName)

        let url = assetUrl

        do {
            
                try fileManager.removeItem(at: url)
                print("removesuccess")
            

        } catch {
            print("Could not delete file: \(error)")
        }
    }
    
    @IBAction func programBtnClicked(_ sender: Any) {
        let button: UIButton = sender as! UIButton
        let index = button.tag
        programvideoID = self.ds_workoutVideo[index].id
    }
    
    @IBAction func detailBtnClicked(_ sender: Any) {
        let button: UIButton = sender as! UIButton
        let index = button.tag
        
        sub_title = self.ds_workoutVideo[index].title
        bottomtitle = self.ds_workoutVideo[index].subtitle
        content = self.ds_workoutVideo[index].minidescription + "\n" + "\n" + self.ds_workoutVideo[index].fulldescription
        imageURL = self.ds_workoutVideo[index].image
        self.gotoStoryBoardVCModal("DetailView")
    }
}

extension MyVideoLibVC : UICollectionViewDelegate, UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return ds_workoutVideo.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "WorkoutVideoCell", for: indexPath) as! WorkoutVideoCell
        cell.entity = ds_workoutVideo[indexPath.row]
        
        cell.btn_play.tag = indexPath.row
        cell.btn_detail.tag = indexPath.row
        cell.btn_cancelsubscription.tag = indexPath.row
        cell.btn_delete.tag = indexPath.row
        cell.btn_programvides.tag = indexPath.row
        cell.imageBtn.tag = indexPath.row
        
        let con1: Bool = ds_workoutVideo[indexPath.row].status == "yes" && ds_workoutVideo[indexPath.row].uri != ""
        let con2: Bool = ds_workoutVideo[indexPath.row].status == "yes" && ds_workoutVideo[indexPath.row].video == ""
        if con1{
            cell.btn_play.isHidden = false
            cell.btn_cancelsubscription.isHidden = false
            cell.btn_delete.isHidden = false
            cell.btn_programvides.isHidden = true
        }
        
        if con2{
            cell.btn_play.isHidden = true
            cell.btn_cancelsubscription.isHidden = false
            cell.btn_delete.isHidden = true
            cell.btn_programvides.isHidden = false
        }
        
        /*cell.buyBlock = {
            print("click Buy button")
            
            self.doBuyAction(indexPath.row)
        }*/
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    }
    
    /*fileprivate func doBuyAction(_ index: Int) {
        /*let vc = storyboard?.instantiateViewController(withIdentifier: "SubscriptionDialog") as! SubscriptionDialog*/
        sub_price = ds_workoutVideo[index].price
        sub_title = ds_workoutVideo[index].subtitle
        //self.gotoVC("SubscriptionDialog")
    }*/
}

extension MyVideoLibVC : UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let w = collectionView.frame.size.width
        let h = collectionView.frame.size.height / 1.8
          
        return CGSize(width: w, height: h)
    }
}







