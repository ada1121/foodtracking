//
//  MyVideoLib4NutritionVC.swift
//  FoodTracking
//
//  Created by Mac on 6/14/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//



import UIKit
import SwiftyJSON
import SwiftyUserDefaults
import Alamofire
import Foundation

import AVKit
import AVFoundation
import Photos
public let VC_MyVideoLib4NutritionVC = "MyVideoLib4NutritionVC"
class MyVideoLib4NutritionVC : BaseVC1 {
    
    @IBOutlet weak var ui_collectionView: UICollectionView!
    
    @IBOutlet weak var str_myVideoTab: UILabel!
    @IBOutlet weak var str_mylibraryTab: UILabel!
    @IBOutlet weak var str_contacTab: UILabel!
    @IBOutlet weak var str_middleTab: UILabel!
    @IBOutlet weak var lbl_middlebottom: UILabel!
    
    var ds_nutritionist = [WcnModel]()
    var detailDelegate: onDetailDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //dialogIndex = 0
        rootVC = 2
        str_myVideoTab.text = "NUTRITION \n PRODUCTS"
        str_mylibraryTab.text = "MY VIDEOS \n LIBRARY"
        str_contacTab.text = "CONTACT \n COATCH"
        str_middleTab.text = "DIETTTIAN DESIGNED NUTRITION"
        lbl_middlebottom.text = "For weight loss, fitness, \n including food sensitivities"
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(false)
        self.getmynutritionist()
    }
    
    func getmynutritionist() {
        self.showProgress()
        ApiManager.getLocalVideo(type: FragmentType.nutrition.rawValue, completion: { (isSuccess,data) in
            self.hideProgress()
            if isSuccess{
                let dict = JSON(data as Any)
                print("myvideolibrary ===>",dict)
                var num = 0
                self.ds_nutritionist.removeAll()
                for one in dict{
                    num += 1
                    let miniJSON = JSON(one.1)
                    
                    let con1: Bool = miniJSON["status"].stringValue == "yes" && miniJSON[PARAMS.URI].stringValue != ""// means local video
                    //let con2: Bool = miniJSON["status"].stringValue == "yes" && miniJSON[PARAMS.VIDEO].stringValue == ""// means program video
                    
                    if con1{
                        self.ds_nutritionist.append(WcnModel(miniJSON))
                    }
                    if num == dict.count{
                        self.ui_collectionView.reloadData()
                    }
                }
            }
        })
    }
    
    @IBAction func back(_ sender: Any) {
        self.gotoStoryBoardVC("MainVC", fullscreen: true)
    }

    @IBAction func buyBtnClicked(_ sender: Any) {
        
        let button: UIButton = sender as! UIButton
        let index = button.tag
        
        sub_price = ds_nutritionist[index].price
        sub_title = ds_nutritionist[index].title
        //detailDelegate.onDetail(subtitle: subtitle, price: price)
        self.gotoVC("SubscriptionDialog")
        
    }
    
    @IBAction func gotoVideoStore(_ sender: Any) {
        //self.gotoVC("WorkoutVideoVC")
        self.gotoStoryBoardVC("NutrionistVC", fullscreen: true)
    }
    
    @IBAction func gotoViewCartVC(_ sender: Any) {
        //self.gotoVC("ViewCartVC")
        self.gotoVC("NutrioinCheckoutVC")
    }
    
    @IBAction func goContactcoach(_ sender: Any) { // view cart
        creatNav1()
        self.openMenu1()
    }
    
    @IBAction func playBtnClicked(_ sender: Any) {
        let button: UIButton = sender as! UIButton
        let index = button.tag
        videoUri = self.ds_nutritionist[index].uri
        self.gotoVC("DownloadedVideoPlayVC")
    }
    
    @IBAction func cancelSubscritionBtnClicked(_ sender: Any) {
        let button: UIButton = sender as! UIButton
        let index = button.tag
        cancelsubsriptionModel = self.ds_nutritionist[index]// for cancel subscription
        subscriptionId = self.ds_nutritionist[index].id
        self.gotoVCModal(VC_CancelSubscriptionDialog)
    }
    
    @IBAction func deleteBtnClicked(_ sender: Any) {
        let button: UIButton = sender as! UIButton
        let index = button.tag
        let name = self.ds_nutritionist[index].uri
        self.remove(fileName: name)
        ApiManager.removeWorkoutVideo1(user_id: user!.id, product_id: self.ds_nutritionist[index].id){
            (issuccess, data) in
            if issuccess{
                self.getmynutritionist()
            }
            else{
                
            }
        }
    }
    
    func remove(fileName: String){

        let fileManager = FileManager.default
        
        let baseUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!

        let assetUrl = baseUrl.appendingPathComponent(fileName)

        let url = assetUrl

        do {
            
                try fileManager.removeItem(at: url)
                print("removesuccess")
            

        } catch {
            print("Could not delete file: \(error)")
        }
    }
    
    @IBAction func programBtnClicked(_ sender: Any) {
        let button: UIButton = sender as! UIButton
        let index = button.tag
        programvideoID = self.ds_nutritionist[index].id
    }
    
    @IBAction func detailBtnClicked(_ sender: Any) {
        let button: UIButton = sender as! UIButton
        let index = button.tag
        
        sub_title = self.ds_nutritionist[index].title
        bottomtitle = self.ds_nutritionist[index].subtitle
        content = self.ds_nutritionist[index].minidescription + "\n" + "\n" + self.ds_nutritionist[index].fulldescription
        imageURL = self.ds_nutritionist[index].image
        self.gotoStoryBoardVCModal("DetailView")
    }
}

extension MyVideoLib4NutritionVC : UICollectionViewDelegate, UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return ds_nutritionist.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "WorkoutVideoCell", for: indexPath) as! WorkoutVideoCell
        cell.entity = ds_nutritionist[indexPath.row]
        
        cell.btn_play.tag = indexPath.row
        cell.btn_detail.tag = indexPath.row
        cell.btn_cancelsubscription.tag = indexPath.row
        cell.btn_delete.tag = indexPath.row
        cell.btn_programvides.tag = indexPath.row
        cell.imageBtn.tag = indexPath.row
        
        let con1: Bool = ds_nutritionist[indexPath.row].status == "yes" && ds_nutritionist[indexPath.row].uri != ""
        let con2: Bool = ds_nutritionist[indexPath.row].status == "yes" && ds_nutritionist[indexPath.row].video == ""
        if con1{
            cell.btn_play.isHidden = false
            cell.btn_cancelsubscription.isHidden = false
            cell.btn_delete.isHidden = false
            cell.btn_programvides.isHidden = true
        }
        
        if con2{
            cell.btn_play.isHidden = true
            cell.btn_cancelsubscription.isHidden = false
            cell.btn_delete.isHidden = true
            cell.btn_programvides.isHidden = true
        }
        
        
        /*cell.buyBlock = {
            print("click Buy button")
            
            self.doBuyAction(indexPath.row)
        }*/
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    }
    
    /*fileprivate func doBuyAction(_ index: Int) {
        /*let vc = storyboard?.instantiateViewController(withIdentifier: "SubscriptionDialog") as! SubscriptionDialog*/
        sub_price = ds_nutritionist[index].price
        sub_title = ds_nutritionist[index].subtitle
        //self.gotoVC("SubscriptionDialog")
    }*/
}

extension MyVideoLib4NutritionVC : UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let w = collectionView.frame.size.width
        let h = collectionView.frame.size.height / 1.6
          
        return CGSize(width: w, height: h)
    }
}







