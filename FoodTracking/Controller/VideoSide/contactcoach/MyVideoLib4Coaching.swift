//
//  MyVideoLib4Coaching.swift
//  FoodTracking
//
//  Created by Mac on 6/15/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import UIKit

public let VC_MyVideoLib4Coaching = "MyVideoLib4Coaching"

import UIKit
import SwiftyJSON
import SwiftyUserDefaults
import Alamofire
import Foundation

import AVKit
import AVFoundation
import Photos

class MyVideoLib4Coaching : BaseVC1 {
    
    @IBOutlet weak var ui_collectionView: UICollectionView!
    @IBOutlet weak var str_myVideoTab: UILabel!
    @IBOutlet weak var str_mylibraryTab: UILabel!
    @IBOutlet weak var str_contacTab: UILabel!
    @IBOutlet weak var str_middleTab: UILabel!
    @IBOutlet weak var lbl_middlebottom: UILabel!
    
    var ds_coaching = [WcnModel]()
    var detailDelegate: onDetailDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //dialogIndex = 0
        rootVC = 2
        str_myVideoTab.text = "COACHING \n SERVICES"
        str_mylibraryTab.text = "MY VIDEOS \n LIBRARY"
        str_contacTab.text = "CONTACT \n COATCH"
        str_middleTab.text = "ONE-ON-ONE HEALTH COACHING"
        lbl_middlebottom.text = "Get one-on-one personalized health coaching \n Let us help you maxiamize your results."
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(false)
        self.getmyCoaching()
    }
    
    func getmyCoaching() {
        self.showProgress()
        ApiManager.getLocalVideo(type: FragmentType.coaching.rawValue, completion: { (isSuccess,data) in
            self.hideProgress()
            if isSuccess{
                let dict = JSON(data as Any)
                print("myvideolibrary ===>",dict)
                var num = 0
                self.ds_coaching.removeAll()
                for one in dict{
                    num += 1
                    let miniJSON = JSON(one.1)
                    let con1: Bool = miniJSON["status"].stringValue == "yes" && miniJSON[PARAMS.URI].stringValue != ""// means local video
                
                    if con1{
                        self.ds_coaching.append(WcnModel(miniJSON))
                    }
                    if num == dict.count{
                        self.ui_collectionView.reloadData()
                    }
                }
            }
        })
    }
    
    @IBAction func back(_ sender: Any) {
        self.gotoStoryBoardVC("MainVC", fullscreen: true)
    }

    @IBAction func buyBtnClicked(_ sender: Any) {
        let button: UIButton = sender as! UIButton
        let index = button.tag
        sub_price = ds_coaching[index].price
        sub_title = ds_coaching[index].title
        self.gotoVC("SubscriptionDialog")
    }
    
    @IBAction func gotoVideoStore(_ sender: Any) {
        //self.gotoVC("WorkoutVideoVC")
        self.gotoStoryBoardVC(VC_contact, fullscreen: true)
    }
    
    @IBAction func gotoViewCartVC(_ sender: Any) {
        //self.gotoVC("ViewCartVC")
        self.gotoVC(VC_ContactCheckVC)
    }
    
    @IBAction func goContactcoach(_ sender: Any) { // view cart
        creatNav1()
        self.openMenu1()
    }
    
    @IBAction func playBtnClicked(_ sender: Any) {
        let button: UIButton = sender as! UIButton
        let index = button.tag
        videoUri = self.ds_coaching[index].uri
        self.gotoVC("DownloadedVideoPlayVC")
    }
    
    @IBAction func cancelSubscritionBtnClicked(_ sender: Any) {
        let button: UIButton = sender as! UIButton
        let index = button.tag
        cancelsubsriptionModel = self.ds_coaching[index]// for cancel subscription
        subscriptionId = self.ds_coaching[index].id
        self.gotoVCModal(VC_CancelSubscriptionDialog)
    }
    
    @IBAction func deleteBtnClicked(_ sender: Any) {
        let button: UIButton = sender as! UIButton
        let index = button.tag
        let name = self.ds_coaching[index].uri
        self.remove(fileName: name)
        ApiManager.removeWorkoutVideo1(user_id: user!.id, product_id: self.ds_coaching[index].id){
            (issuccess, data) in
            if issuccess{
                self.getmyCoaching()
            }
            else{
            }
        }
    }
    
    func remove(fileName: String){
        let fileManager = FileManager.default
        let baseUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let assetUrl = baseUrl.appendingPathComponent(fileName)
        let url = assetUrl
        do {
            try fileManager.removeItem(at: url)
            print("removesuccess")
        } catch {
            print("Could not delete file: \(error)")
        }
    }
    
    @IBAction func programBtnClicked(_ sender: Any) {
        let button: UIButton = sender as! UIButton
        let index = button.tag
        programvideoID = self.ds_coaching[index].id
    }
    
    @IBAction func detailBtnClicked(_ sender: Any) {
        let button: UIButton = sender as! UIButton
        let index = button.tag
        sub_title = self.ds_coaching[index].title
        bottomtitle = self.ds_coaching[index].subtitle
        content = self.ds_coaching[index].minidescription + "\n" + "\n" + self.ds_coaching[index].fulldescription
        imageURL = self.ds_coaching[index].image
        self.gotoStoryBoardVCModal("DetailView")
    }
}

extension MyVideoLib4Coaching : UICollectionViewDelegate, UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return ds_coaching.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "WorkoutVideoCell", for: indexPath) as! WorkoutVideoCell
        cell.entity = ds_coaching[indexPath.row]
        
        cell.btn_play.tag = indexPath.row
        cell.btn_detail.tag = indexPath.row
        cell.btn_cancelsubscription.tag = indexPath.row
        cell.btn_delete.tag = indexPath.row
        cell.btn_programvides.tag = indexPath.row
        cell.imageBtn.tag = indexPath.row
        
        let con1: Bool = ds_coaching[indexPath.row].status == "yes" && ds_coaching[indexPath.row].uri != ""
        let con2: Bool = ds_coaching[indexPath.row].status == "yes" && ds_coaching[indexPath.row].video == ""
        if con1{
            cell.btn_play.isHidden = false
            cell.btn_cancelsubscription.isHidden = false
            cell.btn_delete.isHidden = false
            cell.btn_programvides.isHidden = true
        }
        
        if con2{
            cell.btn_play.isHidden = true
            cell.btn_cancelsubscription.isHidden = false
            cell.btn_delete.isHidden = true
            cell.btn_programvides.isHidden = true
        }
        
        
        /*cell.buyBlock = {
            print("click Buy button")
            
            self.doBuyAction(indexPath.row)
        }*/
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    }
    
    /*fileprivate func doBuyAction(_ index: Int) {
        /*let vc = storyboard?.instantiateViewController(withIdentifier: "SubscriptionDialog") as! SubscriptionDialog*/
        sub_price = ds_coaching[index].price
        sub_title = ds_coaching[index].subtitle
        //self.gotoVC("SubscriptionDialog")
    }*/
}

extension MyVideoLib4Coaching : UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let w = collectionView.frame.size.width
        let h = collectionView.frame.size.height / 1.6
          
        return CGSize(width: w, height: h)
    }
}
