//
//  SmsDialog.swift
//  FoodTracking
//
//  Created by Mac on 5/9/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import UIKit
import MessageUI
class SmsDialog: BaseVC1,MFMessageComposeViewControllerDelegate
{
    @IBOutlet weak var lbl_content: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lbl_content.text = "You are about to send a text. Please \n make sure you included your: \n First Name \n Last Name \n City \n State \n \n And \n How can we help you? \n \n If you don't include this informaion \n we will  need ask for this  information \n before we reply. \n \n Thanks for your understanding."

    }
    @IBAction func okBtnClicked(_ sender: Any) {
        if calltag == 0{
            self.sendNewIMessage("774-240-5287")
        }
        else if calltag == 1{
            self.sendNewIMessage("774-540-8357")
        }
        else{
            
        }
    }
    @IBAction func closeBtnClicked(_ sender: Any) {
        self.dismiss(animated:true)
    }
    func sendNewIMessage(_ phonenumber : String) {
           let messageVC = MFMessageComposeViewController()
           messageVC.body = ""
           messageVC.recipients = [phonenumber]
           messageVC.messageComposeDelegate = self
           self.present(messageVC, animated: true, completion: nil)
    }
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        switch (result) {
        case .cancelled:
            print("Message was cancelled")
            dismiss(animated: true, completion: nil)
        case .failed:
            print("Message failed")
            dismiss(animated: true, completion: nil)
        case .sent:
            print("Message was sent")
            dismiss(animated: true, completion: nil)
        default:
            break
        }
    }
}
