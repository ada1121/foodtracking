


import UIKit
import SwiftyJSON
import SwiftyUserDefaults
import Alamofire

public let VC_ContactCheckVC = "ContactCheckVC"

class ContactCheckVC : BaseVC1 {
    
    @IBOutlet weak var ui_collectionView: UICollectionView!
    
    var ds_workoutVideo = [WcnModel]()
    @IBOutlet weak var collectionViewHeight: NSLayoutConstraint!
    @IBOutlet weak var uiv_bottomCheck: UIView!
    @IBOutlet weak var lbl_totalSubTotal: UILabel!
    
    @IBOutlet weak var str_myVideoTab: UILabel!
    @IBOutlet weak var str_mylibraryTab: UILabel!
    @IBOutlet weak var str_contacTab: UILabel!
    @IBOutlet weak var str_middleTab: UILabel!
    @IBOutlet weak var str_middlebottom: UILabel!
    @IBOutlet weak var cons_middleViewHeight: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //dialogIndex = 0
        
        ds_checkoutModel.removeAll()
        str_myVideoTab.text = "COACHING \n SERVICES"
        str_mylibraryTab.text = "MY VIDEOS \n LIBRARY"
        str_contacTab.text = "CONTACT \n COATCH"
        str_middleTab.text = "ONE-ON-ONE HEALTH COACHING"
        str_middlebottom.text = "Get one-on-one personalized health coaching \n Let us help you maxiamize your results."
        
        fragmentType = FragmentType.coaching.rawValue
        self.cons_middleViewHeight.constant = UIScreen.main.bounds.height * 0.14
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(false)
        if let fragmentType = fragmentType{
            self.getViewCart(fragmentType)
        }
    }
    
    func getViewCart(_ fragmentType: String) {
        self.showProgress()
        ApiManager.getCheckout(product_type: fragmentType, completion: { (isSuccess,data) in
            self.hideProgress()
            if isSuccess{
                let dict = JSON(data as Any)
                print("viewcart==>",dict)
                var num = 0
                var subTotal:Float = 0
                self.ds_workoutVideo.removeAll()
                
                if dict.count == 0{
                    self.uiv_bottomCheck.isHidden = true
                }
                
                for one in dict{
                    num += 1
                    let miniJSON = JSON(one.1)
                    self.ds_workoutVideo.append(WcnModel(miniJSON))
                    subTotal += miniJSON[PARAMS.PRICE].stringValue.toFloat()!
                    if self.ds_workoutVideo.count == 0{
                        self.uiv_bottomCheck.isHidden = true
                    }
                    if num == dict.count{
                        self.uiv_bottomCheck.isHidden = false
                        self.ui_collectionView.reloadData()
                        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
                        layout.minimumInteritemSpacing = 0
                        layout.minimumLineSpacing = 0
                        let height =  self.ui_collectionView.collectionViewLayout.collectionViewContentSize.height
                        self.collectionViewHeight.constant = height
                        let formattedsubTotal = String(format: "%.2f", subTotal)
                        self.lbl_totalSubTotal.text = "SUBTOTAL" + " " + " $" + "\(formattedsubTotal)"
                        self.view.layoutIfNeeded()
                        //self.loadtime += 1
                    }
                }
            }
        })
    }
    
    @IBAction func back(_ sender: Any) {
        self.gotoStoryBoardVC("MainVC", fullscreen: true)
    }
    
    @IBAction func gotoMylab(_ sender: Any) {
        //self.gotoVC("MyVideoLib4NutritionVC")
        self.gotoStoryBoardVC(VC_MyVideoLib4Coaching, fullscreen: true)
    }
    
    @IBAction func goContactcoach(_ sender: Any) { // view cart
        creatNav1()
        self.openMenu1()
    }
    
    @IBAction func gotoVideoHome(_ sender: Any) {
        //self.gotoVC("WorkoutVideoVC")
        self.gotoStoryBoardVC(VC_contact, fullscreen: true)
    }

    @IBAction func cancelBtnClicked(_ sender: Any) {
        let button: UIButton = sender as! UIButton
        let index = button.tag
        ApiManager.removeCheckout(user_id: user!.id, product_id: ds_workoutVideo[index].id.toInt() ?? -1){
            (isSuccess, data) in
            if isSuccess{
                self.ds_workoutVideo.remove(at: index) // subcategory remove
                self.ui_collectionView.reloadData()
                let height =  self.ui_collectionView.collectionViewLayout.collectionViewContentSize.height
                self.collectionViewHeight.constant = height
                if self.ds_workoutVideo.count != 0{
                    var subTotal:Float = 0
                    var num = 0
                    for one in self.ds_workoutVideo{
                        num += 1
                        subTotal += one.price.toFloat()!
                        if num == self.ds_workoutVideo.count{
                            let formattedsubTotal = String(format: "%.2f", subTotal)
                            self.lbl_totalSubTotal.text = "SUBTOTAL" + " " + " $" + "\(formattedsubTotal)"
                        }
                    }
                }
                else{
                   self.uiv_bottomCheck.isHidden = true
                }
                
                self.view.layoutIfNeeded()
            }
                
            else{
                
            }
        }
    }
    
    @IBAction func checkoutBtnclicked(_ sender: Any) {
       
        if self.ds_workoutVideo.count != 0{
            var subTotal:Float = 0
            var num = 0
            for one in self.ds_workoutVideo{
                num += 1
                subTotal += one.price.toFloat()!
                if num == self.ds_workoutVideo.count{
                    let formattedsubTotal = String(format: "%.2f", subTotal)
                    checkoutamount = formattedsubTotal
                }
            }
        }
        ds_checkoutModel = self.ds_workoutVideo
        self.gotoVC("CheckoutVC")
    }
}

    extension ContactCheckVC : UICollectionViewDelegate, UICollectionViewDataSource{
        
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return ds_workoutVideo.count
        }
        
        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CheckoutCell", for: indexPath) as! CheckoutCell
            cell.entity = ds_workoutVideo[indexPath.row]
            cell.btn_cancel.tag = indexPath.row
            /*cell.buyBlock = {
                print("click Buy button")
                
                self.doBuyAction(indexPath.row)
            }*/
            return cell
        }
        
        func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        }
        
        /*fileprivate func doBuyAction(_ index: Int) {
            /*let vc = storyboard?.instantiateViewController(withIdentifier: "SubscriptionDialog") as! SubscriptionDialog*/
            sub_price = ds_workoutVideo[index].price
            sub_title = ds_workoutVideo[index].subtitle
            //self.gotoVC("SubscriptionDialog")
        }*/
    }

    extension ContactCheckVC : UICollectionViewDelegateFlowLayout{
        
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            
            let w = collectionView.frame.size.width
            let h = w / 2
              
            return CGSize(width: w, height: h)
        }
}






