//
//  ShowContactViewController.swift
//  FoodTracking
//
//  Created by Mac on 2/11/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import UIKit

class ShowContactViewController: BaseVC1 {

    @IBOutlet weak var content: UILabel!
    
    @IBOutlet weak var showView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        creatNav1()

        // Do any additional setup after loading the view.
        content.text = "Please send us a text to get \n approval. Click OK then click \n the text button below."
    }
    @IBAction func okayBtnClicked(_ sender: Any) {
        self.openMenu1()
        self.showView.removeFromSuperview()
        //self.view.isHidden = true
    }
    @IBAction func spaceClicked(_ sender: Any) {
        self.gotoStoryBoardVC("LoginVC", fullscreen: true)
    }
}
