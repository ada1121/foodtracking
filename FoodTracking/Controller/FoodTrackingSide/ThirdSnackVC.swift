




//
//  ThirdSnackVC.swift
//  FoodTracking
//
//  Created by Ubuntu on 12/28/19.
//  Copyright © 2019 Ubuntu. All rights reserved.
//

import UIKit
import ExpyTableView
import SwiftyMenu
import ExpandableCell
import SwiftyUserDefaults
import SwiftyJSON
import SwiftValidator
import RealmSwift
import Realm
import EasyTipView

class ThirdSnackVC: BaseVC1 , ValidationDelegate, UITextFieldDelegate,UINavigationControllerDelegate,UIGestureRecognizerDelegate, UIScrollViewDelegate {
    
    //  variable initializer
    @IBOutlet weak var dlg_remove: UIView!
    @IBOutlet weak var dlg_title: UILabel!
    @IBOutlet weak var dlg_content: UILabel!
    var collectionRemoveIndex = 0
    var tableViewSectionIndex = 0
    var tableViewRowIndex = 0
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var heightContraint: NSLayoutConstraint! // collectionView Height Constraint
    @IBOutlet weak var addFoodListHeight: NSLayoutConstraint! // addFoodList view Heigth control
    @IBOutlet weak var elementShowView: NSLayoutConstraint!
    @IBOutlet weak var recommededView: NSLayoutConstraint!
    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var SelectedCatView: ExpyTableView! // expandable tableView
    @IBOutlet weak var saveBtnView: UIView!
    @IBOutlet weak var saveLabel: UILabel!
    @IBOutlet weak var dropDown: SwiftyMenu! // dropdownView
    @IBOutlet weak var tableViewheight: NSLayoutConstraint! // expandable tableview total height
    @IBOutlet weak var totalCalVal: UILabel!
    @IBOutlet weak var valueTableGroup: UIView!
    @IBOutlet weak var recommendedTxt: UILabel!
    @IBOutlet weak var total_cal_Label: UILabel!
    @IBOutlet weak var totalCarbs: UILabel!
    @IBOutlet weak var totalFat: UILabel!
    @IBOutlet weak var totalProtein: UILabel!
    @IBOutlet weak var totalFiber: UILabel!
    @IBOutlet weak var addCustomFood: UITextField!
    @IBOutlet weak var addCustomCalPerCup: UITextField!
    @IBOutlet weak var addCustomCal: UITextField!
    @IBOutlet weak var addCustomCarbsPerCup: UITextField!
    @IBOutlet weak var addCustomFatPerCup: UITextField!
    @IBOutlet weak var addCustomProteinPerCup: UITextField!
    @IBOutlet weak var addCustomFiberPerCup: UITextField!
    @IBOutlet weak var totalBreakFastCalories: UILabel!
    @IBOutlet weak var remainingCalroies: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    
    var mealCategorieModelList = List<CategoriesModel>()
    var cellHeight = 60 // expandable table view cell height
    let validator1 = Validator()

    let count_num = 0
    // cal_List View Datasource
    var cal_listDatasource = [Cal_listModel]()
    // expandalbe tableView Id array
    var expandableHeader_ID : String?
    var tempid :String = ""
    // selectedFoodlist datasource
    var selectedFoodListDatasource = [selectedFoodModel]()
    var totalCaloriesVal : Int = 0
    var mealHistoy : FoodHistoryModel?
    var tipView = EasyTipView(text: "")
    var tipviewState = true
    var dropDonwHeaderIndex = 0
    var addFoodBtnclicked = false
    
    //MARK:- Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        foodctrType = FoodctrType.thirdsnack.rawValue
        // for datasaving
        self.dlg_remove.isHidden = true
        
        categoriesModelList.removeAll()
        selectedFoodListDatasource.removeAll() //
        tempModelList.removeAll()
        total_cal_Label.text = "0"
        self.scrollView.delegate = self
        // get data from Relam
        // TODO:ChangePart1
        mealHistoy = DBHelper.shared.getMealHistory(Constants.THIRDSNACK)
        //TODO:get total history and expandable tableview datasource
        self.getSaveDataSource4DropDown()
        // dropdown datasource init
        initDropDownDataSource()
         // first value define
        totalCalVal.text = UserDefault.getString(key: Constants.OLD_VALUE,defaultValue: "")
        
        //
        remainingCalroies.text = "\(UserDefault.getInt(key: Constants.REMAINING_CALORIE, defaultValue: 0))"
        totalBreakFastCalories.text = "\(UserDefault.getInt(key: Constants.TOTAL_THIRD_SNACK_CALORIE, defaultValue: 0))"
        // TODO: changepart2
        if sampleArrayList.count != 0 {
            sampleArray = sampleArrayList[5]
        }
        
        sampleMealName = 6
        creatNav()
        //TODO: changepart3
        valueTableGroup.isHidden = true
        createGradientLabel(titleView, letter: "Create & Track Third Snack", fontsize: 35, position: 1)
        createGradientView(saveBtnView)
        saveLabel.text = "SAVE"
        saveLabel.textColor = .white
        saveBtnView.addSubview(saveLabel)
        total_cal_Label.text = ""
        totalCarbs.text = ""
        totalFat.text = ""
        totalProtein.text = ""
        totalFiber.text = ""
        
        // Longpress touch
        let longPressRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(longPressed(sender:)))
        self.SelectedCatView.addGestureRecognizer(longPressRecognizer)
       
        NotificationCenter.default.addObserver(self, selector: #selector(self.orientationDidChange), name: UIDevice.orientationDidChangeNotification, object: nil)
        
        // DropDown Setup component
        dropDown.delegate = self
        dropDown.options = dropDownOptionsDataSource
        //* Support CallBacks
        dropDown.didExpand = {
            self.tipView.dismiss()
            //print("SwiftyMenu Expanded!")
        }
        dropDown.didCollapse = {
           // print("SwiftyMeny Collapsed")
        }
        dropDown.didSelectOption = { (selection: Selection) in
            //print("DropDownParam ==\(selection.value) at index: \(selection.index)")
            self.dropDonwHeaderIndex = selection.index
            self.expandableHeader_ID = self.initExpandableHeaderForId(selection.index)
            self.tempid = self.expandableHeader_ID ?? ""
            //print("this is the Expandable header Id",self.expandableHeader_ID as Any)
            self.recommendedTxt.text = self.initRecommendedShow(selection.index)
            // auto sizing collectionView
            self.cal_listDatasource = self.initCalList(selection.index)
            self.showSelectedCell(index: selection.index)
            self.collectionView.reloadData()
            let height =  self.collectionView.collectionViewLayout.collectionViewContentSize.height
            self.heightContraint.constant = height
            self.view.layoutIfNeeded()
            self.valueTableGroup.isHidden = false
        }
        //* Custom Behavior
        dropDown.scrollingEnabled = false
        dropDown.isMultiSelect = false
        //* Custom UI
        dropDown.rowHeight = 80
        dropDown.listHeight = 650
        //dropDown.borderWidth = 1.0
        //* Custom Colors
        dropDown.borderColor = .black
        dropDown.optionColor = UIColor.init(named: "ColorDropDownOption")!
        dropDown.placeHolderColor = .white
        dropDown.menuHeaderBackgroundColor = UIColor.init(named: "ColorDropHeader")!
        dropDown.rowBackgroundColor = UIColor.init(named: "ColordropDownListBack")!
        dropDown.placeHolderText = "Add Category"
        
        // Custom Animation
        dropDown.expandingAnimationStyle = .spring(level: .normal)
        dropDown.expandingDuration = 0.5
        dropDown.collapsingAnimationStyle = .linear
        dropDown.collapsingDuration = 0.5
        dropDown.hideOptionsWhenSelect = true
        
        // collectionView autoHeight control
        collectionView.delegate = self
        collectionView.dataSource = self
        let height =  collectionView.collectionViewLayout.collectionViewContentSize.height
        heightContraint.constant = height
        self.view.layoutIfNeeded()
        
        // table Veiw Height set
        tableViewheight.constant = SelectedCatView.contentSize.height
        // ********  add FoodList Height determine *******//
        addFoodListHeight.constant = UIScreen.main.bounds.width / 4.5
        elementShowView.constant = addFoodListHeight.constant
        recommededView.constant =  150
        
        let lpgr = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress(gestureRecognizer:)))
        lpgr.minimumPressDuration = 0.5
        lpgr.delaysTouchesBegan = true
        self.collectionView?.addGestureRecognizer(lpgr)
        
        // ================= Easy Tip View Setting ==================//
        var preferences = EasyTipView.Preferences()
        preferences.drawing.font = UIFont(name: "Futura-Medium", size: 30)!
        preferences.drawing.foregroundColor = UIColor.white
        preferences.drawing.backgroundColor = UIColor.init(named: "ColorTip")!
        preferences.drawing.arrowPosition = EasyTipView.ArrowPosition.bottom

        /*
         * Optionally you can make these preferences global for all future EasyTipViews
         */
        EasyTipView.globalPreferences = preferences
        
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        // selected catview reload data
        SelectedCatView.rowHeight = 80 // table view row height defined
        SelectedCatView.expandingAnimation = .left
        SelectedCatView.collapsingAnimation = .right
        SelectedCatView.tableFooterView = UIView()
        // selectedCattable View set
        SelectedCatView.dataSource = self
        SelectedCatView.delegate = self
        SelectedCatView.reloadData()
        tableViewheight.constant = SelectedCatView.contentSize.height
        self.view.layoutIfNeeded()
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.tipView.dismiss()
    }
    // TODO: Collection LongPress
    @objc func handleLongPress(gestureRecognizer : UILongPressGestureRecognizer){
        
        if  gestureRecognizer.state == UIGestureRecognizer.State.began{
            let p = gestureRecognizer.location(in: self.collectionView)
            if let indexPath = collectionView.indexPathForItem(at: p) {
                let width = UIScreen.main.bounds.width
                //print("\(width)")
                if p.x >= 0 && p.x <= width / 2 {
                    print("first item")
                    if self.cal_listDatasource[indexPath.row].user_id == "0"{
                        return
                    }
                    else{
                        removeType = RemoveType.collectionView.rawValue
                        self.dlg_title.text = "Remove Custom Food"
                        self.dlg_content.text = "Do you want to remove this custom food?"
                        self.dlg_remove.isHidden = false
                        collectionRemoveIndex = indexPath.row
                    }
                   
                } else if p.x >= width * 5 / 6  {
                    print("last item")
                    longPressIndex = indexPath.row
                    self.cal_listDatasource[indexPath.row].longpress = 1
                    self.collectionView.reloadData()
                }
            }
        }
    }
    
    @IBAction func removeCancelBtnClicked(_ sender: Any) {
        self.dlg_remove.isHidden = true
    }
    // TODO:3
   @IBAction func removeOkBtnClicked(_ sender: Any) {
        self.dlg_remove.isHidden = true
        if let removetype = removeType{
            if removetype == RemoveType.collectionView.rawValue{
                self.showAlert_Remove_collection(row: collectionRemoveIndex)
            }else if removetype == RemoveType.tableView.rawValue{
                self.showAlert_Remove(tableViewSectionIndex, row: tableViewRowIndex)
            }else if removetype == RemoveType.customfoodadd.rawValue{
                self.gotoServerUpload()
            }else{
                self.dlg_remove.isHidden = true
            }
        }
        removeType = nil
    }
    
    func showSelectedCell(index : Int){
        if selectedFoodListDatasource.count != 0{
            for one in selectedFoodListDatasource{
                for two in one.miniFood{
                    for three in self.cal_listDatasource{
                        if two.id == three.id{
                            if two.value == three.cal{
                                three.selected_State = 2
                            }
                            else if two.value == "\(three.cal.toInt()! / 2)"{
                                three.selected_State = 1
                            }
                            else {
                                three.selected_State = 3
                                three.other = two.value
                            }
                        }
                    }
                }
            }
            
        }
    }
    
    func showAlert_Remove_collection(row: Int) {
         self.addFoodBtnclicked = true
         ApiManager.removeMyFood(mycalorie_id: self.cal_listDatasource[row].id.toInt() ?? 0){
             (isSuccess, data) in
             if isSuccess{
                 self.cal_listDatasource.remove(at: row) // subcategory remove
                 self.collectionView.reloadData()
                 let height =  self.collectionView.collectionViewLayout.collectionViewContentSize.height
                 self.heightContraint.constant = height
                 self.view.layoutIfNeeded()
                 self.progShowInfo(true, msg: "Remove Success!")
                 
             }
             else{
                 self.progShowInfo(true, msg: "Remove Fail!")
             }
         }
    }
    
   @objc func longPressed(sender: UILongPressGestureRecognizer) {
        if sender.state == UIGestureRecognizer.State.began {
            let touchPoint = sender.location(in: self.SelectedCatView)
            if let indexPath = SelectedCatView.indexPathForRow(at: touchPoint) {
                self.dlg_title.text = "Food Remove"
                self.dlg_content.text = "Are you going to sure remove this food?"
                self.dlg_remove.isHidden = false
                removeType = RemoveType.tableView.rawValue
                self.tableViewSectionIndex = indexPath.section
                self.tableViewRowIndex = indexPath.row
            }
        }
    }

    func getSaveDataSource4DropDown()  {
        if mealHistoy != nil{
            var total = 0
            for one in mealHistoy!.categories {
                var minifood = 0
                var subfood = [miniFoodModel]()
                for two in one.subcategories{
                    subfood.append(miniFoodModel(id:two.id,foodName: two.title, value: two.cal, carbs: two.carbs, fat: two.fat, protein: two.protein, fiber: two.fiber))
                    minifood += 1
                    if minifood == one.subcategories.count{
                        selectedFoodListDatasource.append(selectedFoodModel(id:one.id, header_title: one.food_type, miniFood: subfood))
                        subfood.removeAll()
                    }
                }
                total += 1
                if total == mealHistoy!.categories.count{
                   self.getTempData()
                }
            }
        }
        else{
            initDropDownDataSource()
        }
    }
    func getTempData(){
       var total = 0
       for one in mealHistoy!.categories {
           var subfood = [miniFoodModel]()
           var subcategory = 0
           for two in one.subcategories{
               subfood.append(miniFoodModel(id:two.id,foodName: two.title, value: two.cal, carbs: two.carbs, fat: two.fat, protein: two.protein, fiber: two.fiber))
               subcategory += 1
            if subcategory == one.subcategories.count{
                tempModelList.append(tempFoodModel(id: one.id, meal_type: one.meal_type, food_type: one.food_type, miniFood: subfood))
                subfood.removeAll()
                total += 1
                if total == mealHistoy?.categories.count{
                   initDropDownDataSource()
                }
            }
           }
       }
    }
   
     func showAlert_Remove(_ section:Int,row: Int) {
           if row == 0{ // category totally remove
               self.selectedFoodListDatasource.remove(at: section)
               self.makeBreakFastHistory()
               self.SelectedCatView.reloadData()
               self.tableViewheight.constant = self.SelectedCatView.contentSize.height
               self.calculateSum()
               self.view.layoutIfNeeded()
               
           }
           else{
                self.selectedFoodListDatasource[section].miniFood.remove(at: row - 1) // subcategory remove
               if self.selectedFoodListDatasource[section].miniFood.count == 0{
                   self.selectedFoodListDatasource.remove(at: section)
               }
               self.makeBreakFastHistory()
               self.SelectedCatView.reloadData()
               self.tableViewheight.constant = self.SelectedCatView.contentSize.height
               self.calculateSum()
               self.view.layoutIfNeeded()
                   
               }
       }
    
    @objc func orientationDidChange() {
        switch UIDevice.current.orientation {
        case .portrait, .portraitUpsideDown, .landscapeLeft, .landscapeRight: SelectedCatView.reloadSections(IndexSet(Array(SelectedCatView.expandedSections.keys)), with: .none)
        default:break
        }
    }
    @IBAction func sampleBtnClicked(_ sender: Any) {
        openMenu()
    }
    @IBAction func gotoHome(_ sender: Any) {
        self.gotoStoryBoardVC("MainVC", fullscreen: true)
    }
    func getTitleFromId(_ foodListid : String) -> String {
        var foodtitle = ""
        for one in collectionViewDataSource{
            if one.id.elementsEqual(foodListid) {
               foodtitle =  one.title
               continue
            }
            else{
                print("")
            }
        }
        return foodtitle
    }
    func calculateSum() {
        var miniBreakFastCaloriesVal:Int = 0
        var sumTotalCalories:Int = 0
        var miniTotalRemainingCalories:Int = 0
        for one in selectedFoodListDatasource{
            for two in one.miniFood{
                miniBreakFastCaloriesVal = miniBreakFastCaloriesVal + two.value.toInt()!
            }
        }
        // TODO:changepart2
        totalBreakFastCalories.text = "\(miniBreakFastCaloriesVal)"// do sum action
        UserDefault.setInt(key: Constants.TOTAL_THIRD_SNACK_CALORIE, value: miniBreakFastCaloriesVal)
        getTotalValArray()
        for one in mealCaloriesArray {
            sumTotalCalories = sumTotalCalories + one
        }
        miniTotalRemainingCalories = (UserDefault.getString(key: Constants.OLD_VALUE)?.toInt() ?? 0) - sumTotalCalories
        remainingCalroies.text = "\(miniTotalRemainingCalories)"
        UserDefault.setInt(key: Constants.REMAINING_CALORIE, value: miniTotalRemainingCalories)
    }
    //  SaveBtnClicked
    @IBAction func saveBtnClicked(_ sender: Any) {
        addFoodBtnclicked = false
        saveBtnAction()
        makeBreakFastHistory()
        scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        //self.calGrade()
    }
    func saveBtnAction() {
        var miniFoodListDatasource = [miniFoodModel]()
        miniFoodListDatasource.removeAll()
        for one in cal_listDatasource{
            if one.selected_State == 0{
                print("")
            }
            else if one.selected_State == 1{// halfvalue selected
                miniFoodListDatasource.append(miniFoodModel(id:one.id,foodName: one.title, value: "\(one.cal.toInt()! / 2)",carbs:"\(one.carbs.toInt()! / 2)",fat:"\(one.fat.toInt()! / 2)",protein:"\(one.protein.toInt()! / 2)",fiber:"\(one.fiber.toInt()! / 2)"))
            }
            else if one.selected_State == 2{
                miniFoodListDatasource.append(miniFoodModel(id:one.id,foodName: one.title, value: "\(one.cal.toInt()!)",carbs:"\(one.carbs.toInt()!)",fat:"\(one.fat.toInt()!)",protein:"\(one.protein.toInt()!)",fiber:"\(one.fiber.toInt()!)"))
            }
            else if one.selected_State == 3{
                if one.other.isEmpty{
                    //print("novalue")
                }
                else{ miniFoodListDatasource.append(miniFoodModel(id:one.id,foodName: one.title, value: "\(one.other.toInt()!)",carbs:"\(one.carbs.toInt()! * one.other.toInt()! / one.cal.toInt()!)",fat:"\(one.fat.toInt()! * one.other.toInt()! / one.cal.toInt()!)",protein:"\(one.protein.toInt()! * one.other.toInt()! / one.cal.toInt()!)",fiber:"\(one.fiber.toInt()! * one.other.toInt()! / one.cal.toInt()!)"))
                }
            }
            else{
                //print("error")
            }
        }
        
        if selectedFoodListDatasource.count == 0{// first time added action
            if expandableHeader_ID == nil || miniFoodListDatasource.count == 0{
                return
            }
            else{ self.selectedFoodListDatasource.append(selectedFoodModel(id: expandableHeader_ID!, header_title: self.getTitleFromId(expandableHeader_ID!) , miniFood: miniFoodListDatasource))
                expandableHeader_ID = nil
                miniFoodListDatasource.removeAll()
                SelectedCatView.reloadData()
                tableViewheight.constant = SelectedCatView.contentSize.height
                self.view.layoutIfNeeded()
                self.calculateSum()
            }
        }
        else{ // next time action
            for i in 0 ... selectedFoodListDatasource.count - 1{
                if selectedFoodListDatasource[i].id == expandableHeader_ID{
                    if expandableHeader_ID == nil || miniFoodListDatasource.count == 0{
                        return
                    }
                    else{
                        self.selectedFoodListDatasource.remove(at: i)
                        print("remove")
                        break
                    }
                }
                else {
                    print("normal add")
                }
            }
            
            if expandableHeader_ID == nil && miniFoodListDatasource.count != 0{
                self.selectedFoodListDatasource.last?.miniFood = miniFoodListDatasource
                expandableHeader_ID = nil
                miniFoodListDatasource.removeAll()
                SelectedCatView.reloadData()
                tableViewheight.constant = SelectedCatView.contentSize.height
                self.view.layoutIfNeeded()
                self.calculateSum()
            }
            
            if  miniFoodListDatasource.count == 0{
                return
            }
            else{ self.selectedFoodListDatasource.append(selectedFoodModel(id: expandableHeader_ID!, header_title: self.getTitleFromId(expandableHeader_ID!) , miniFood: miniFoodListDatasource))
                expandableHeader_ID = nil
                //print("\(selectedFoodListDatasource.last?.miniFood.count)")
                miniFoodListDatasource.removeAll()
                SelectedCatView.reloadData()
                tableViewheight.constant = SelectedCatView.contentSize.height
                self.view.layoutIfNeeded()
                self.calculateSum()
            }
        }
    }
    func isString10Digits(_ ten_digits: String) -> Bool{
        if !ten_digits.isEmpty {
            let numberCharacters = NSCharacterSet.decimalDigits.inverted
            return !ten_digits.isEmpty && ten_digits.rangeOfCharacter(from: numberCharacters) == nil
        }
        return false
    }
    @IBAction func addFoodBtnClicked(_ sender: Any) {
        addFoodBtnclicked = true
            //upload foodlist
        validator1.registerField(addCustomFood, errorLabel: nil , rules: [RequiredRule()])
        validator1.registerField(addCustomCalPerCup, errorLabel: nil , rules: [RequiredRule(),FloatRule()])
        validator1.styleTransformers(success:{ (validationRule) -> Void in
            // clear error label
            validationRule.errorLabel?.isHidden = true
            validationRule.errorLabel?.text = ""
        }, error:{ (validationError) -> Void in
            print("error")
            
            removeType = RemoveType.foodalert.rawValue
            self.dlg_title.text = ""
            self.dlg_content.text = "Please input food name and calroie values per 1 cup!"
            self.dlg_remove.isHidden = false
            //self.alertDisplay(alertController: self.alertMake("Please input food name and calroie values per 1 cup!"))
        })
        validator1.validate(self)
    }
    
    func validationSuccessful() {
        
        let customCal = self.addCustomCal.text
        let customCarbsPerCup = self.addCustomCarbsPerCup.text
        let customFatPerCup = self.addCustomFatPerCup.text
        let customProteinPerCup = self.addCustomProteinPerCup.text
        let customFiberPerCup = self.addCustomFiberPerCup.text
        if (customCal! != "" && !isString10Digits(customCal ?? "")) || (customCarbsPerCup! != "" && !isString10Digits(customCarbsPerCup ?? ""))  || (customFatPerCup! != "" && !isString10Digits(customFatPerCup ?? ""))  || (customProteinPerCup! != "" && !isString10Digits(customProteinPerCup ?? ""))  || (customFiberPerCup! != "" && !isString10Digits(customFiberPerCup ?? "")) {
            
            self.alertDisplay(alertController: self.alertMake("Input Format Error!"))
        }
        else{
            print("ok")
            self.uploadFood2Local()
        }
    }
    
    func uploadFood2Local() {
        
        let customCarbsPerCup = self.addCustomCarbsPerCup.text
        let customFatPerCup = self.addCustomFatPerCup.text
        let customProteinPerCup = self.addCustomProteinPerCup.text
        let customFiberPerCup = self.addCustomFiberPerCup.text
        
        if customCarbsPerCup!.isEmpty || customFatPerCup!.isEmpty || customProteinPerCup!.isEmpty || customFiberPerCup!.isEmpty{
            self.dlg_remove.isHidden = false
            removeType = RemoveType.customfoodadd.rawValue
            self.dlg_title.text = ""
            self.dlg_content.text = "You didn’t put in all value including carbs, fats, protein and fiber. If you proceed your total daily calories will be correct but your total daily carbs, fats, protein and fiber will be incorrect."
            
        }
        else {
            self.gotoServerUpload()
        }
    }
    
    func localAddAction(_ id : Int) {
        let customFoodName = self.addCustomFood.text
        let customCalPerCup = self.addCustomCalPerCup.text
        let customCal = self.addCustomCal.text
        let customCarbsPerCup = self.addCustomCarbsPerCup.text
        let customFatPerCup = self.addCustomFatPerCup.text
        let customProteinPerCup = self.addCustomProteinPerCup.text
        let customFiberPerCup = self.addCustomFiberPerCup.text
        var scale : Int!
        if customCal?.isEmpty ?? true{
            scale = 1
        }
        else{
            scale = customCal!.toInt()! / customCalPerCup!.toInt()!
        }
        
        let customCarbsPerCupVal : Int = (customCarbsPerCup!.toInt() ?? 0) * scale
        let customFatPerCupVal : Int = (customFatPerCup!.toInt() ?? 0) * scale
        let customProteinPerCupVal : Int = (customProteinPerCup!.toInt() ?? 0) * scale
        let customFiberPerCupVal : Int = (customFiberPerCup!.toInt() ?? 0) * scale
        self.cal_listDatasource.append( Cal_listModel( id : "\(id)", title : customFoodName!,cal : customCalPerCup!,carbs : "\(customCarbsPerCupVal)",fat  : "\(customFatPerCupVal)",protein : "\(customProteinPerCupVal)",fiber : "\(customFiberPerCupVal)",other : "",selected_Stated : 0, longPress: 0))
        
    }
    
    func gotoServerUpload() {
        
        print("serverupload start")
        let customFoodName = addCustomFood.text
        let customCalPerCup = addCustomCalPerCup.text
        let customCal = addCustomCal.text
        
        let customCarbsPerCup = addCustomCarbsPerCup.text
        let customFatPerCup = addCustomFatPerCup.text
        let customProteinPerCup = addCustomProteinPerCup.text
        let customFiberPerCup = addCustomFiberPerCup.text
        
        var scale : Int!
        if customCal?.isEmpty ?? true{
            scale = 1
        }
        else{
            scale = customCal!.toInt()! / customCalPerCup!.toInt()!
        }
        let user_id = user?.id!
        self.uploadCustomFoodList(user_id: user_id!, category_id: self.tempid , title: customFoodName!, cal: customCalPerCup ?? "", carbs: "\(customCarbsPerCup!.toInt() ?? 0 * scale)", fat: "\(customFatPerCup!.toInt() ?? 0 * scale)", protein: "\(customProteinPerCup!.toInt() ?? 0 * scale)", fiber: "\(customFiberPerCup!.toInt() ?? 0 * scale)")
    }
    
    func uploadCustomFoodList( user_id: Int, category_id: String, title: String, cal: String, carbs: String, fat: String, protein: String, fiber: String){
        ApiManager.uploadCustomFoodList(user_id: user_id, category_id: category_id, title: title, cal: cal, carbs: carbs, fat: fat, protein: protein, fiber: fiber) { (isSuccess, data) in
            
            if isSuccess{
                print("foodserverUploadEnd")
                print("data")
                let mycalorie_id = data as! Int
                self.localAddAction(mycalorie_id)
                self.initializeEdt()
            }
            else{
                self.alertDisplay(alertController: self.alertMake("CustomFood Add Fail"))
            }
        }
    }
    
    func initializeEdt(){
        print("initstart")
        self.addCustomFood.text = ""
        self.addCustomCalPerCup.text = ""
        self.addCustomCal.text = ""
        self.addCustomCarbsPerCup.text = ""
        self.addCustomFatPerCup.text = ""
        self.addCustomProteinPerCup.text = ""
        self.addCustomFiberPerCup.text = ""
        self.collectionView.reloadData()
        let height =  self.collectionView.collectionViewLayout.collectionViewContentSize.height
        self.heightContraint.constant = height
        self.view.layoutIfNeeded()
        self.collectionView.reloadData()
    }
    
    func validationFailed(_ errors: [(Validatable, ValidationError)]) {
        print("validation error")
    }
    func updateState( _ view : UIView)  {
        if view.tag % 3 == 0 { // halfvalue state
            let indexPathRow = view.tag / 3
            //print("oneValueindexPathRow=====","\(indexPathRow)")
            if cal_listDatasource[indexPathRow].selected_State == 0
                 {
                   
                    cal_listDatasource[indexPathRow].selected_State = 1
                    self.collectionView.reloadData()
                  }
            else if cal_listDatasource[indexPathRow].selected_State == 1
            {
               cal_listDatasource[indexPathRow].selected_State = 0
                self.collectionView.reloadData()
             }
            else if cal_listDatasource[indexPathRow].selected_State == 2
            {
               cal_listDatasource[indexPathRow].selected_State = 1
                self.collectionView.reloadData()
             }
            else if cal_listDatasource[indexPathRow].selected_State == 3
            {
               cal_listDatasource[indexPathRow].selected_State = 1
                self.collectionView.reloadData()
             }
             else{
             }
        }
        else if view.tag % 3 == 1{// oneavalue treatment
            let indexPathRow = (view.tag - 1) / 3
            //print("halfvalueindexPathRow=====","\(indexPathRow)")
            if cal_listDatasource[indexPathRow].selected_State == 0{
            cal_listDatasource[indexPathRow].selected_State = 2
            self.collectionView.reloadData()
            }
            else if cal_listDatasource[indexPathRow].selected_State == 1{
            cal_listDatasource[indexPathRow].selected_State = 2
                self.collectionView.reloadData()
            }
            else if cal_listDatasource[indexPathRow].selected_State == 2{
            cal_listDatasource[indexPathRow].selected_State = 0
                self.collectionView.reloadData()
            }
            else if cal_listDatasource[indexPathRow].selected_State == 3{
            cal_listDatasource[indexPathRow].selected_State = 2
                self.collectionView.reloadData()
            }
            else{
               //print("noItem halfValue")
            }
        }
        else if view.tag % 3 == 2{ // textfield
            let indexPathRow = (view.tag - 2) / 3
            //print("halfvalueindexPathRow=====","\(indexPathRow)")
            if cal_listDatasource[indexPathRow].selected_State == 0{
            cal_listDatasource[indexPathRow].selected_State = 3
            self.collectionView.reloadData()
            }
            else if cal_listDatasource[indexPathRow].selected_State == 1{
            cal_listDatasource[indexPathRow].selected_State = 3
                self.collectionView.reloadData()
            }
            else if cal_listDatasource[indexPathRow].selected_State == 2{
            cal_listDatasource[indexPathRow].selected_State = 3
                self.collectionView.reloadData()
            }
            else if cal_listDatasource[indexPathRow].selected_State == 3{
            cal_listDatasource[indexPathRow].selected_State = 0
                self.collectionView.reloadData()
            }
            else{
               //print("noItem halfValue")
            }
        }
        else{
            //print("no Item")
        }
    }
    
    func showtipView(_ sender : UIView) {
        
        if tipviewState {
           tipView.dismiss()
           self.tipviewState = false
        }
        if sender.tag % 3 == 0 { // halfvalue state
            //print("tag ====","\(String(describing: sender.tag))")
            
            let indexPathRow = sender.tag / 3
            //print("oneValueindexPathRow=====","\(indexPathRow)")
            let carbs = Float(cal_listDatasource[indexPathRow].carbs.toFloat()! / 2)
            let fat = Float(cal_listDatasource[indexPathRow].fat.toFloat()! / 2)
            let protein = Float(cal_listDatasource[indexPathRow].protein.toFloat()! / 2)
            let fiber = Float(cal_listDatasource[indexPathRow].fiber.toFloat()! / 2)
            
            let formattedcarbs = String(format: "%.1f", carbs)
            let formattedfat = String(format: "%.1f", fat)
            let formattedprotein = String(format: "%.1f", protein)
            let formattedfiber = String(format: "%.1f", fiber)
            
            tipView = EasyTipView(text: "Carbs=\(formattedcarbs),Fat=\(formattedfat),\n Protein=\(formattedprotein),Fiber=\(formattedfiber)")
            tipView.show(forView: sender)
            
            self.tipviewState = true
       }
                
       else if sender.tag % 3 == 1{// oneavalue treatment
           let indexPathRow = (sender.tag - 1) / 3
           
                           //print("oneValueindexPathRow=====","\(indexPathRow)")
           let carbs = Float(cal_listDatasource[indexPathRow].carbs.toFloat()!)
           let fat = Float(cal_listDatasource[indexPathRow].fat.toFloat()!)
           let protein = Float(cal_listDatasource[indexPathRow].protein.toFloat()!)
           let fiber = Float(cal_listDatasource[indexPathRow].fiber.toFloat()!)
            
            
           
           let formattedcarbs = String(format: "%.1f", carbs)
           let formattedfat = String(format: "%.1f", fat)
           let formattedprotein = String(format: "%.1f", protein)
           let formattedfiber = String(format: "%.1f", fiber)
           
           tipView = EasyTipView(text: "Carbs=\(formattedcarbs),Fat=\(formattedfat),\n Protein=\(formattedprotein),Fiber=\(formattedfiber)")
            
           tipView.show(forView: sender)
           
           self.tipviewState = true
       }
       else if sender.tag % 3 == 2{ // textfield
          
         let indexPathRow = (sender.tag - 2) / 3
         var value : Float?           //print("oneValueindexPathRow=====","\(indexPathRow)")
         if cal_listDatasource[indexPathRow].other == ""{
            value = 0
         }
         else{
            value = Float(cal_listDatasource[indexPathRow].other.toFloat()! / cal_listDatasource[indexPathRow].cal.toFloat()! )
         }
          
         let carbs = Float(cal_listDatasource[indexPathRow].carbs.toFloat()! * value!)
         let fat = Float(cal_listDatasource[indexPathRow].fat.toFloat()! * value!)
         let protein = Float(cal_listDatasource[indexPathRow].protein.toFloat()! * value!)
         let fiber = Float(cal_listDatasource[indexPathRow].fiber.toFloat()! * value!)
            
         let formattedcarbs = String(format: "%.1f", carbs)
         let formattedfat = String(format: "%.1f", fat)
         let formattedprotein = String(format: "%.1f", protein)
         let formattedfiber = String(format: "%.1f", fiber)
         
         tipView = EasyTipView(text: "Carbs=\(formattedcarbs),Fat=\(formattedfat),\n Protein=\(formattedprotein),Fiber=\(formattedfiber)")
         tipView.show(forView: sender)
         
         self.tipviewState = true
           
       }
       else{
           //print("no Item")
       }
    }
    
    @objc func cellTap(_ sender:UITapGestureRecognizer) {
        showtipView((sender.view)!)
        updateState((sender.view)!)
        //print(sender.self.view?.largeContentTitle)
        total_cal_Label.text = "\(calculateTotalValue())"
        
        let formattedcarbs = String(format: "%.1f", calculate4Values()[0])
        let formattedfat = String(format: "%.1f", calculate4Values()[1])
        let formattedprotein = String(format: "%.1f", calculate4Values()[2])
        let formattedfiber = String(format: "%.1f", calculate4Values()[3])
        
        totalCarbs.text = "\(formattedcarbs)"
        totalFat.text = "\(formattedfat)"
        totalProtein.text = "\(formattedprotein)"
        totalFiber.text = "\(formattedfiber)"
    }
    
    func calculateTotalValue() -> Int {
        var totalcal = 0
        for one in cal_listDatasource{
            if one.selected_State == 0{
                //print("no selection")
            }
            else if one.selected_State == 1{
                totalcal = totalcal + one.cal.toInt()!/2
            }
            else if one.selected_State == 2{
                totalcal = totalcal + one.cal.toInt()!
            }
            else if one.selected_State == 3{
                if one.other.isEmpty{
                    //print("novalue")
                }
                else{
                    totalcal = totalcal + one.other.toInt()!
                }
            }
            else{
                //print("error")
            }
        }
        return totalcal
    }
    func calculate4Values() -> [Float] {
        var totalCarbs : Float = 0
        var totalFat: Float = 0
        var totalProtein: Float = 0
        var totalFiber: Float = 0
        var groupTotal : [Float] = [0,0,0,0]
           for one in cal_listDatasource{
               if one.selected_State == 0{
                   //print("no selection")
               }
               else if one.selected_State == 1{
                    totalCarbs += Float(one.carbs.toFloat()!/2)
                    totalFat += one.fat.toFloat()!/2
                    totalProtein = totalProtein + one.protein.toFloat()!/2
                    totalFiber = totalFiber + one.fiber.toFloat()!/2
               }
               else if one.selected_State == 2{
                   totalCarbs = totalCarbs + one.carbs.toFloat()!
                   totalFat = totalFat + one.fat.toFloat()!
                   totalProtein = totalProtein + one.protein.toFloat()!
                   totalFiber = totalFiber + one.fiber.toFloat()!
               }
               else if one.selected_State == 3{
                   if one.other.isEmpty{
                       //print("novalue")
                   }
                   else{
                    let scale : Float = one.other.toFloat()! / one.cal.toFloat()!
                       totalCarbs = totalCarbs + one.carbs.toFloat()! * scale
                       totalFat = totalFat + one.fat.toFloat()! * scale
                       totalProtein = totalProtein + one.protein.toFloat()! * scale
                       totalFiber = totalFiber + one.fiber.toFloat()! * scale
                   }
               }
               else{
                   //print("error")
               }
            groupTotal[0] = totalCarbs
            groupTotal[1] = totalFat
            groupTotal[2] = totalProtein
            groupTotal[3] = totalFiber
           }
           return groupTotal
       }
    
    func makeBreakFastHistory(){
        self.getonlyOneMealData()
    }
    
    func getonlyOneMealData()  {
        mealCategorieModelList.removeAll()
        var selectedFoodList = 0
        let breakfastsubCategoriesModelList = List<SubcategoriesModel>()
        
        if selectedFoodListDatasource.count == 0{
            if DBHelper.shared.getMealHistory(Constants.THIRDSNACK).id != ""{
                DBHelper.shared.removewithIndex(Constants.THIRDSNACK)
            }
            else{
                return
            }
        }
        else{
            for one in selectedFoodListDatasource{
                var minifood = 0
                for two in one.miniFood{
                    breakfastsubCategoriesModelList.append( SubcategoriesModel(id:two.id,title:two.foodName,cal:two.value,carbs:two.carbs,fat:two.fat,protein:two.protein,fiber:two.fiber))
                    minifood += 1//TODO: changepart4
                    if minifood == one.miniFood.count{ mealCategorieModelList.append(CategoriesModel(id:one.id,meal_type:PARAMS.THIRDSNACK,food_type:one.header_title,subcategories:breakfastsubCategoriesModelList))
                        selectedFoodList += 1
                        breakfastsubCategoriesModelList.removeAll()

                        if selectedFoodList  == selectedFoodListDatasource.count{
                            print("\(categoriesModelList.count)")
                            print("1=========================")
                            self.appendDatasource()
                        }
                    }
                }
            }
        }
    }

    func appendDatasource()  {// changepart5
        var mealist = 0
        for one in mealCategorieModelList{
            categoriesModelList.append(one)
            mealist += 1
            if mealist == mealCategorieModelList.count{
               mealCategorieModelList.removeAll()
                print("\(categoriesModelList.count)")
               print("2================================")
               self.realmSaveAction()
            }
        }
    }
    // TODO: Changepart6
    func realmSaveAction(){
        let willSaveddata = self.makeMealHistory(Constants.THIRDSNACK)// will saved data will be changed everytime when saved btn clicked
        DBHelper.shared.updateHistoy(Constants.THIRDSNACK, updateModel: willSaveddata)
        categoriesModelList.removeAll()
    }
}


//MARK: ExpyTableViewDataSourceMethods
extension ThirdSnackVC: ExpyTableViewDataSource {
    func tableView(_ tableView: ExpyTableView, canExpandSection section: Int) -> Bool {
        return true
    }
    func tableView(_ tableView: ExpyTableView, expandableCellForSection section: Int) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: SelCatCell.self)) as! SelCatCell
        cell.labelCatName.text = selectedFoodListDatasource[section].header_title
        cell.layoutMargins = UIEdgeInsets.zero
        cell.showSeparator()
        return cell
    }
}

//MARK: ExpyTableView delegate methods
extension ThirdSnackVC: ExpyTableViewDelegate {
    func tableView(_ tableView: ExpyTableView, expyState state: ExpyState, changeForSection section: Int) {
    
        switch state {
        case .willExpand:
            //print("WILL EXPAND")
            return
            
        case .willCollapse:
            //print("WILL COLLAPSE")
            return
            
        case .didExpand:
            //print("DID EXPAND")
            let detailnum = selectedFoodListDatasource[section].miniFood.count
            tableViewheight.constant += CGFloat(detailnum * cellHeight )
            tableViewheight.constant = SelectedCatView.contentSize.height
            //print("expandHeight ==========\(tableViewheight.constant)")
            
        case .didCollapse:
            //print("DID COLLAPSE")
            let detailnum = selectedFoodListDatasource[section].miniFood.count
            tableViewheight.constant -= CGFloat(detailnum * cellHeight)
            tableViewheight.constant = SelectedCatView.contentSize.height
            //print("collapseHeight ==========\(tableViewheight.constant)")
        }
    }
}

// Selected category autodimention
extension ThirdSnackVC {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        print("DID SELECT row: \(indexPath.row), section: \(indexPath.section)")
    }
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return UITableView.automaticDimension
//    }
}

//MARK: UITableView Data Source Methods
extension ThirdSnackVC {
    func numberOfSections(in tableView: UITableView) -> Int {
        print("\(selectedFoodListDatasource.count)")
        return selectedFoodListDatasource.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       // print("Row count for section \(section) is \(selectedFoodListDatasource[section].miniFood.count)")
        return selectedFoodListDatasource[section].miniFood.count + 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: DetailValTableViewCell.self)) as! DetailValTableViewCell
        //  expandable TableView changePart
        cell.labelselectedName.text = selectedFoodListDatasource[indexPath.section].miniFood[(indexPath.row - 1)].foodName
        cell.labelselectedvalue.text = selectedFoodListDatasource[indexPath.section].miniFood[(indexPath.row - 1)].value
            cell.layoutMargins = UIEdgeInsets.zero
             //cell.showSeparator()
            cell.hideSeparator()
            cellHeight = Int(cell.bounds.size.height)
            return cell
      
    }
}

// Drop Down view delegate
extension ThirdSnackVC: SwiftyMenuDelegate {
    
    func didSelectOption(_ swiftyMenu: SwiftyMenu, _ selectedOption: SwiftyMenuDisplayable, _ index: Int) {
        print("Selected option: \(selectedOption), at index: \(index)")
    }
    func swiftyMenuWillAppear(_ swiftyMenu: SwiftyMenu) {
        //print("SwiftyMenu will appear.")
    }
    func swiftyMenuDidAppear(_ swiftyMenu: SwiftyMenu) {
        //print("SwiftyMenu did appear.")
    }
    func swiftyMenuWillDisappear(_ swiftyMenu: SwiftyMenu) {
        //print("SwiftyMenu will disappear.")
    }
    func swiftyMenuDidDisappear(_ swiftyMenu: SwiftyMenu) {
        //print("SwiftyMenu did disappear.")
    }
}


extension ThirdSnackVC: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
      
        
        if !addFoodBtnclicked {
            if indexPath.row < collectionViewDataSource[dropDonwHeaderIndex].calorie_list.count{
                if collectionViewDataSource[dropDonwHeaderIndex].calorie_list[indexPath.row].title.count > 95{
                        
                    let width = collectionView.frame.size.width
                    let height = width/5
                    return CGSize(width: width, height: height)
                }
                
                else  if collectionViewDataSource[dropDonwHeaderIndex].calorie_list[indexPath.row].title.count > 90{
                    let width = collectionView.frame.size.width
                    let height = width/5.2
                    return CGSize(width: width, height: height)
                }
                else  if collectionViewDataSource[dropDonwHeaderIndex].calorie_list[indexPath.row].title.count > 80{
                    let width = collectionView.frame.size.width
                    let height = width/5.5
                    return CGSize(width: width, height: height)
                }
                else  if collectionViewDataSource[dropDonwHeaderIndex].calorie_list[indexPath.row].title.count > 70{
                    let width = collectionView.frame.size.width
                    let height = width/6.0
                    return CGSize(width: width, height: height)
                }
                else
                {
                    let width = collectionView.frame.size.width
                    let height = width/6.5
                    return CGSize(width: width, height: height)
                }
            }
            else{
                let width = collectionView.frame.size.width
                let height = width/6.0
                return CGSize(width: width, height: height)
            }
        }
        
        else if addFoodBtnclicked {
            let width = collectionView.frame.size.width
            let height = width/6.0
            
            return CGSize(width: width, height: height)
        }
        else {
            let width = collectionView.frame.size.width
            let height = width/6.0
            return CGSize(width: width, height: height)
        }
    }
}

extension ThirdSnackVC {
    
    @objc func enterPressed(_ sender: Any) {
        
        let textField = sender as! UITextField
        if cal_listDatasource[longPressIndex!].id != ""{
            cal_listDatasource[longPressIndex!].other = textField.text ?? "0"
             textField.text = ""
             cal_listDatasource[longPressIndex!].longpress = 2
             cal_listDatasource[longPressIndex!].selected_State = 3
             collectionView.reloadData()
            total_cal_Label.text = "\(calculateTotalValue())"
             
            let formattedcarbs = String(format: "%.1f", calculate4Values()[0])
            let formattedfat = String(format: "%.1f", calculate4Values()[1])
            let formattedprotein = String(format: "%.1f", calculate4Values()[2])
            let formattedfiber = String(format: "%.1f", calculate4Values()[3])
            
            totalCarbs.text = "\(formattedcarbs)"
            totalFat.text = "\(formattedfat)"
            totalProtein.text = "\(formattedprotein)"
            totalFiber.text = "\(formattedfiber)"
             
             self.showtipView(textField.superview!)
        }
    }
}

// UIcollectionView datasource and delegate
extension ThirdSnackVC: UICollectionViewDelegate,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return cal_listDatasource.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ValueCollectionViewCell", for: indexPath) as! ValueCollectionViewCell
        
        cell.entity = self.cal_listDatasource[indexPath.row]
        cell.halfVal.superview?.tag = 3 * indexPath.row //0
        cell.oneVal.superview?.tag = 3 * indexPath.row + 1//1
        cell.otherLabel.superview?.tag = 3 * indexPath.row + 2//2
        
        cell.oneVal.superview?.cellTapGesture(tapNumber: 1, target: self, action: #selector(cellTap))
        cell.halfVal.superview?.cellTapGesture(tapNumber: 1, target: self, action: #selector(cellTap))
        cell.otherLabel.superview?.cellTapGesture(tapNumber: 1, target: self, action: #selector(cellTap))
        
        if cell.entity.selected_State == 0{
            cell.halfVal.superview?.backgroundColor = UIColor.init(named: "ColorFoodListBack")!
            cell.oneVal.superview?.backgroundColor = UIColor.init(named: "ColorFoodListBack")!
            cell.otherLabel.superview?.backgroundColor = UIColor.init(named: "ColorFoodListBack")!
        }
        
        if cell.entity.selected_State == 1{
            cell.halfVal.superview?.backgroundColor = UIColor.init(named: "Colorselected")!
            cell.oneVal.superview?.backgroundColor = UIColor.init(named: "ColorFoodListBack")!
            cell.otherLabel.superview?.backgroundColor = UIColor.init(named: "ColorFoodListBack")!
        }
        
        if cell.entity.selected_State == 2{
            cell.oneVal.superview?.backgroundColor = UIColor.init(named: "Colorselected")!
            cell.halfVal.superview?.backgroundColor = UIColor.init(named: "ColorFoodListBack")!
            cell.otherLabel.superview?.backgroundColor = UIColor.init(named: "ColorFoodListBack")!
        }
            
        if cell.entity.selected_State == 3 {
            cell.otherLabel.superview?.backgroundColor = UIColor.init(named: "Colorselected")!
            cell.oneVal.superview?.backgroundColor = UIColor.init(named: "ColorFoodListBack")!
            cell.halfVal.superview?.backgroundColor = UIColor.init(named: "ColorFoodListBack")!
        }
            
        if cell.entity.longpress == 1{
            cell.otherLabel.isHidden = true
            cell.valTxf.isHidden = false
            cell.valTxf.backgroundColor = UIColor.init(named: "Colorselected")!
            cell.valTxf.becomeFirstResponder()
            cell.valTxf.resignFirstResponder()
            ///cell.valTxf.setNeedsDisplay()
            ///cell.valTxf.setNeedsFocusUpdate()
        }
        if cell.entity.longpress == 2{
           cell.otherLabel.isHidden = false
           cell.valTxf.isHidden = true
        }
        else {
            //print("no selction")
        }
        
        //cell.valTxf.tag = indexPath.row
        cell.valTxf.addTarget(self, action: #selector(enterPressed), for: .editingDidEnd)
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        print("scroll ended")
    }
}

