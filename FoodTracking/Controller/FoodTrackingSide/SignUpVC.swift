//
//  SignUpVC.swift
//  FoodTracking
//
//  Created by Ubuntu on 12/30/19.
//  Copyright © 2019 Ubuntu. All rights reserved.
//

import Foundation
import UIKit
import SwiftValidator
import IQKeyboardManagerSwift
import SwiftyJSON
import SwiftyUserDefaults
import GDCheckbox
import KRProgressHUD
import KRActivityIndicatorView

class SignUpVC: BaseVC1, ValidationDelegate, UITextFieldDelegate,UINavigationControllerDelegate, UIImagePickerControllerDelegate {

    @IBOutlet weak var signBtnView: UIView!
    @IBOutlet weak var signCaption: UILabel!
    
    @IBOutlet weak var edtName: UITextField!
    @IBOutlet weak var edtEmail: UITextField!
    @IBOutlet weak var edtPwd: UITextField!
    @IBOutlet weak var confirmPwd: UITextField!
    @IBOutlet weak var checkBox: GDCheckbox!
    
    let validator = Validator()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        signCaption.text = "SIGN UP HERE"
        editInit()
        //loadLayout()
    }
    
    func loadLayout() {
        edtPwd.text = "123456"
        confirmPwd.text = "123456"
        edtName.text = "ios_tester"
        edtEmail.text = "contactconfigurations@gmail.com"
    }
    
    func editInit() {
        setEdtPlaceholderColor(edtEmail, placeholderText: "Email", placeColor: UIColor.gray)
        setEdtPlaceholderColor(edtPwd, placeholderText: "Password", placeColor: UIColor.gray)
        setEdtPlaceholderColor(edtName, placeholderText: "User Name", placeColor: UIColor.gray)
        setEdtPlaceholderColor(confirmPwd, placeholderText: "Confirm Password", placeColor: UIColor.gray)
    }
    
    override func viewWillAppear(_ animated: Bool) {
            createGradientView(signBtnView)
    }
    
    @IBAction func gotoLogin(_ sender: Any) {
        //gotoVC("LoginVC")
        self.gotoStoryBoardVC("LoginVC", fullscreen: true)
    }
    @IBAction func signUpBtnClicked(_ sender: Any) {
       validator.registerField(edtName, errorLabel: nil , rules: [RequiredRule()])
       validator.registerField(edtEmail, errorLabel: nil , rules: [RequiredRule(),EmailRule()])
       validator.registerField(edtPwd, errorLabel: nil , rules: [RequiredRule(),AlphaNumericRule()])
       validator.registerField(confirmPwd, errorLabel: nil , rules: [RequiredRule(),ConfirmationRule(confirmField: edtPwd)])
       validator.styleTransformers(success:{ (validationRule) -> Void in
           
           // clear error label
           validationRule.errorLabel?.isHidden = true
           validationRule.errorLabel?.text = ""
           
       }, error:{ (validationError) -> Void in
           print("error")
           self.progressSet( styleVal: 2, backColor: UIColor.init(named: "ColorBlur")!, textColor: .white, imgcolor: .red, headerColor: .red, trailColor: .yellow)
           self.progShowError(true, msg: "Please Input Correctly!")
           
       })
       validator.validate(self)
    }
    
    func validationSuccessful() {
        
        if checkBox.isOn == false{
            self.progressSet( styleVal: 2, backColor: UIColor.init(named: "ColorDarkBlur")!, textColor: .white, imgcolor: .blue, headerColor: .red, trailColor: .yellow)
            self.progShowInfo(true, msg: "Are you agree terms and conditions?")
        }
        
        else {
              signupApi(usertname : edtName.text!, email :edtEmail.text!, password : edtPwd.text!)
            
        }
       
    }
    
    func signupApi( usertname : String, email : String, password : String ){
        showProgressSet_withMessage("Registering ... \n Just a moment!", msgOn: true, styleVal: 2, backColor: UIColor.init(named: "ColorBlur")!, textColor: .white, imgColor: .clear, headerColor: .red, trailColor: .yellow)
        
        ApiManager.signup(username : usertname, email: email, password : password  ) { (isSuccess, data) in
            self.hideProgress()
            
            if isSuccess{
                UserDefault.setString(key: PARAMS.EMAIL, value: email)
                self.signupsuccess()
            }
            else{
                if data == nil{
                    
                    self.alertDisplay(alertController: self.alertMake("Connection Error!"))
                    print("connection Error")
                    
                }
                else{
                    let result_message = data as! String
                    if(result_message == "1"){
                        self.alertDisplay(alertController: self.alertMake("Email already Exist!"))
                        print("email already Exist")
                    }
                    else{
                        print("Network Error!")
                        self.alertDisplay(alertController: self.alertMake("Network Error!"))
                       
                    }
                }
                
            }
            
        }
        
    }
    func signupsuccess(){
        self.clearLocalDatabase(true)
        self.gotoStoryBoardVC("LoginVC", fullscreen: true)
    }
    func validationFailed(_ errors: [(Validatable, ValidationError)]) {
        print("validation error")
    }
}
