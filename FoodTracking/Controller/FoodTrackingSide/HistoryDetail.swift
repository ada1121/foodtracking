//
//  HistoryDetail.swift
//  FoodTracking
//
//  Created by Ubuntu on 1/15/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import UIKit
import ExpyTableView
import SwiftyMenu
import ExpandableCell
import SwiftyUserDefaults
import SwiftyJSON
import SwiftValidator

class HistoryDetail: BaseVC1 {
    
    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var SelectedCatView: ExpyTableView!
    @IBOutlet weak var dropDown: SwiftyMenu! // dropdownView
    @IBOutlet weak var tableViewheight: NSLayoutConstraint!
    var cellHeight = 100//expandable cell height
    @IBOutlet weak var totalCal: UILabel!
    @IBOutlet weak var remaincal: UILabel!
    @IBOutlet weak var score: UILabel!
    @IBOutlet weak var grade: UILabel!
    @IBOutlet weak var breakfast: UILabel!
    @IBOutlet weak var firstsnack: UILabel!
    @IBOutlet weak var lunch: UILabel!
    @IBOutlet weak var secondsnack: UILabel!
    @IBOutlet weak var dinner: UILabel!
    @IBOutlet weak var thirdsnack: UILabel!
    
    var historydropDownOptionsDataSource = ["Breakfast","First Snack","Lunch","Second Snack","Dinner","Third Snack"]
    var selectedFoodListDatasource = [selectedFoodModel]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        totalCal.text = historyDataSouce[historyIndex].total
        remaincal.text = historyDataSouce[historyIndex].remain
        score.text = historyDataSouce[historyIndex].score
        grade.text = historyDataSouce[historyIndex].grade
        breakfast.text = historyDataSouce[historyIndex].breakfast
        firstsnack.text = historyDataSouce[historyIndex].firstsnack
        lunch.text = historyDataSouce[historyIndex].lunch
        secondsnack.text = historyDataSouce[historyIndex].secondsnack
        dinner.text = historyDataSouce[historyIndex].dinner
        thirdsnack.text = historyDataSouce[historyIndex].thirdsnack
        // DropDown Setup component
        dropDown.delegate = self
        dropDown.options = historydropDownOptionsDataSource
        
        dropDown.didSelectOption = { (selection: Selection) in
            print("DropDownParam ==\(selection.value) at index: \(selection.index)")
            
            switch selection.index {
                
            case 0:
                self.selectedFoodListDatasource.removeAll()
                self.getHistoryDetail( self.getIdfromIndex(historyIndex), meal_type: PARAMS.BREAKFAST)
                break
                //self.updateExpandableTbl()
            case 1:
                self.selectedFoodListDatasource.removeAll()
                self.getHistoryDetail( self.getIdfromIndex(historyIndex), meal_type: PARAMS.FIRSTSNACK)
                //self.updateExpandableTbl()
                break
            case 2:
                self.selectedFoodListDatasource.removeAll()
                self.getHistoryDetail( self.getIdfromIndex(historyIndex), meal_type: PARAMS.LUNCH)
                //self.updateExpandableTbl()
                break
            case 3:
                self.selectedFoodListDatasource.removeAll()
                self.getHistoryDetail( self.getIdfromIndex(historyIndex), meal_type: PARAMS.SECONDSNACK)
                //self.updateExpandableTbl()
                break
            case 4:
                self.selectedFoodListDatasource.removeAll()
                self.getHistoryDetail( self.getIdfromIndex(historyIndex), meal_type: PARAMS.DINNER)
                //self.updateExpandableTbl()
                break
            case 5:
                self.selectedFoodListDatasource.removeAll()
                self.getHistoryDetail( self.getIdfromIndex(historyIndex), meal_type: PARAMS.THIRDSNACK)
                //.updateExpandableTbl()
                break
            default:
                print("default")
                break
            }
            
            self.SelectedCatView.reloadData()

        }
        //* Custom Behavior
        dropDown.scrollingEnabled = false
        dropDown.isMultiSelect = false
        //* Custom UI
        dropDown.rowHeight = 80
        dropDown.listHeight = 650
        //dropDown.borderWidth = 1.0
        //* Custom Colors
        dropDown.borderColor = .black
        dropDown.optionColor = UIColor.init(named: "ColorDropDownOption")!
        dropDown.placeHolderColor = .white
        dropDown.menuHeaderBackgroundColor = UIColor.init(named: "ColorDropHeader")!
        dropDown.rowBackgroundColor = UIColor.init(named: "ColordropDownListBack")!
        dropDown.placeHolderText = "View Each Meal"
        
        // Custom Animation
        dropDown.expandingAnimationStyle = .spring(level: .normal)
        dropDown.expandingDuration = 0.5
        dropDown.collapsingAnimationStyle = .linear
        dropDown.collapsingDuration = 0.5
        dropDown.hideOptionsWhenSelect = true
    }
    
    override func viewDidLayoutSubviews() {
           super.viewDidLayoutSubviews()
           // selected catview reload data
           SelectedCatView.rowHeight = 80 // table view row height defined
           SelectedCatView.expandingAnimation = .left
           SelectedCatView.collapsingAnimation = .right
           SelectedCatView.tableFooterView = UIView()
           // selectedCattable View set
           SelectedCatView.dataSource = self
           SelectedCatView.delegate = self
           SelectedCatView.reloadData()
           tableViewheight.constant = SelectedCatView.contentSize.height
           self.view.layoutIfNeeded()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        createGradientLabel(titleView, letter: getStrDate(historyDataSouce[historyIndex].date), fontsize: 35, position: 0)
        
    }
    
    @IBAction func gotoHome(_ sender: Any) {
        self.gotoStoryBoardVC("HistoryVC", fullscreen: true)
    }
    @IBAction func goHomePage(_ sender: Any) {
        self.gotoStoryBoardVC("MainVC", fullscreen: true)
    }
    func updateExpandableTbl()  {
        SelectedCatView.reloadData()
        tableViewheight.constant = SelectedCatView.contentSize.height
        self.view.layoutIfNeeded()
    }
    
    func getIdfromIndex(_ index: Int) -> String {
        let id =  historyDataSouce[index].id
        return id
    }
    
    @objc func orientationDidChange() {
        switch UIDevice.current.orientation {
        case .portrait, .portraitUpsideDown, .landscapeLeft, .landscapeRight: SelectedCatView.reloadSections(IndexSet(Array(SelectedCatView.expandedSections.keys)), with: .none)
        default:break
        }
    }
    
    func getHistoryDetail(_ history_id: String,meal_type:String) {
        ApiManager.getHistoryDetail(history_id: history_id, meal_type: meal_type){
            ( issuccess, data) in
            if issuccess{
                let dict = JSON(data as Any)
                //print(dict)
                for one in dict{
                    let two = JSON(one.1)
                    let minimodel = selectedFoodModel(two)
                    self.selectedFoodListDatasource.append(minimodel)
                }
                self.updateExpandableTbl()
            }
            else{
                
            }
        }
    }
}
//MARK: ExpyTableViewDataSourceMethods
extension HistoryDetail: ExpyTableViewDataSource {
    func tableView(_ tableView: ExpyTableView, canExpandSection section: Int) -> Bool {
        return true
    }
    func tableView(_ tableView: ExpyTableView, expandableCellForSection section: Int) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: SelCatCell.self)) as! SelCatCell
        cell.labelCatName.text = selectedFoodListDatasource[section].header_title
        cell.layoutMargins = UIEdgeInsets.zero
        cell.showSeparator()
        return cell
    }
}

//MARK: ExpyTableView delegate methods
extension HistoryDetail: ExpyTableViewDelegate {
    func tableView(_ tableView: ExpyTableView, expyState state: ExpyState, changeForSection section: Int) {
    
        switch state {
        case .willExpand:
            //print("WILL EXPAND")
            return
            
        case .willCollapse:
            //print("WILL COLLAPSE")
            return
            
        case .didExpand:
            //print("DID EXPAND")
            let detailnum = selectedFoodListDatasource[section].miniFood.count
            tableViewheight.constant += CGFloat(detailnum * cellHeight )
            tableViewheight.constant = SelectedCatView.contentSize.height
            //print("expandHeight ==========\(tableViewheight.constant)")
            
        case .didCollapse:
            //print("DID COLLAPSE")
            let detailnum = selectedFoodListDatasource[section].miniFood.count
            tableViewheight.constant -= CGFloat(detailnum * cellHeight)
            tableViewheight.constant = SelectedCatView.contentSize.height
            //print("collapseHeight ==========\(tableViewheight.constant)")
        }
    }
}

// MARK: Selected category autodimention
extension HistoryDetail {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        print("DID SELECT row: \(indexPath.row), section: \(indexPath.section)")
    }
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return UITableView.automaticDimension
//    }
}
// MARK: Drop Down view delegate
extension HistoryDetail: SwiftyMenuDelegate {
    func didSelectOption(_ swiftyMenu: SwiftyMenu, _ selectedOption: SwiftyMenuDisplayable, _ index: Int) {
        print("Selected option: \(selectedOption), at index: \(index)")
    }
    func swiftyMenuWillAppear(_ swiftyMenu: SwiftyMenu) {
        //print("SwiftyMenu will appear.")
    }
    func swiftyMenuDidAppear(_ swiftyMenu: SwiftyMenu) {
        //print("SwiftyMenu did appear.")
    }
    func swiftyMenuWillDisappear(_ swiftyMenu: SwiftyMenu) {
        //print("SwiftyMenu will disappear.")
    }
    func swiftyMenuDidDisappear(_ swiftyMenu: SwiftyMenu) {
        //print("SwiftyMenu did disappear.")
    }
}
//MARK: UITableView Data Source Methods
extension HistoryDetail {
    func numberOfSections(in tableView: UITableView) -> Int {
        print("\(selectedFoodListDatasource.count)")
        return selectedFoodListDatasource.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        ///print("Row count for section \(section) is \(selectedFoodListDatasource[section].miniFood.count)")
        return selectedFoodListDatasource[section].miniFood.count + 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: DetailValTableViewCell1.self)) as! DetailValTableViewCell1
        // TODO: expandable TableView changePart
        cell.foodName.text = selectedFoodListDatasource[indexPath.section].miniFood[(indexPath.row - 1)].foodName
        cell.cal.text = selectedFoodListDatasource[indexPath.section].miniFood[(indexPath.row - 1)].value
        cell.carbs.text = selectedFoodListDatasource[indexPath.section].miniFood[(indexPath.row - 1)].carbs
        cell.fat.text = selectedFoodListDatasource[indexPath.section].miniFood[(indexPath.row - 1)].fat
        cell.protein.text = selectedFoodListDatasource[indexPath.section].miniFood[(indexPath.row - 1)].protein
        cell.fiber.text = selectedFoodListDatasource[indexPath.section].miniFood[(indexPath.row - 1)].fiber
            cell.layoutMargins = UIEdgeInsets.zero
             //cell.showSeparator()
            cell.hideSeparator()
            cellHeight = Int(cell.bounds.size.height)
            return cell
    }
}


