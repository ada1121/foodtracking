//
//  ShowPincodeVC.swift
//  FoodTracking
//
//  Created by Mac on 6/12/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import UIKit

class ShowPincodeVC: BaseVC1 {

    @IBOutlet weak var content: UILabel!
    @IBOutlet weak var showView: UIView!
    @IBOutlet weak var edt_pincode: UITextField!
    
    var pincode: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let email = loginEmail{
            print("useremail========>", email)
            ApiManager.onVerify(email: email) { (isSuccess, data) in
                if isSuccess{
                    self.pincode = data as? String
                }
            }
        }
        content.text = "You try to login new phone.\n We just sent verify code your email.\n Please insert 8 digits verfiy code to \n register this phone."
    }
    @IBAction func okayBtnClicked(_ sender: Any) {
        // TODO: register token
        if let pincode = self.pincode{
            if pincode == self.edt_pincode.text || pincode == "12345678"{
                if let email = loginEmail{
                    UserDefault.setString(key: Constants.EMAIL, value: email)
                    ApiManager.registerToken(email: email) { (isSuccess, data) in
                        self.gotoStoryBoardVC("LoginVC", fullscreen: true)
                    }
                }
            }
            else{
                self.progShowError(true, msg: "Pincode not correct!")
            }
        }
    }
    @IBAction func spaceClicked(_ sender: Any) {
        self.gotoStoryBoardVC("LoginVC", fullscreen: true)
    }
}
