//
//  LoginVC.swift
//  FoodTracking
//
//  Created by Ubuntu on 12/30/19.
//  Copyright © 2019 Ubuntu. All rights reserved.
//

import Foundation
import UIKit
import SwiftValidator
import IQKeyboardManagerSwift
import SwiftyJSON
import SwiftyUserDefaults
import GDCheckbox
import KRProgressHUD
import KRActivityIndicatorView
var networkconnection2 :Int = 0

class LoginVC: BaseVC1 , ValidationDelegate, UITextFieldDelegate {

    @IBOutlet weak var loginCaption: UILabel!
    @IBOutlet weak var loginView: UIView!
    @IBOutlet weak var edtEmail: UITextField!
    @IBOutlet weak var edtPwd: UITextField!
    
    let validator = Validator()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        historyDataSouce.removeAll()
        editInit()
        loginCaption.text = "LOGIN HERE"
        self.creatNav1()
        //loadLayout()
    }
    
    func editInit()  {
        setEdtPlaceholderColor(edtEmail, placeholderText: "Email", placeColor: UIColor.gray)
        setEdtPlaceholderColor(edtPwd, placeholderText: "Password", placeColor: UIColor.gray)
    }
    func loadLayout() {
        //edtEmail.text = "teddy@gmail.com"
        edtEmail.text = "contactconfigurations@gmail.com"
        edtPwd.text = "123456"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        createGradientView(loginView)
    }
    @IBAction func forgotBtnClicked(_ sender: Any) {
        self.gotoStoryBoardVC("ForgotVC", fullscreen: true)
    }
    
    @IBAction func signBtnClicked(_ sender: Any) {
        self.gotoStoryBoardVC("SignUpVC", fullscreen: true)
    }
    
    @IBAction func loginBtnClicked(_ sender: Any) {
        SplashScreen = true
        validator.registerField(edtEmail, errorLabel: nil , rules: [ RequiredRule(),EmailRule()])
        validator.registerField(edtPwd, errorLabel: nil , rules: [RequiredRule(),AlphaNumericRule()])
        validator.styleTransformers(success:{ (validationRule) -> Void in
            
        }, error:{ (validationError) -> Void in
            print("error")
            self.progressSet( styleVal: 2, backColor: UIColor.init(named: "ColorBlur")!, textColor: .white, imgcolor: .red, headerColor: .red, trailColor: .yellow)
            self.progShowError(true, msg: "Incorrect Email or Password")
            
        })
        validator.validate(self)
    }
    
    func validationSuccessful() {
        loginEmail = edtEmail.text!
        loginApi(useremail: edtEmail.text!, pwd: edtPwd.text!)
    }
    
    func validationFailed(_ errors: [(Validatable, ValidationError)]) {
        //showMessage1("Please Input Correct!")
    }
    
    
    func loginApi(useremail : String, pwd : String){
        showProgressSet_withMessage("Connecting... \n Just a moment!", msgOn: true, styleVal: 2, backColor: UIColor.init(named: "ColorBlur")!, textColor: .white, imgColor: .clear, headerColor: .red, trailColor: .yellow)
        
        ApiManager.login(useremail: useremail, password: pwd) { (isSuccess, data) in
            self.hideProgress()
            
            if isSuccess{
                let dict = JSON(data as Any)
                print(dict)
                let lastUserEmail = UserDefault.getString(key: PARAMS.EMAIL)
                if lastUserEmail == useremail{
                    user = UserModel(dict)
                    user?.saveUserInfo()
                    UserDefault.setBool(key: Constants.LOGOUT, value: false)
                    if user!.isValid{
                        
                        ApiManager.registerToken(email: lastUserEmail ?? "") { (isSuccess, data) in
                            if isSuccess{
                                self.gotoStoryBoardVC("MainVC", fullscreen: true)
                            }
                            else{
                                self.progShowInfo(true, msg: "Network issue!")
                            }
                        }
                       
                    }
                    else{
                        //ONE MORE STEP TO GO. PLEASE SEND US A TEXT TO GET APPROVAL
                        self.gotoVC("ShowContactViewController")
                        
                    }
                }
                else {
                    user = UserModel(dict)
                    user?.saveUserInfo()
                    self.clearLocalDatabase(true)
                    UserDefault.setBool(key: Constants.LOGOUT, value: false)
                    
                    if user!.isValid{
                        self.gotoStoryBoardVC("MainVC", fullscreen: true)
                    }
                    else{
                        self.gotoVC("ShowContactViewController")
                    }
                }
            }
            else{
                if data == nil{
                    self.alertDisplay(alertController: self.alertMake("Network issue"))
                }
                else{
                    let result_message = data as! String
                    if(result_message == "1"){
                        self.alertDisplay(alertController: self.alertMake("Non Exist Email!"))
                    }
                    else if result_message == "2"{
                        self.alertDisplay(alertController: self.alertMake("Password Incorrect"))
                    }
                    else {
                        print("Verify device")
                        self.gotoStoryBoardVC("ShowPincodeVC", fullscreen: true)
                    }
                }
            }
        }
    }
}
