//
//  MainVC.swift
//  FoodTracking
//
//  Created by Ubuntu on 12/24/19.
//  Copyright © 2019 Ubuntu. All rights reserved.
//

import UIKit
import SwiftyUserDefaults
import SwiftyJSON
import RealmSwift

class MainVC: BaseVC1 {
    
    @IBOutlet weak var setView: UIView!
    @IBOutlet weak var setLbl: UILabel!
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var firstSetValue: UITextField!
    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var mainTable: UIView!
    let defaults = UserDefaults.standard
    @IBOutlet weak var totalCalVal: UILabel!
    @IBOutlet weak var remainingCalVal: UILabel!
    @IBOutlet weak var breakfast:UIView!
    @IBOutlet weak var breakval:UIView!
    @IBOutlet weak var firstsnack:UIView!
    @IBOutlet weak var firstval:UIView!
    @IBOutlet weak var lunch:UIView!
    @IBOutlet weak var lunchval:UIView!
    @IBOutlet weak var secondsnack:UIView!
    @IBOutlet weak var secondval:UIView!
    @IBOutlet weak var dinner:UIView!
    @IBOutlet weak var dinnerval:UIView!
    @IBOutlet weak var thirdsnack:UIView!
    @IBOutlet weak var thirdval:UIView!
    @IBOutlet weak var breakvallbl: UILabel!
    @IBOutlet weak var firstvallbl: UILabel!
    @IBOutlet weak var lunchvallbl: UILabel!
    @IBOutlet weak var secondvallbl: UILabel!
    @IBOutlet weak var dinnervallbl: UILabel!
    @IBOutlet weak var thirdvallbl: UILabel!
    @IBOutlet weak var scorelbl: UILabel!
    @IBOutlet weak var gradelbl: UILabel!
    @IBOutlet weak var breakStar: UIImageView!
    @IBOutlet weak var firstStar: UIImageView!
    @IBOutlet weak var lunchStar: UIImageView!
    @IBOutlet weak var secondStar: UIImageView!
    @IBOutlet weak var dinnerStar: UIImageView!
    @IBOutlet weak var thirdStar: UIImageView!
    @IBOutlet weak var lbl_workout: UILabel!
    @IBOutlet weak var lbl_nutrition: UILabel!
    @IBOutlet weak var lbl_coach: UILabel!
    @IBOutlet weak var bottomView: UIView!
    var valueArray : [Float] = [0,0,0,0,0,0]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        SplashScreen = false
        collectionViewDataSource.removeAll()
        let user_id = user?.id!
        getFoodList( user_id!)
        
        sampleMealName = 0
        
        totalCalVal.text = UserDefault.getString(key: Constants.OLD_VALUE,defaultValue: "")
        breakvallbl.text = "\(UserDefault.getInt(key: Constants.TOTAL_BREAKFAST_CALORIE, defaultValue: 0))"
        firstvallbl.text = "\(UserDefault.getInt(key: Constants.TOTAL_FIRST_SNACK_CALORIE, defaultValue: 0))"
        lunchvallbl.text = "\(UserDefault.getInt(key: Constants.TOTAL_LUNCH_CALORIE, defaultValue: 0))"
        secondvallbl.text = "\(UserDefault.getInt(key: Constants.TOTAL_SECOND_SNACK_CALORIE, defaultValue: 0))"
        dinnervallbl.text = "\(UserDefault.getInt(key: Constants.TOTAL_DINNER_CALORIE, defaultValue: 0))"
        thirdvallbl.text = "\(UserDefault.getInt(key: Constants.TOTAL_THIRD_SNACK_CALORIE, defaultValue: 0))"
        
        getTotalValArray()
        
        var sumTotalCalories : Int = 0
        var miniTotalRemainingCalories : Int = 0
        
        for one in mealCaloriesArray {
            sumTotalCalories = sumTotalCalories + one
        }
        miniTotalRemainingCalories = (UserDefault.getString(key: Constants.OLD_VALUE)?.toInt() ?? 0) - sumTotalCalories
        remainingCalVal.text = "\(miniTotalRemainingCalories)"
        //TODO: change part-4
        UserDefault.setInt(key: Constants.REMAINING_CALORIE, value: miniTotalRemainingCalories)
        
        self.calStarLimitVal()
        self.showStar()
        self.setGradeandScore()
        self.calGrade()
        self.calWaring()
        
        createGradientLabel(titleView, letter: "Create Meal / Snack", fontsize: 30, position: 0)
        if UserDefault.getString(key: Constants.OLD_VALUE) == nil || UserDefault.getString(key: Constants.OLD_VALUE) == "0" {
            //print("nil")
            self.mainTable.isHidden = true
            self.alertViewShow()
        }
        else{
            self.alertView.isHidden = true
            self.mainTable.isHidden = false
            //print("nonil")
        }
        
        self.lbl_workout.text = "WORKOUT \n VIDEOS"
        self.lbl_nutrition.text = "NUTRITIONIST \n & PRODUCTS"
        self.lbl_coach.text = "COACH \n & SERVICES"
  }
    func calWaring(){
        let totalvaln : Int = UserDefault.getString(key: Constants.OLD_VALUE)?.toInt() ?? 0
        if Float(mealCaloriesArray[0]) > Float(Float(totalvaln) * 0.4) {
            setColor(1)
        }
        if Float(mealCaloriesArray[1]) > Float(Float(totalvaln) * 0.4) {
            setColor(2)
        }
        if Float(mealCaloriesArray[2]) > Float(Float(totalvaln) * 0.4) {
            setColor(3)
        }
        if Float(mealCaloriesArray[3]) > Float(Float(totalvaln) * 0.4) {
            setColor(4)
        }
        if Float(mealCaloriesArray[4]) > Float(Float(totalvaln) * 0.4) {
            setColor(5)
        }
        if Float(mealCaloriesArray[5]) > Float(Float(totalvaln) * 0.4) {
            setColor(6)
        }
        else{
            
        }
    }
    
    func setColor(_ index : Int ) {
        switch index {
        case 1:
                breakfast.backgroundColor = .red
                breakval.backgroundColor = .red
                break
            
        case 2:
                firstsnack.backgroundColor = .red
                firstval.backgroundColor = .red
                break
           
        case 3:
                lunch.backgroundColor = .red
                lunchval.backgroundColor = .red
                break
        case 4:
                secondsnack.backgroundColor = .red
                secondval.backgroundColor = .red
                break
        case 5:
                dinner.backgroundColor = .red
                dinnerval.backgroundColor = .red
                break
        case 6:
                thirdsnack.backgroundColor = .red
                thirdval.backgroundColor = .red
                break
        default:
            print("default")
                thirdsnack.backgroundColor = UIColor.init(named: "ColorBlur")
                thirdval.backgroundColor = UIColor.init(named: "ColorBlur")
                breakfast.backgroundColor = UIColor.init(named: "ColorBlur")
                breakval.backgroundColor = UIColor.init(named: "ColorBlur")
                firstsnack.backgroundColor = UIColor.init(named: "ColorBlur")
                firstval.backgroundColor = UIColor.init(named: "ColorBlur")
                lunch.backgroundColor = UIColor.init(named: "ColorBlur")
                lunchval.backgroundColor = UIColor.init(named: "ColorBlur")
                secondsnack.backgroundColor = UIColor.init(named: "ColorBlur")
                secondval.backgroundColor = UIColor.init(named: "ColorBlur")
                dinner.backgroundColor = UIColor.init(named: "ColorBlur")
                dinnerval.backgroundColor = UIColor.init(named: "ColorBlur")
                break
        }
    }
   
    func getFoodList(_ user_id: Int) {
        ApiManager.getFoodList(user_id: user_id){(isSuccess, data) in
             
            if isSuccess{
            let JSONData = JSON(data as Any)
            //print("this is JSONData ====",JSONData as Any)
            for one in JSONData{
                //print("this is one===",one)
                let miniJSON = JSON(one.1)
                //print("this is miniJSON====",miniJSON as Any)
                let minimodel = CollectionViewModel(miniJSON)
                collectionViewDataSource.append(minimodel)
                }
                print("getfoodEnd")
            }
            else{
            }
        }
    }
    
    @IBAction func settingBtnClicked(_ sender: Any) {
        self.gotoStoryBoardVC("SettingsVC", fullscreen: true)
   }
    
    func alertViewShow() {
        alertView.isHidden = false
        createGradientView(setView)
        setLbl.text = "SET"
        setView.addSubview(setLbl)
        transitionAnimation(view: alertView, animationOptions: .transitionCurlDown, isReset: false)
        
    }

    func calGrade() {
        //self.getValue()
        let totalvaln : Int = UserDefault.getString(key: Constants.OLD_VALUE)?.toInt() ?? 0
        let remainingval : Int = UserDefault.getInt(key: Constants.REMAINING_CALORIE, defaultValue: 0)
        var score : Float!
        var gradeVal = 0
        
        if totalvaln == 0{
           score = 0
       }
       if remainingval > 0{
           score = Float(Float(Float(totalvaln) - Float(remainingval)) * 100) / Float(totalvaln)
       }
       else if remainingval < 0 {
           score = Float(Float(Float(totalvaln) + Float(remainingval)) * 100) / Float(totalvaln)
       }
        
        if mealCaloriesArray[0] > 0{
            gradeVal += 1
        }
        if mealCaloriesArray[1] > 0{
            gradeVal += 1
        }
        if mealCaloriesArray[2] > 0{
            gradeVal += 1
        }
        if mealCaloriesArray[3] > 0{
            gradeVal += 1
        }
        if mealCaloriesArray[4] > 0{
            gradeVal += 1
        }
        if mealCaloriesArray[5] > 0{
            gradeVal += 1
        }
        
        if totalvaln == 0{
            gradelbl.text = "0"
            UserDefault.setString(key: Constants.DAILY_GRADE, value: "0")
            
        }
        else{
            
            if gradeVal < 4 {
                
                gradelbl.text = "0"
                UserDefault.setString(key: Constants.DAILY_GRADE, value: "0")
            }
            else{
                if score >= 95.8{
                    gradelbl.text = "A"
                    UserDefault.setString(key: Constants.DAILY_GRADE, value: "A")
               }
               else if score > 91.8{
                   gradelbl.text = "B"
                   UserDefault.setString(key: Constants.DAILY_GRADE, value: "B")
               }
               else if score > 83.4{
                   gradelbl.text = "C"
                   UserDefault.setString(key: Constants.DAILY_GRADE, value: "C")
               }
               else if score > 75.1{
                   gradelbl.text = "D"
                   UserDefault.setString(key: Constants.DAILY_GRADE, value: "D")
               }
               else{
                    gradelbl.text = "F"
                    UserDefault.setString(key: Constants.DAILY_GRADE, value: "F")
               }
            }
        }
    }
    
    func setGradeandScore() {
        
        let totalvaln : Int = UserDefault.getString(key: Constants.OLD_VALUE)?.toInt() ?? 0
        let remainingval : Int = UserDefault.getInt(key: Constants.REMAINING_CALORIE, defaultValue: 0)
        var score : Float!
        if totalvaln == 0{
            score = 0
        }
        if remainingval > 0{
            score = Float(Float(Float(totalvaln) - Float(remainingval)) * 100) / Float(totalvaln)
        }
        else if remainingval < 0{
            score = Float(Float(Float(totalvaln) + Float(remainingval)) * 100) / Float(totalvaln)
        }
        
        scorelbl.text = "\(Int(score ?? 0))"
        
        
        UserDefault.setFloat(key: Constants.DAILY_SCORE, value: score)
    }
    
    func showStar(){
        //self.getValue()
        if valueArray[0] > 18 && valueArray[0] < 22{
            breakStar.isHidden = false
        }
        else{
            breakStar.isHidden = true
        }
        if valueArray[1] > 9 && valueArray[1] < 11{
            firstStar.isHidden = false
        }
        else{
            firstStar.isHidden = true
        }
        if valueArray[2] > 27 && valueArray[2] < 33{
            lunchStar.isHidden = false
        }
        else{
            lunchStar.isHidden = true
        }
        if valueArray[3] > 9 && valueArray[3] < 11{
            secondStar.isHidden = false
        }
        else{
            secondStar.isHidden = true
        }
        if valueArray[4] > 18 && valueArray[4] < 22{
            dinnerStar.isHidden = false
        }
        else{
            dinnerStar.isHidden = true
        }
        if valueArray[5] > 9 && valueArray[5] < 11{
            thirdStar.isHidden = false
        }
        else{
            thirdStar.isHidden = true
        }
    }
    
    func calStarLimitVal(){
        
        let totalvaln : Int = UserDefault.getString(key: Constants.OLD_VALUE)?.toInt() ?? 0
        
        if totalvaln == 0{
            return valueArray = [0,0,0,0,0,0]
        }
        else{
            valueArray[0] = Float(mealCaloriesArray[0] * 100) / Float(totalvaln)
            valueArray[1] = Float(mealCaloriesArray[1] * 100) / Float(totalvaln)
            valueArray[2] = Float(mealCaloriesArray[2] * 100) / Float(totalvaln)
            valueArray[3] = Float(mealCaloriesArray[3] * 100) / Float(totalvaln)
            valueArray[4] = Float(mealCaloriesArray[4] * 100) / Float(totalvaln)
            valueArray[5] = Float(mealCaloriesArray[5] * 100) / Float(totalvaln)
        }
    }
    
    @IBAction func setBtnClicked(_ sender: Any) {
        if firstSetValue.text == ""{
            //print("caloriesetnil")
            self.alertDisplay(alertController: self.alertMake("Please input total calories value!"))
            return
        }
        else{
            //defaults.set(1, forKey: "calorieSetState")
            UserDefault.setString(key: Constants.OLD_VALUE, value: firstSetValue.text)
            totalCalVal.text = firstSetValue.text
            self.view.layoutIfNeeded()
            
            alertView.isHidden = true
            mainTable.isHidden = false
        }
    }
    
    @IBAction func breakFastBtnClicked(_ sender: Any) {
        self.gotoStoryBoardVC("BreakFastVC", fullscreen: true)
    }
    @IBAction func firstSnackBtnClicked(_ sender: Any) {
        self.gotoStoryBoardVC("FirstSnackVC", fullscreen: true)
    }
    
    @IBAction func lunchBtnClicked(_ sender: Any) {
        self.gotoStoryBoardVC("LunchVC", fullscreen: true)
        
    }
    @IBAction func secondSnackBtnClicked(_ sender: Any) {
        self.gotoStoryBoardVC("SecondSnackVC", fullscreen: true)
        
    }
    @IBAction func dinnerBtnClicked(_ sender: Any) {
        self.gotoStoryBoardVC("DinnerVC", fullscreen: true)
    }
    @IBAction func thirdBtnClicked(_ sender: Any) {
        self.gotoStoryBoardVC("ThirdSnackVC", fullscreen: true)
    }
    @IBAction func dailyBtnClicked(_ sender: Any) {
        self.gotoVC("ShowDailyGrade")
    }
    @IBAction func gradeBtnClicked(_ sender: Any) {
        self.gotoVC("AlertView")
    }
    @IBAction func workOutVideoBtnClicked(_ sender: Any) {
        //self.gotoVC("WorkoutVideoVC")
        self.gotoStoryBoardVC("WorkoutVideoVC", fullscreen: true)
    }
    @IBAction func nutrionistBtnClicked(_ sender: Any) {
        //self.gotoVC("NutrionistVC")
        self.gotoStoryBoardVC("NutrionistVC", fullscreen: true)
    }
    @IBAction func coatchBtnClicked(_ sender: Any) {
        //self.gotoVC("ContactVC")
        self.gotoStoryBoardVC("ContactVC", fullscreen: true)
    }
}
