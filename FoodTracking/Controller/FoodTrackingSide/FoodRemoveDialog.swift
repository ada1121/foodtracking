//
//  FoodRemoveDialog.swift
//  FoodTracking
//
//  Created by Mac on 6/16/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import UIKit
public let VC_FoodRemoveDialog = "FoodRemoveDialog"
class FoodRemoveDialog: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func okBtnclicked(_ sender: Any) {
        self.dismiss(animated: true) {
            if let foodctrtpye = foodctrType{
                if foodctrtpye == FoodctrType.breakfast.rawValue{
                    let storyboad = UIStoryboard(name: foodctrtpye, bundle: nil)
                    let targetVC = storyboad.instantiateViewController(withIdentifier: foodctrtpye) as! BreakFastVC
                    targetVC.showAlert_Remove_collection(row: remove_dialogindex)
                }
                else if foodctrtpye == FoodctrType.firstsnack.rawValue{
                    let storyboad = UIStoryboard(name: foodctrtpye, bundle: nil)
                    let targetVC = storyboad.instantiateViewController(withIdentifier: foodctrtpye) as! FirstSnackVC
                    targetVC.showAlert_Remove_collection(row: remove_dialogindex)
                    //targetVC.collectionView.reloadData()
                }
                else if foodctrtpye == FoodctrType.lunch.rawValue{
                    let storyboad = UIStoryboard(name: foodctrtpye, bundle: nil)
                    let targetVC = storyboad.instantiateViewController(withIdentifier: foodctrtpye) as! LunchVC
                    targetVC.showAlert_Remove_collection(row: remove_dialogindex)
                    //targetVC.collectionView.reloadData()
                }
                else if foodctrtpye == FoodctrType.secondsnack.rawValue{
                    let storyboad = UIStoryboard(name: foodctrtpye, bundle: nil)
                    let targetVC = storyboad.instantiateViewController(withIdentifier: foodctrtpye) as! SecondSnackVC
                    targetVC.showAlert_Remove_collection(row: remove_dialogindex)
                    //targetVC.collectionView.reloadData()
                }
                else if foodctrtpye == FoodctrType.dinner.rawValue{
                    let storyboad = UIStoryboard(name: foodctrtpye, bundle: nil)
                    let targetVC = storyboad.instantiateViewController(withIdentifier: foodctrtpye) as! DinnerVC
                    targetVC.showAlert_Remove_collection(row: remove_dialogindex)
                    //targetVC.collectionView.reloadData()
                }
                else {
                    let storyboad = UIStoryboard(name: foodctrtpye, bundle: nil)
                    let targetVC = storyboad.instantiateViewController(withIdentifier: foodctrtpye) as! ThirdSnackVC
                    targetVC.showAlert_Remove_collection(row: remove_dialogindex)
                    //targetVC.collectionView.reloadData()
                }
            }
        }
    }
    
    @IBAction func cancelBtnclicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
