//
//  AlertView.swift
//  FoodTracking
//
//  Created by Ubuntu on 1/17/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import UIKit

class AlertView: BaseVC1 {

    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var totalView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        createGradientLabel(titleView, letter: "Calculating Your Grade", fontsize: 35
            , position: 0)
    }
    @IBAction func okBtnClicked(_ sender: Any) {
        self.gotoStoryBoardVC("MainVC", fullscreen: true)
    }
}
