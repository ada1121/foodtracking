//
//  SplashVC.swift
//  FoodTracking
//
//  Created by Ubuntu on 12/24/19.
//  Copyright © 2019 Ubuntu. All rights reserved.
//

import UIKit
import SwiftyJSON
import RealmSwift
import SwiftyUserDefaults
import Alamofire

var networkcondition1 : Int = 0
class SplashVC: BaseVC1 {
    override func viewDidLoad() {
        super.viewDidLoad()
        print("SplashStart")
        self.progressSet( styleVal: 2, backColor: UIColor.init(named: "ColorBlur")!, textColor: .white, imgcolor: .red, headerColor: .red, trailColor: .yellow)
        let timeNow = Int(NSDate().timeIntervalSince1970) * 1000
        let lasttimestamp = UserDefault.getString(key: Constants.LASTTIMEDATE,defaultValue: "\(timeNow)")
        print("lastdate ==========================================",getStrDate(lasttimestamp!))
        print("nowdate ==========================================",getStrDate("\(timeNow)"))
        sampleArrayList.removeAll()
        sampleArray.removeAll()
        if networkcondition1 == 0{
            getSampleList(PARAMS.TB_SAMPLEBREAKFAST)
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 35.0, execute: {
            if networkcondition1 == 0{
            self.progShowInfo(true, msg: "There is no connection with Server")
            let storyBoard : UIStoryboard = UIStoryboard(name: "LoginVC", bundle: nil)
            let nextVC = storyBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            nextVC.modalPresentationStyle = .fullScreen
            self.present(nextVC, animated: true, completion: nil)
            }
            if networkcondition1 == 1 {
                print("networkOK")
            }
        })
    }
    
    func getSampleList(_ tableName : String) {
        
        ApiManager.getSampleLists(tableName){(isSuccess, data) in
           if isSuccess {
            var miniArray = [String]()
            let JSONData = JSON(data as Any)
            for one in JSONData{
                let miniJSON = JSON(one.1)
                miniArray.append(miniJSON[PARAMS.TITLE].stringValue)
            }
            sampleArrayList.append(miniArray)
            
                switch tableName {
                case PARAMS.TB_SAMPLEBREAKFAST:
                    self.getSampleList(PARAMS.TB_SAMPLEFIRSTSNACK)
                    break
                    
                    case PARAMS.TB_SAMPLEFIRSTSNACK:
                        self.getSampleList(PARAMS.TB_SAMPLELUNCH)
                    break
                    
                    case PARAMS.TB_SAMPLELUNCH:
                        self.getSampleList(PARAMS.TB_SAMPLEBSECONDSNACK)
                    break
                    
                    case PARAMS.TB_SAMPLEBSECONDSNACK:
                        self.getSampleList(PARAMS.TB_SAMPLEDINNER)
                    
                    case PARAMS.TB_SAMPLEDINNER:
                        self.getSampleList(PARAMS.TB_SAMPLETHIRDSNACK)
                        networkcondition1 = 1
                        //print("useremail ==========",UserDefault.getString(key: PARAMS.EMAIL))
                        self.gotoUserCheck()
                    break
                    
                default:
                    break
                }
            
            }
                        
        print("this is sampleArrayListCount====",sampleArrayList.count)
       }
    }
    
    func gotoUserCheck()  {
        let email = UserDefault.getString(key: PARAMS.EMAIL)
        if email != nil && email != ""{
            ApiManager.getMydetails(){
                (issuccess,data) in
                if issuccess{
                    let userinfoJson = JSON(data as Any)
                    user = UserModel(userinfoJson)
                    //print("this is userinfojson",userinfoJson)
                    user?.saveUserInfo()
                   // let timeNow = Int(NSDate().timeIntervalSince1970)
                    let lasttimedate : String = UserDefault.getString(key: Constants.LASTTIMEDATE,defaultValue: "0")!
                    if self.dayDifference(from: lasttimedate.toDouble()! / 1000)
                    {
                        if DBHelper.shared.getTotalHistoy().count != 0{
                            self.uploadHistoryToServer()
                            let timeNow = Int(NSDate().timeIntervalSince1970) * 1000
                            UserDefault.setString(key: Constants.LASTTIMEDATE, value: "\(timeNow)")
                            
                        }
                        else{
                            self.clearLocalDatabase(false)
                            
                            let logout = UserDefault.getBool(key: Constants.LOGOUT,defaultValue: false)
                            if logout{
                                self.gotoLoginPage()
                                networkcondition1 = 1
                                //UserDefault.setBool(key: Constants.LOGOUT, value: false)
                            }
                            else{
                                if user!.isValid{
                                    self.gotoStoryBoardVC("MainVC", fullscreen: true)
                                }
                                else{
                                   self.gotoLoginPage()
                                }
                                networkcondition1 = 1
                            }
                            
                        }
                    }
                    else{
                        let logout = UserDefault.getBool(key: Constants.LOGOUT,defaultValue: false)
                        if logout{
                            self.gotoLoginPage()
                            networkcondition1 = 1
                            //UserDefault.setBool(key: Constants.LOGOUT, value: false)
                        }
                            
                        else{
                            if user!.isValid{
                                self.gotoStoryBoardVC("MainVC", fullscreen: true)
                            }
                            else{
                               self.gotoLoginPage()
                            }
                            networkcondition1 = 1
                            //UserDefault.setBool(key: Constants.LOGOUT, value: false)
                        }
                    }
                }
                    
                else{
                    self.clearLocalDatabase(true)
                    self.gotoLoginPage()
                }
            }
        }
        else{
            self.clearLocalDatabase(true)
            self.gotoLoginPage()
        }
    }

     func uploadHistoryToServer() {

            DBHelper.shared.getTotalUploadHistoryFromRealm()
            ApiManager.uploadHistory(foodHistory: totalfoodHistoyModel){
                (issuccess, data) in
                if issuccess{
                    print("==============******* serverupload success **************========")
                    self.clearLocalDatabase(false)

                     let logout = UserDefault.getBool(key: Constants.LOGOUT,defaultValue: false)
                        if logout{
                           self.gotoLoginPage()
                           networkcondition1 = 1
                           //UserDefault.setBool(key: Constants.LOGOUT, value: false)
                        }
                        else{
                            if user!.isValid{
                                self.gotoStoryBoardVC("MainVC", fullscreen: true)
                            }
                            else{
                               self.gotoLoginPage()
                            }
                           networkcondition1 = 1
                           //UserDefault.setBool(key: Constants.LOGOUT, value: false)
                        }
                }
            }
    }
    
    func gotoLoginPage(){
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "LoginVC", bundle: nil)
        let nextVC = storyBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        nextVC.modalPresentationStyle = .fullScreen
        self.present(nextVC, animated: true, completion: nil)
        networkcondition1 = 1
    }
}
