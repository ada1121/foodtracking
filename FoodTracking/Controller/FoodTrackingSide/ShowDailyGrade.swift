//
//  ShowDailyGrade.swift
//  FoodTracking
//
//  Created by Ubuntu on 1/31/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import UIKit

class ShowDailyGrade: BaseVC1 {

    @IBOutlet weak var textContent: UILabel!
    @IBOutlet weak var titleView1: UIView!
    @IBOutlet weak var titleView2: UIView!
    
    var mealHistoy1 : FoodHistoryModel?
    var mealHistoy2 : FoodHistoryModel?
    var mealHistoy3 : FoodHistoryModel?
    var mealHistoy4 : FoodHistoryModel?
    var mealHistoy5 : FoodHistoryModel?
    var mealHistoy6 : FoodHistoryModel?
    
    var carbs: Float = 0
    var fat: Float = 0
    var protein: Float = 0
    var fiber: Float = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        createGradientLabel(titleView1, letter: "TOTAL DAILY", fontsize: 35
        , position: 0)
        
        createGradientLabel(titleView2, letter: "CARBS, FAT, PROTEIN, FIBER", fontsize: 35
        , position: 0)
        
        mealHistoy1 = DBHelper.shared.getMealHistory(Constants.BREAKFAST)
        mealHistoy2 = DBHelper.shared.getMealHistory(Constants.FIRTSNACK)
        mealHistoy3 = DBHelper.shared.getMealHistory(Constants.LUNCH)
        mealHistoy4 = DBHelper.shared.getMealHistory(Constants.SECONDSNACK)
        mealHistoy5 = DBHelper.shared.getMealHistory(Constants.DINNER)
        mealHistoy6 = DBHelper.shared.getMealHistory(Constants.THIRDSNACK)
        self.firstloop()
    }
    
    func firstloop()  {
        if mealHistoy1?.id != ""{
         var breakfast_1 = 0
         for one in mealHistoy1!.categories{
             var breakfast_2 = 0
             for two in one.subcategories{
                 carbs += Float(two.carbs.toFloat()!)
                 fat += Float(two.fat.toFloat()!)
                 protein += Float(two.protein.toFloat()!)
                 fiber += Float(two.fiber.toFloat()!)
                 breakfast_2 += 1
                 if breakfast_2 == one.subcategories.count{
                    breakfast_1 += 1
                    if breakfast_1 == mealHistoy1?.categories.count{
                        self.nextloop()
                    }
                }
              }
           }
        }
        
        else{
            self.nextloop()
        }
    }
    
    func nextloop() {
        if mealHistoy2?.id != ""{
         var breakfast_1 = 0
         for one in mealHistoy2!.categories{
             var breakfast_2 = 0
             for two in one.subcategories{
                 carbs += Float(two.carbs.toFloat()!)
                 fat += Float(two.fat.toFloat()!)
                 protein += Float(two.protein.toFloat()!)
                 fiber += Float(two.fiber.toFloat()!)
                
                breakfast_2 += 1
                 if breakfast_2 == one.subcategories.count{
                    breakfast_1 += 1
                    if breakfast_1 == mealHistoy2?.categories.count{
                        self.thirdloop()
                    }
                }
              }
           }
        }
        
        else{
           self.thirdloop()
        }
    }
    
    func thirdloop() {
        if mealHistoy3?.id != ""{
         var breakfast_1 = 0
         for one in mealHistoy3!.categories{
             var breakfast_2 = 0
             for two in one.subcategories{
                 carbs += Float(two.carbs.toFloat()!)
                 fat += Float(two.fat.toFloat()!)
                 protein += Float(two.protein.toFloat()!)
                 fiber += Float(two.fiber.toFloat()!)
                
                breakfast_2 += 1
                 if breakfast_2 == one.subcategories.count{
                    breakfast_1 += 1
                    if breakfast_1 == mealHistoy3?.categories.count{
                        self.fourthloop()
                    }
                }
                
               }
           }
        }
        
        else{
           self.fourthloop()
        }
    }
    
    func fourthloop() {
        if mealHistoy4?.id != ""{
         var breakfast_1 = 0
         for one in mealHistoy4!.categories{
             var breakfast_2 = 0
             for two in one.subcategories{
                 carbs += Float(two.carbs.toFloat()!)
                 fat += Float(two.fat.toFloat()!)
                 protein += Float(two.protein.toFloat()!)
                 fiber += Float(two.fiber.toFloat()!)
                
                 breakfast_2 += 1
                    if breakfast_2 == one.subcategories.count{
                       breakfast_1 += 1
                       if breakfast_1 == mealHistoy4?.categories.count{
                           self.fifthloop()
                       }
                   }
                
               }
             
           }
        }
        
        else{
           self.fifthloop()
        }
    }
    
    func fifthloop() {
        if mealHistoy5?.id != ""{
         var breakfast_1 = 0
         for one in mealHistoy5!.categories{
             var breakfast_2 = 0
             for two in one.subcategories{
                 carbs += Float(two.carbs.toFloat()!)
                 fat += Float(two.fat.toFloat()!)
                 protein += Float(two.protein.toFloat()!)
                 fiber += Float(two.fiber.toFloat()!)
                
                breakfast_2 += 1
                   if breakfast_2 == one.subcategories.count{
                      breakfast_1 += 1
                      if breakfast_1 == mealHistoy5?.categories.count{
                          self.sixthloop()
                      }
                  }
               }
           }
        }
        
        else{
           self.sixthloop()
        }
    }
    
    func sixthloop() {
        if mealHistoy6?.id != ""{
         var breakfast_1 = 0
         for one in mealHistoy6!.categories{
             var breakfast_2 = 0
             for two in one.subcategories{
                 carbs += Float(two.carbs.toFloat()!)
                 fat += Float(two.fat.toFloat()!)
                 protein += Float(two.protein.toFloat()!)
                 fiber += Float(two.fiber.toFloat()!)
                
                breakfast_2 += 1
                  if breakfast_2 == one.subcategories.count{
                     breakfast_1 += 1
                     if breakfast_1 == mealHistoy6?.categories.count{
                         self.showText()
                     }
                 }
               }
           }
        }
        
        else{
           self.showText()
        }
    }
    
    func showText()  {
        
        let formattedcarbs = String(format: "%.1f", carbs)
        let formattedfat = String(format: "%.1f", fat)
        let formattedprotein = String(format: "%.1f", protein)
        let formattedfiber = String(format: "%.1f", fiber)
        
       textContent.text = "Carbs = " + "\(formattedcarbs)" + "," + "  " + "Fat = " + "\(formattedfat)" + "," + "  " + "Protein = " + "\(formattedprotein)" + "," + "  " + "Fiber = " + "\(formattedfiber)"
    }
    
    @IBAction func okBtnClicked(_ sender: Any) {
        self.gotoStoryBoardVC("MainVC", fullscreen: true)
    }
}
