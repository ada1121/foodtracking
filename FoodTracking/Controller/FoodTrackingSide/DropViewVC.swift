//
//  DropViewVC.swift
//  FoodTracking
//
//  Created by Ubuntu on 12/30/19.
//  Copyright © 2019 Ubuntu. All rights reserved.
//

import UIKit

class DropViewVC: BaseVC1{

    @IBOutlet weak var spaceView: UIView!
    @IBOutlet weak var sampleTable: UITableView!
    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var tableViewHeigh: NSLayoutConstraint!
    
    var cellHeight = 80
    
//    let sampleMeal = ["1/2 cup Fiber One Original","1 cup nonfat milk","1/4 whole cantalope","2 cups black coffee","water"]
    override func viewDidLoad() {
        super.viewDidLoad()
        tableViewHeigh.constant = CGFloat(sampleArray.count * cellHeight)
        spaceView.addTapGesture(tapNumber: 1, target: self, action: #selector(onSpace))
    }
    
    @objc func onSpace(gesture: UITapGestureRecognizer) -> Void {
          closeMenu()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        switch sampleMealName {
        case 1 :
            createGradientLabel(titleView, letter: "Sample Breakfast", fontsize: 35, position: 0)
            //getSampleList(PARAMS.TB_SAMPLEBREAKFAST)
        case 2 :
            createGradientLabel(titleView, letter: "Sample First Snack", fontsize: 35, position: 0)
            //getSampleList(PARAMS.TB_SAMPLEFIRSTSNACK)
        case 3 :
            createGradientLabel(titleView, letter: "Sample Lunch", fontsize: 35, position: 0)
            //getSampleList(PARAMS.TB_SAMPLELUNCH)
        case 4 :
            createGradientLabel(titleView, letter: "Sample Second Snack", fontsize: 35, position: 0)
            //getSampleList(PARAMS.TB_SAMPLEBSECONDSNACK)
        case 5 :
            createGradientLabel(titleView, letter: "Sample Dinner", fontsize: 35, position: 0)
            //getSampleList(PARAMS.TB_SAMPLEDINNER)
        case 6 :
            createGradientLabel(titleView, letter: "Sample Third Snack", fontsize: 35, position: 0)
            //getSampleList(PARAMS.TB_SAMPLETHIRDSNACK)
        default:
            print("default")
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
             sampleTable.roundCorners([.bottomLeft, .bottomRight], radius: 20)
    }
    @IBAction func closeBtnClicked(_ sender: Any) {
        self.closeMenu()
    }
}

extension DropViewVC : UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sampleArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: SampleCell.self)) as! SampleCell
        cell.sampleMeal.text = sampleArray[indexPath.row]
        cellHeight = Int(cell.bounds.size.height)
        return cell
    }
}

class SampleCell: UITableViewCell {
    @IBOutlet weak var sampleMeal: UILabel!
}
