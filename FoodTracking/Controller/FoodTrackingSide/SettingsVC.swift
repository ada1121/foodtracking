//
//  SettingsVC.swift
//  FoodTracking
//
//  Created by Ubuntu on 12/24/19.
//  Copyright © 2019 Ubuntu. All rights reserved.
//

import UIKit
import SwiftyUserDefaults
import SwiftyJSON

class SettingsVC: BaseVC1 {
    
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var coverView: UIView!
    
    @IBOutlet weak var updateView: UIView!
    @IBOutlet weak var cancelView: UIView!
    @IBOutlet weak var setView: UIView!
    
    @IBOutlet weak var updateBtn: UIButton!
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var setBtn: UIButton!
    
    @IBOutlet weak var titleView: UIView!
    
    @IBOutlet weak var updatelbl: UILabel!
    
    @IBOutlet weak var calorieValue: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        SplashScreen = false
        alertView.isHidden = true
        alertViewShow()
        calorieValue.placeholder = UserDefault.getString(key: Constants.OLD_VALUE,defaultValue: "")
        creatNav1()
    }
    
    func alertViewShow() {
        setView.removeFromSuperview()
        createGradientView(updateView)
        updatelbl.text = "UPDATE"
        updateView.addSubview(updatelbl)
        cancelView.backgroundColor = UIColor.systemIndigo
        transitionAnimation(view: alertView, animationOptions: .transitionCurlDown, isReset: false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        createGradientLabel(titleView, letter: R.string.SETTINGS , fontsize: 35, position: 1)
    }
    
    @IBAction func showAlertDilaog(_ sender: Any) {
        
        alertView.isHidden = !alertView.isHidden
        calorieValue.placeholder = UserDefault.getString(key: Constants.OLD_VALUE,defaultValue: "")
        
        if !alertView.isHidden {
            transitionAnimation(view: alertView, animationOptions: .transitionCurlDown, isReset: false)
            //coverView.isHidden = false
        }
        
        else {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3, execute: {
                //self.coverView.isHidden = true
              self.alertView.isHidden = true
              }
            )
        }
    }
    @IBAction func logout(_ sender: Any) {
        self.gotoStoryBoardVC("LoginVC", fullscreen: true)
        //user?.clearUserInfo()
        UserDefault.setBool(key: Constants.LOGOUT, value: true)
    }
    
    @IBAction func gotoFoodTracking(_ sender: Any) {
        //print("gotoFoodTracking")
        self.getHistory()
        
    }
    
    @IBAction func gotoCoach(_ sender: Any) {
        //print("gotoCoach")
        self.openMenu1()
    }
    
    @IBAction func updateBtnClicked(_ sender: Any) {
        if calorieValue.text == "" {
            print("nilValue")
            self.alertDisplay(alertController: self.alertMake("Please input updated total calories value!"))
            return
        }
        else{
            UserDefault.setString(key: Constants.OLD_VALUE, value: calorieValue.text!)
            UserDefault.setString(key: PARAMS.TOTAL, value: calorieValue.text!)
            self.alertView.isHidden = true
            calorieValue.text = ""
        }
    }
    
    @IBAction func cancelBtnClicked(_ sender: Any) {
        self.alertView.isHidden = true
        calorieValue.text = ""
    }
    
    @IBAction func backBtnClicked(_ sender: Any) {
        self.gotoStoryBoardVC("MainVC", fullscreen: true)
    }
}
