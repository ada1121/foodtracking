//
//  NewDownloadDialogVC.swift
//  FoodTracking
//
//  Created by Mac on 6/12/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import UIKit

class NewDownloadDialogVC: BaseVC1 {
    
    @IBOutlet weak var content: UILabel!
    @IBOutlet weak var toptitle: UILabel!
    
    @IBOutlet weak var showView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        toptitle.text = "ATTENTION \n You are about to register your new phone. "
        content.text = "Please send us a text to get \n approval. Click OK then click \n the text button below."
    }
    
    @IBAction func okayBtnClicked(_ sender: Any) {
        // TODO: add command to add new newphone setting
    }
    @IBAction func spaceClicked(_ sender: Any) {
        self.gotoStoryBoardVC("LoginVC", fullscreen: true)
    }
}
