//
//  ContactCoachVC.swift
//  FoodTracking
//
//  Created by Ubuntu on 1/16/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//
import UIKit
import MessageUI

class ContactCoachVC: BaseVC1 {

    @IBOutlet weak var spaceView: UIView!
    @IBOutlet weak var sampleTable: UITableView!
    @IBOutlet weak var titleView: UIView!
    
    var cellHeight = 50
    var coachmodeldatasource = [coachmodel]()
    var name = ["Primary","Secondary"]
    var phone = ["Bill-774-240-5287","Jill-774-540-8357"]

    override func viewDidLoad() {
        super.viewDidLoad()
        //tableViewHeigh.constant = CGFloat(sampleArray.count * cellHeight)
        spaceView.addTapGesture(tapNumber: 1, target: self, action: #selector(onSpace))
        for i in 0 ... 1 {
            coachmodeldatasource.append(coachmodel(name[i],phone:phone[i]))
        }
    }
    
    @objc func onSpace(gesture: UITapGestureRecognizer) -> Void {
          closeMenu1()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        titleView.roundCorners([.topLeft, .topRight], radius: 20)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        //createGradientLabel(titleView, letter: "Contact coach", fontsize: 24, position: 0)
    }
    @IBAction func closeBtnClicked(_ sender: Any) {
        if !SplashScreen{
            self.closeMenu1()
        }
        else{
            self.gotoStoryBoardVC("LoginVC", fullscreen: true)
        }
    }
    
    @IBAction func smsClicked(_ sender: Any) {
        let button : UIButton = sender as! UIButton
        if dialogIndex != 2{
            if button.tag == 0{
                //self.sendNewIMessage("774-240-5287")
                messagePhoneNumber = "774-240-5287"
            }
            else{
                //self.sendNewIMessage("774-540-8357")
                messagePhoneNumber = "774-540-8357"
            }
            gotoVCModal(VC_MessageAttachDilaog)
        }
        else{
            calltag = button.tag
            self.gotoVC("SmsDialog")
        }
        
    }
    
    @IBAction func callClicked(_ sender: Any) {
        let button : UIButton = sender as! UIButton
        if button.tag == 0{
            "774-240-5287".makeACall()
        }
        else{
            "774-540-8357".makeACall()
        }
    }
}

extension ContactCoachVC : UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return coachmodeldatasource.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ContactCell.self)) as! ContactCell
        cell.name.text = coachmodeldatasource[indexPath.row].name
        cell.phonenumber.text = coachmodeldatasource[indexPath.row].phone
        cell.callBtn.tag = indexPath.row
        cell.smsBtn.tag = indexPath.row
        cellHeight = Int(cell.bounds.size.height)
        return cell
    }
}

class ContactCell: UITableViewCell {
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var phonenumber: UILabel!
    @IBOutlet weak var smsBtn: UIButton!
    @IBOutlet weak var callBtn: UIButton!
    
}
class coachmodel {
    var name = ""
    var phone = ""
    init(_ name:String,phone:String) {
        self.name = name
        self.phone = phone
    }
}
