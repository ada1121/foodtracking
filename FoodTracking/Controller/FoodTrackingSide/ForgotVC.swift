//
//  ForgotVC.swift
//  FoodTracking
//
//  Created by Ubuntu on 12/30/19.
//  Copyright © 2019 Ubuntu. All rights reserved.
//

import Foundation
import UIKit
import SwiftValidator
import IQKeyboardManagerSwift
import SwiftyJSON
import SwiftyUserDefaults
import GDCheckbox
import KRProgressHUD
import KRActivityIndicatorView

class ForgotVC: BaseVC1 , ValidationDelegate, UITextFieldDelegate{

    @IBOutlet weak var sendCaption: UILabel!
    @IBOutlet weak var sendBtn: UIView!
    
    @IBOutlet weak var edtEmail: UITextField!
    let validator = Validator()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        sendCaption.text = "SEND"
        editInit()
        
    }
    func editInit()  {
           setEdtPlaceholderColor(edtEmail, placeholderText: "Email", placeColor: UIColor.gray)
       }
    override func viewWillAppear(_ animated: Bool) {
            createGradientView(sendBtn)
    }
    
    @IBAction func sendBtnClicked(_ sender: Any) {
        
        validator.registerField( edtEmail, errorLabel: nil , rules: [ RequiredRule(),EmailRule()])
        validator.styleTransformers(success:{ (validationRule) -> Void in
            
            // clear error label
           
        }, error:{ (validationError) -> Void in
            print("error")
            self.progressSet( styleVal: 2, backColor: UIColor.init(named: "ColorBlur")!, textColor: .white, imgcolor: .red, headerColor: .red, trailColor: .yellow)
            self.progShowError(true, msg: "Please Input Your Correct Email! ")
            
        })
        validator.validate(self)
    }
    
    func validationSuccessful() {
        forgotapi(useremail: edtEmail.text!)
        
        
    }
    
    func validationFailed(_ errors: [(Validatable, ValidationError)]) {
        //showMessage1("Please Input Correct!")
    }
    
    func forgotapi(useremail : String){
        showProgressSet_withMessage("  Connecting ... \n Just a moment!", msgOn: true, styleVal: 2, backColor: UIColor.init(named: "ColorBlur")!, textColor: .white, imgColor: .clear, headerColor: .red, trailColor: .yellow)
        
        ApiManager.forgot(email: useremail) { (isSuccess, data) in
            self.hideProgress()
            self.showLoginAlert()
            if isSuccess{
                //self.showMessage1(data as! String)
                
                
            }
            else{
                if data == nil{
                    self.alertDisplay(alertController: self.alertMake("Connection Error!"))
                }
                else{
                    let result_message = data as! String
                    if(result_message == "1"){
                        
                    self.alertDisplay(alertController: self.alertMake("Non Exist Email!"))
                    }
                    
                }
                
            }
            
        }
        
    }
    
    
    @IBAction func loginBtnClicked(_ sender: Any) {
        self.gotoStoryBoardVC("LoginVC", fullscreen: true)
    }
    

    func showLoginAlert() {
        let loginAlert = UIAlertController(title: "Reset Password", message: "Please Input Your PIN First", preferredStyle: .alert)
        loginAlert.view.tintColor = .systemBlue

        loginAlert.addTextField { usernameField in
            usernameField.font = .systemFont(ofSize: 14.0)
            usernameField.placeholder = "Your PIN here"
        }
        loginAlert.addTextField { passwordField in
            passwordField.font = .systemFont(ofSize: 14.0)
            passwordField.isSecureTextEntry = true
            passwordField.placeholder = "New Password"
        }
        
        loginAlert.addTextField { passwordField in
            passwordField.font = .systemFont(ofSize: 14.0)
            passwordField.isSecureTextEntry = true
            passwordField.placeholder = "Confirm New Password"
        }
        
        let loginAction = UIAlertAction(title: "Ok",
                                        style: .default,
                                        handler: { _ in
                                            // self.handleUsernamePasswordEntered(loginAlert: loginAlert)
                                             loginAlert.dismiss(animated: true)
        })

        let cancelAction = UIAlertAction(title: "Cancel",
                                         style: .destructive,
                                         handler: { _ in
                                            // self.handleUsernamePasswordCanceled(loginAlert: loginAlert)
                                            loginAlert.dismiss(animated: true)
                                            
        })
        

        
        loginAlert.addAction(loginAction)
        loginAlert.addAction(cancelAction)
        loginAlert.preferredAction = loginAction
        present(loginAlert, animated: true, completion: nil)
    }
    
}
