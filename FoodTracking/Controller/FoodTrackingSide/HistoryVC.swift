//
//  HistoryVC.swift
//  FoodTracking
//
//  Created by Ubuntu on 1/15/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import UIKit
import SwiftyJSON
class HistoryVC: BaseVC1 {

    @IBOutlet weak var historyTbl: UITableView!
    @IBOutlet weak var titleView: UIView!
    var cellHeight = 80

    override func viewDidLoad() {
        super.viewDidLoad()
        //tableViewHeigh.constant = CGFloat(sampleArray.count * cellHeight)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        createGradientLabel(titleView, letter: "Histories", fontsize: 35, position: 0)
        
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        self.historyTbl.reloadData()
        //tableViewHeigh.constant = CGFloat(sampleArray.count * cellHeight)
        self.view.layoutIfNeeded()
        
    }
    @IBAction func gotoHome(_ sender: Any) {
        self.gotoStoryBoardVC("MainVC", fullscreen: true)
    }
    
}

extension HistoryVC : UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return historyDataSouce.count
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: SampleCell.self)) as! SampleCell
        
        //let dateString = getStrDate(entity.timestamp)
        cell.sampleMeal.text = getStrDate(historyDataSouce[indexPath.row].date)
        //cell.sampleMeal.text = historyDataSouce[indexPath.row].date
        cellHeight = Int(cell.bounds.size.height)
        let backgroundView = UIView()
        backgroundView.backgroundColor = UIColor.clear
        cell.selectedBackgroundView = backgroundView
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        historyIndex = indexPath.row
        self.gotoStoryBoardVC("HistoryDetail", fullscreen: true)
    }
}
